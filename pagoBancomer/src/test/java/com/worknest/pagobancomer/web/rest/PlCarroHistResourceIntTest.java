package com.worknest.pagobancomer.web.rest;

import static com.worknest.pagobancomer.web.rest.TestUtil.createFormattingConversionService;
import static com.worknest.pagobancomer.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.worknest.pagobancomer.PagoBancomerApp;
import com.worknest.pagobancomer.domain.PlCarroHist;
import com.worknest.pagobancomer.repository.PlCarroHistRepository;
import com.worknest.pagobancomer.service.PlCarroHistService;
import com.worknest.pagobancomer.service.dto.PlCarroHistDTO;
import com.worknest.pagobancomer.service.mapper.PlCarroHistMapper;
import com.worknest.pagobancomer.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the PlCarroHistResource REST controller.
 *
 * @see PlCarroHistResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PagoBancomerApp.class)
public class PlCarroHistResourceIntTest {

    private static final ZonedDateTime DEFAULT_FECHA_ENVIO = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FECHA_ENVIO = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_REFERENCIA = "AAAAAAAAAA";
    private static final String UPDATED_REFERENCIA = "BBBBBBBBBB";

    @Autowired
    private PlCarroHistRepository plCarroHistRepository;

    @Autowired
    private PlCarroHistMapper plCarroHistMapper;

    @Autowired
    private PlCarroHistService plCarroHistService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPlCarroHistMockMvc;

    private PlCarroHist plCarroHist;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PlCarroHistResource plCarroHistResource = new PlCarroHistResource(plCarroHistService);
        this.restPlCarroHistMockMvc = MockMvcBuilders.standaloneSetup(plCarroHistResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PlCarroHist createEntity(EntityManager em) {
        PlCarroHist plCarroHist = new PlCarroHist()
            .fechaEnvio(DEFAULT_FECHA_ENVIO)
            .referencia(DEFAULT_REFERENCIA);
        return plCarroHist;
    }

    @Before
    public void initTest() {
        plCarroHist = createEntity(em);
    }

    @Test
    @Transactional
    public void createPlCarroHist() throws Exception {
        int databaseSizeBeforeCreate = plCarroHistRepository.findAll().size();

        // Create the PlCarroHist
        PlCarroHistDTO plCarroHistDTO = plCarroHistMapper.toDto(plCarroHist);
        restPlCarroHistMockMvc.perform(post("/api/pl-carro-hists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(plCarroHistDTO)))
            .andExpect(status().isCreated());

        // Validate the PlCarroHist in the database
        List<PlCarroHist> plCarroHistList = plCarroHistRepository.findAll();
        assertThat(plCarroHistList).hasSize(databaseSizeBeforeCreate + 1);
        PlCarroHist testPlCarroHist = plCarroHistList.get(plCarroHistList.size() - 1);
        assertThat(testPlCarroHist.getFechaEnvio()).isEqualTo(DEFAULT_FECHA_ENVIO);
        assertThat(testPlCarroHist.getReferencia()).isEqualTo(DEFAULT_REFERENCIA);
    }

    @Test
    @Transactional
    public void createPlCarroHistWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = plCarroHistRepository.findAll().size();

        // Create the PlCarroHist with an existing ID
        plCarroHist.setId(1L);
        PlCarroHistDTO plCarroHistDTO = plCarroHistMapper.toDto(plCarroHist);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPlCarroHistMockMvc.perform(post("/api/pl-carro-hists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(plCarroHistDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PlCarroHist in the database
        List<PlCarroHist> plCarroHistList = plCarroHistRepository.findAll();
        assertThat(plCarroHistList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkFechaEnvioIsRequired() throws Exception {
        int databaseSizeBeforeTest = plCarroHistRepository.findAll().size();
        // set the field null
        plCarroHist.setFechaEnvio(null);

        // Create the PlCarroHist, which fails.
        PlCarroHistDTO plCarroHistDTO = plCarroHistMapper.toDto(plCarroHist);

        restPlCarroHistMockMvc.perform(post("/api/pl-carro-hists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(plCarroHistDTO)))
            .andExpect(status().isBadRequest());

        List<PlCarroHist> plCarroHistList = plCarroHistRepository.findAll();
        assertThat(plCarroHistList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkReferenciaIsRequired() throws Exception {
        int databaseSizeBeforeTest = plCarroHistRepository.findAll().size();
        // set the field null
        plCarroHist.setReferencia(null);

        // Create the PlCarroHist, which fails.
        PlCarroHistDTO plCarroHistDTO = plCarroHistMapper.toDto(plCarroHist);

        restPlCarroHistMockMvc.perform(post("/api/pl-carro-hists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(plCarroHistDTO)))
            .andExpect(status().isBadRequest());

        List<PlCarroHist> plCarroHistList = plCarroHistRepository.findAll();
        assertThat(plCarroHistList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPlCarroHists() throws Exception {
        // Initialize the database
        plCarroHistRepository.saveAndFlush(plCarroHist);

        // Get all the plCarroHistList
        restPlCarroHistMockMvc.perform(get("/api/pl-carro-hists?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(plCarroHist.getId().intValue())))
            .andExpect(jsonPath("$.[*].fechaEnvio").value(hasItem(sameInstant(DEFAULT_FECHA_ENVIO))))
            .andExpect(jsonPath("$.[*].referencia").value(hasItem(DEFAULT_REFERENCIA.toString())));
    }

    @Test
    @Transactional
    public void getPlCarroHist() throws Exception {
        // Initialize the database
        plCarroHistRepository.saveAndFlush(plCarroHist);

        // Get the plCarroHist
        restPlCarroHistMockMvc.perform(get("/api/pl-carro-hists/{id}", plCarroHist.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(plCarroHist.getId().intValue()))
            .andExpect(jsonPath("$.fechaEnvio").value(sameInstant(DEFAULT_FECHA_ENVIO)))
            .andExpect(jsonPath("$.referencia").value(DEFAULT_REFERENCIA.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPlCarroHist() throws Exception {
        // Get the plCarroHist
        restPlCarroHistMockMvc.perform(get("/api/pl-carro-hists/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePlCarroHist() throws Exception {
        // Initialize the database
        plCarroHistRepository.saveAndFlush(plCarroHist);
        int databaseSizeBeforeUpdate = plCarroHistRepository.findAll().size();

        // Update the plCarroHist
        PlCarroHist updatedPlCarroHist = plCarroHistRepository.findOne(plCarroHist.getId());
        updatedPlCarroHist
            .fechaEnvio(UPDATED_FECHA_ENVIO)
            .referencia(UPDATED_REFERENCIA);
        PlCarroHistDTO plCarroHistDTO = plCarroHistMapper.toDto(updatedPlCarroHist);

        restPlCarroHistMockMvc.perform(put("/api/pl-carro-hists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(plCarroHistDTO)))
            .andExpect(status().isOk());

        // Validate the PlCarroHist in the database
        List<PlCarroHist> plCarroHistList = plCarroHistRepository.findAll();
        assertThat(plCarroHistList).hasSize(databaseSizeBeforeUpdate);
        PlCarroHist testPlCarroHist = plCarroHistList.get(plCarroHistList.size() - 1);
        assertThat(testPlCarroHist.getFechaEnvio()).isEqualTo(UPDATED_FECHA_ENVIO);
        assertThat(testPlCarroHist.getReferencia()).isEqualTo(UPDATED_REFERENCIA);
    }

    @Test
    @Transactional
    public void updateNonExistingPlCarroHist() throws Exception {
        int databaseSizeBeforeUpdate = plCarroHistRepository.findAll().size();

        // Create the PlCarroHist
        PlCarroHistDTO plCarroHistDTO = plCarroHistMapper.toDto(plCarroHist);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPlCarroHistMockMvc.perform(put("/api/pl-carro-hists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(plCarroHistDTO)))
            .andExpect(status().isCreated());

        // Validate the PlCarroHist in the database
        List<PlCarroHist> plCarroHistList = plCarroHistRepository.findAll();
        assertThat(plCarroHistList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePlCarroHist() throws Exception {
        // Initialize the database
        plCarroHistRepository.saveAndFlush(plCarroHist);
        int databaseSizeBeforeDelete = plCarroHistRepository.findAll().size();

        // Get the plCarroHist
        restPlCarroHistMockMvc.perform(delete("/api/pl-carro-hists/{id}", plCarroHist.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PlCarroHist> plCarroHistList = plCarroHistRepository.findAll();
        assertThat(plCarroHistList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PlCarroHist.class);
        PlCarroHist plCarroHist1 = new PlCarroHist();
        plCarroHist1.setId(1L);
        PlCarroHist plCarroHist2 = new PlCarroHist();
        plCarroHist2.setId(plCarroHist1.getId());
        assertThat(plCarroHist1).isEqualTo(plCarroHist2);
        plCarroHist2.setId(2L);
        assertThat(plCarroHist1).isNotEqualTo(plCarroHist2);
        plCarroHist1.setId(null);
        assertThat(plCarroHist1).isNotEqualTo(plCarroHist2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PlCarroHistDTO.class);
        PlCarroHistDTO plCarroHistDTO1 = new PlCarroHistDTO();
        plCarroHistDTO1.setId(1L);
        PlCarroHistDTO plCarroHistDTO2 = new PlCarroHistDTO();
        assertThat(plCarroHistDTO1).isNotEqualTo(plCarroHistDTO2);
        plCarroHistDTO2.setId(plCarroHistDTO1.getId());
        assertThat(plCarroHistDTO1).isEqualTo(plCarroHistDTO2);
        plCarroHistDTO2.setId(2L);
        assertThat(plCarroHistDTO1).isNotEqualTo(plCarroHistDTO2);
        plCarroHistDTO1.setId(null);
        assertThat(plCarroHistDTO1).isNotEqualTo(plCarroHistDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(plCarroHistMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(plCarroHistMapper.fromId(null)).isNull();
    }
}
