package com.worknest.pagobancomer.web.rest;

import static com.worknest.pagobancomer.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.worknest.pagobancomer.PagoBancomerApp;
import com.worknest.pagobancomer.domain.CatBanco;
import com.worknest.pagobancomer.repository.CatBancoRepository;
import com.worknest.pagobancomer.service.CatBancoService;
import com.worknest.pagobancomer.service.dto.CatBancoDTO;
import com.worknest.pagobancomer.service.mapper.CatBancoMapper;
import com.worknest.pagobancomer.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the CatBancoResource REST controller.
 *
 * @see CatBancoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PagoBancomerApp.class)
public class CatBancoResourceIntTest {

    private static final String DEFAULT_DESCRIPCION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPCION = "BBBBBBBBBB";

    @Autowired
    private CatBancoRepository catBancoRepository;

    @Autowired
    private CatBancoMapper catBancoMapper;

    @Autowired
    private CatBancoService catBancoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCatBancoMockMvc;

    private CatBanco catBanco;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CatBancoResource catBancoResource = new CatBancoResource(catBancoService);
        this.restCatBancoMockMvc = MockMvcBuilders.standaloneSetup(catBancoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CatBanco createEntity(EntityManager em) {
        CatBanco catBanco = new CatBanco()
            .descripcion(DEFAULT_DESCRIPCION);
        return catBanco;
    }

    @Before
    public void initTest() {
        catBanco = createEntity(em);
    }

    @Test
    @Transactional
    public void createCatBanco() throws Exception {
        int databaseSizeBeforeCreate = catBancoRepository.findAll().size();

        // Create the CatBanco
        CatBancoDTO catBancoDTO = catBancoMapper.toDto(catBanco);
        restCatBancoMockMvc.perform(post("/api/cat-bancos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(catBancoDTO)))
            .andExpect(status().isCreated());

        // Validate the CatBanco in the database
        List<CatBanco> catBancoList = catBancoRepository.findAll();
        assertThat(catBancoList).hasSize(databaseSizeBeforeCreate + 1);
        CatBanco testCatBanco = catBancoList.get(catBancoList.size() - 1);
        assertThat(testCatBanco.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
    }

    @Test
    @Transactional
    public void createCatBancoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = catBancoRepository.findAll().size();

        // Create the CatBanco with an existing ID
        catBanco.setId(1L);
        CatBancoDTO catBancoDTO = catBancoMapper.toDto(catBanco);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCatBancoMockMvc.perform(post("/api/cat-bancos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(catBancoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CatBanco in the database
        List<CatBanco> catBancoList = catBancoRepository.findAll();
        assertThat(catBancoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescripcionIsRequired() throws Exception {
        int databaseSizeBeforeTest = catBancoRepository.findAll().size();
        // set the field null
        catBanco.setDescripcion(null);

        // Create the CatBanco, which fails.
        CatBancoDTO catBancoDTO = catBancoMapper.toDto(catBanco);

        restCatBancoMockMvc.perform(post("/api/cat-bancos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(catBancoDTO)))
            .andExpect(status().isBadRequest());

        List<CatBanco> catBancoList = catBancoRepository.findAll();
        assertThat(catBancoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCatBancos() throws Exception {
        // Initialize the database
        catBancoRepository.saveAndFlush(catBanco);

        // Get all the catBancoList
        restCatBancoMockMvc.perform(get("/api/cat-bancos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(catBanco.getId().intValue())))
            .andExpect(jsonPath("$.[*].descripcion").value(hasItem(DEFAULT_DESCRIPCION.toString())));
    }

    @Test
    @Transactional
    public void getCatBanco() throws Exception {
        // Initialize the database
        catBancoRepository.saveAndFlush(catBanco);

        // Get the catBanco
        restCatBancoMockMvc.perform(get("/api/cat-bancos/{id}", catBanco.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(catBanco.getId().intValue()))
            .andExpect(jsonPath("$.descripcion").value(DEFAULT_DESCRIPCION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCatBanco() throws Exception {
        // Get the catBanco
        restCatBancoMockMvc.perform(get("/api/cat-bancos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCatBanco() throws Exception {
        // Initialize the database
        catBancoRepository.saveAndFlush(catBanco);
        int databaseSizeBeforeUpdate = catBancoRepository.findAll().size();

        // Update the catBanco
        CatBanco updatedCatBanco = catBancoRepository.findOne(catBanco.getId());
        updatedCatBanco
            .descripcion(UPDATED_DESCRIPCION);
        CatBancoDTO catBancoDTO = catBancoMapper.toDto(updatedCatBanco);

        restCatBancoMockMvc.perform(put("/api/cat-bancos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(catBancoDTO)))
            .andExpect(status().isOk());

        // Validate the CatBanco in the database
        List<CatBanco> catBancoList = catBancoRepository.findAll();
        assertThat(catBancoList).hasSize(databaseSizeBeforeUpdate);
        CatBanco testCatBanco = catBancoList.get(catBancoList.size() - 1);
        assertThat(testCatBanco.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
    }

    @Test
    @Transactional
    public void updateNonExistingCatBanco() throws Exception {
        int databaseSizeBeforeUpdate = catBancoRepository.findAll().size();

        // Create the CatBanco
        CatBancoDTO catBancoDTO = catBancoMapper.toDto(catBanco);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCatBancoMockMvc.perform(put("/api/cat-bancos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(catBancoDTO)))
            .andExpect(status().isCreated());

        // Validate the CatBanco in the database
        List<CatBanco> catBancoList = catBancoRepository.findAll();
        assertThat(catBancoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCatBanco() throws Exception {
        // Initialize the database
        catBancoRepository.saveAndFlush(catBanco);
        int databaseSizeBeforeDelete = catBancoRepository.findAll().size();

        // Get the catBanco
        restCatBancoMockMvc.perform(delete("/api/cat-bancos/{id}", catBanco.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CatBanco> catBancoList = catBancoRepository.findAll();
        assertThat(catBancoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CatBanco.class);
        CatBanco catBanco1 = new CatBanco();
        catBanco1.setId(1L);
        CatBanco catBanco2 = new CatBanco();
        catBanco2.setId(catBanco1.getId());
        assertThat(catBanco1).isEqualTo(catBanco2);
        catBanco2.setId(2L);
        assertThat(catBanco1).isNotEqualTo(catBanco2);
        catBanco1.setId(null);
        assertThat(catBanco1).isNotEqualTo(catBanco2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CatBancoDTO.class);
        CatBancoDTO catBancoDTO1 = new CatBancoDTO();
        catBancoDTO1.setId(1L);
        CatBancoDTO catBancoDTO2 = new CatBancoDTO();
        assertThat(catBancoDTO1).isNotEqualTo(catBancoDTO2);
        catBancoDTO2.setId(catBancoDTO1.getId());
        assertThat(catBancoDTO1).isEqualTo(catBancoDTO2);
        catBancoDTO2.setId(2L);
        assertThat(catBancoDTO1).isNotEqualTo(catBancoDTO2);
        catBancoDTO1.setId(null);
        assertThat(catBancoDTO1).isNotEqualTo(catBancoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(catBancoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(catBancoMapper.fromId(null)).isNull();
    }
}
