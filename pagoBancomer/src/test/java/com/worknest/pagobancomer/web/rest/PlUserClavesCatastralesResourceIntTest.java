package com.worknest.pagobancomer.web.rest;

import static com.worknest.pagobancomer.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.worknest.pagobancomer.PagoBancomerApp;
import com.worknest.pagobancomer.domain.PlUserClavesCatastrales;
import com.worknest.pagobancomer.repository.PlUserClavesCatastralesRepository;
import com.worknest.pagobancomer.service.PlUserClavesCatastralesService;
import com.worknest.pagobancomer.service.dto.PlUserClavesCatastralesDTO;
import com.worknest.pagobancomer.service.mapper.PlUserClavesCatastralesMapper;
import com.worknest.pagobancomer.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the PlUserClavesCatastralesResource REST controller.
 *
 * @see PlUserClavesCatastralesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PagoBancomerApp.class)
public class PlUserClavesCatastralesResourceIntTest {

    private static final String DEFAULT_LLAVE = "AAAAAAAAAA";
    private static final String UPDATED_LLAVE = "BBBBBBBBBB";

    @Autowired
    private PlUserClavesCatastralesRepository plUserClavesCatastralesRepository;

    @Autowired
    private PlUserClavesCatastralesMapper plUserClavesCatastralesMapper;

    @Autowired
    private PlUserClavesCatastralesService plUserClavesCatastralesService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPlUserClavesCatastralesMockMvc;

    private PlUserClavesCatastrales plUserClavesCatastrales;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PlUserClavesCatastralesResource plUserClavesCatastralesResource = new PlUserClavesCatastralesResource(plUserClavesCatastralesService);
        this.restPlUserClavesCatastralesMockMvc = MockMvcBuilders.standaloneSetup(plUserClavesCatastralesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PlUserClavesCatastrales createEntity(EntityManager em) {
        PlUserClavesCatastrales plUserClavesCatastrales = new PlUserClavesCatastrales()
            .llave(DEFAULT_LLAVE);
        return plUserClavesCatastrales;
    }

    @Before
    public void initTest() {
        plUserClavesCatastrales = createEntity(em);
    }

    @Test
    @Transactional
    public void createPlUserClavesCatastrales() throws Exception {
        int databaseSizeBeforeCreate = plUserClavesCatastralesRepository.findAll().size();

        // Create the PlUserClavesCatastrales
        PlUserClavesCatastralesDTO plUserClavesCatastralesDTO = plUserClavesCatastralesMapper.toDto(plUserClavesCatastrales);
        restPlUserClavesCatastralesMockMvc.perform(post("/api/pl-user-claves-catastrales")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(plUserClavesCatastralesDTO)))
            .andExpect(status().isCreated());

        // Validate the PlUserClavesCatastrales in the database
        List<PlUserClavesCatastrales> plUserClavesCatastralesList = plUserClavesCatastralesRepository.findAll();
        assertThat(plUserClavesCatastralesList).hasSize(databaseSizeBeforeCreate + 1);
        PlUserClavesCatastrales testPlUserClavesCatastrales = plUserClavesCatastralesList.get(plUserClavesCatastralesList.size() - 1);
        assertThat(testPlUserClavesCatastrales.getLlave()).isEqualTo(DEFAULT_LLAVE);
    }

    @Test
    @Transactional
    public void createPlUserClavesCatastralesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = plUserClavesCatastralesRepository.findAll().size();

        // Create the PlUserClavesCatastrales with an existing ID
        plUserClavesCatastrales.setId(1L);
        PlUserClavesCatastralesDTO plUserClavesCatastralesDTO = plUserClavesCatastralesMapper.toDto(plUserClavesCatastrales);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPlUserClavesCatastralesMockMvc.perform(post("/api/pl-user-claves-catastrales")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(plUserClavesCatastralesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PlUserClavesCatastrales in the database
        List<PlUserClavesCatastrales> plUserClavesCatastralesList = plUserClavesCatastralesRepository.findAll();
        assertThat(plUserClavesCatastralesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkLlaveIsRequired() throws Exception {
        int databaseSizeBeforeTest = plUserClavesCatastralesRepository.findAll().size();
        // set the field null
        plUserClavesCatastrales.setLlave(null);

        // Create the PlUserClavesCatastrales, which fails.
        PlUserClavesCatastralesDTO plUserClavesCatastralesDTO = plUserClavesCatastralesMapper.toDto(plUserClavesCatastrales);

        restPlUserClavesCatastralesMockMvc.perform(post("/api/pl-user-claves-catastrales")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(plUserClavesCatastralesDTO)))
            .andExpect(status().isBadRequest());

        List<PlUserClavesCatastrales> plUserClavesCatastralesList = plUserClavesCatastralesRepository.findAll();
        assertThat(plUserClavesCatastralesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPlUserClavesCatastrales() throws Exception {
        // Initialize the database
        plUserClavesCatastralesRepository.saveAndFlush(plUserClavesCatastrales);

        // Get all the plUserClavesCatastralesList
        restPlUserClavesCatastralesMockMvc.perform(get("/api/pl-user-claves-catastrales?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(plUserClavesCatastrales.getId().intValue())))
            .andExpect(jsonPath("$.[*].llave").value(hasItem(DEFAULT_LLAVE.toString())));
    }

    @Test
    @Transactional
    public void getPlUserClavesCatastrales() throws Exception {
        // Initialize the database
        plUserClavesCatastralesRepository.saveAndFlush(plUserClavesCatastrales);

        // Get the plUserClavesCatastrales
        restPlUserClavesCatastralesMockMvc.perform(get("/api/pl-user-claves-catastrales/{id}", plUserClavesCatastrales.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(plUserClavesCatastrales.getId().intValue()))
            .andExpect(jsonPath("$.llave").value(DEFAULT_LLAVE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPlUserClavesCatastrales() throws Exception {
        // Get the plUserClavesCatastrales
        restPlUserClavesCatastralesMockMvc.perform(get("/api/pl-user-claves-catastrales/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePlUserClavesCatastrales() throws Exception {
        // Initialize the database
        plUserClavesCatastralesRepository.saveAndFlush(plUserClavesCatastrales);
        int databaseSizeBeforeUpdate = plUserClavesCatastralesRepository.findAll().size();

        // Update the plUserClavesCatastrales
        PlUserClavesCatastrales updatedPlUserClavesCatastrales = plUserClavesCatastralesRepository.findOne(plUserClavesCatastrales.getId());
        updatedPlUserClavesCatastrales
            .llave(UPDATED_LLAVE);
        PlUserClavesCatastralesDTO plUserClavesCatastralesDTO = plUserClavesCatastralesMapper.toDto(updatedPlUserClavesCatastrales);

        restPlUserClavesCatastralesMockMvc.perform(put("/api/pl-user-claves-catastrales")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(plUserClavesCatastralesDTO)))
            .andExpect(status().isOk());

        // Validate the PlUserClavesCatastrales in the database
        List<PlUserClavesCatastrales> plUserClavesCatastralesList = plUserClavesCatastralesRepository.findAll();
        assertThat(plUserClavesCatastralesList).hasSize(databaseSizeBeforeUpdate);
        PlUserClavesCatastrales testPlUserClavesCatastrales = plUserClavesCatastralesList.get(plUserClavesCatastralesList.size() - 1);
        assertThat(testPlUserClavesCatastrales.getLlave()).isEqualTo(UPDATED_LLAVE);
    }

    @Test
    @Transactional
    public void updateNonExistingPlUserClavesCatastrales() throws Exception {
        int databaseSizeBeforeUpdate = plUserClavesCatastralesRepository.findAll().size();

        // Create the PlUserClavesCatastrales
        PlUserClavesCatastralesDTO plUserClavesCatastralesDTO = plUserClavesCatastralesMapper.toDto(plUserClavesCatastrales);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPlUserClavesCatastralesMockMvc.perform(put("/api/pl-user-claves-catastrales")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(plUserClavesCatastralesDTO)))
            .andExpect(status().isCreated());

        // Validate the PlUserClavesCatastrales in the database
        List<PlUserClavesCatastrales> plUserClavesCatastralesList = plUserClavesCatastralesRepository.findAll();
        assertThat(plUserClavesCatastralesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePlUserClavesCatastrales() throws Exception {
        // Initialize the database
        plUserClavesCatastralesRepository.saveAndFlush(plUserClavesCatastrales);
        int databaseSizeBeforeDelete = plUserClavesCatastralesRepository.findAll().size();

        // Get the plUserClavesCatastrales
        restPlUserClavesCatastralesMockMvc.perform(delete("/api/pl-user-claves-catastrales/{id}", plUserClavesCatastrales.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PlUserClavesCatastrales> plUserClavesCatastralesList = plUserClavesCatastralesRepository.findAll();
        assertThat(plUserClavesCatastralesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PlUserClavesCatastrales.class);
        PlUserClavesCatastrales plUserClavesCatastrales1 = new PlUserClavesCatastrales();
        plUserClavesCatastrales1.setId(1L);
        PlUserClavesCatastrales plUserClavesCatastrales2 = new PlUserClavesCatastrales();
        plUserClavesCatastrales2.setId(plUserClavesCatastrales1.getId());
        assertThat(plUserClavesCatastrales1).isEqualTo(plUserClavesCatastrales2);
        plUserClavesCatastrales2.setId(2L);
        assertThat(plUserClavesCatastrales1).isNotEqualTo(plUserClavesCatastrales2);
        plUserClavesCatastrales1.setId(null);
        assertThat(plUserClavesCatastrales1).isNotEqualTo(plUserClavesCatastrales2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PlUserClavesCatastralesDTO.class);
        PlUserClavesCatastralesDTO plUserClavesCatastralesDTO1 = new PlUserClavesCatastralesDTO();
        plUserClavesCatastralesDTO1.setId(1L);
        PlUserClavesCatastralesDTO plUserClavesCatastralesDTO2 = new PlUserClavesCatastralesDTO();
        assertThat(plUserClavesCatastralesDTO1).isNotEqualTo(plUserClavesCatastralesDTO2);
        plUserClavesCatastralesDTO2.setId(plUserClavesCatastralesDTO1.getId());
        assertThat(plUserClavesCatastralesDTO1).isEqualTo(plUserClavesCatastralesDTO2);
        plUserClavesCatastralesDTO2.setId(2L);
        assertThat(plUserClavesCatastralesDTO1).isNotEqualTo(plUserClavesCatastralesDTO2);
        plUserClavesCatastralesDTO1.setId(null);
        assertThat(plUserClavesCatastralesDTO1).isNotEqualTo(plUserClavesCatastralesDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(plUserClavesCatastralesMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(plUserClavesCatastralesMapper.fromId(null)).isNull();
    }
}
