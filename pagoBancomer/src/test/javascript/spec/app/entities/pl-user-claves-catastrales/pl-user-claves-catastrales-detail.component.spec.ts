/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PagoBancomerTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { PlUserClavesCatastralesDetailComponent } from '../../../../../../main/webapp/app/entities/pl-user-claves-catastrales/pl-user-claves-catastrales-detail.component';
import { PlUserClavesCatastralesService } from '../../../../../../main/webapp/app/entities/pl-user-claves-catastrales/pl-user-claves-catastrales.service';
import { PlUserClavesCatastrales } from '../../../../../../main/webapp/app/entities/pl-user-claves-catastrales/pl-user-claves-catastrales.model';

describe('Component Tests', () => {

    describe('PlUserClavesCatastrales Management Detail Component', () => {
        let comp: PlUserClavesCatastralesDetailComponent;
        let fixture: ComponentFixture<PlUserClavesCatastralesDetailComponent>;
        let service: PlUserClavesCatastralesService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PagoBancomerTestModule],
                declarations: [PlUserClavesCatastralesDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PlUserClavesCatastralesService,
                    JhiEventManager
                ]
            }).overrideTemplate(PlUserClavesCatastralesDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PlUserClavesCatastralesDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PlUserClavesCatastralesService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new PlUserClavesCatastrales(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.plUserClavesCatastrales).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
