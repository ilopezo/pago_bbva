/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PagoBancomerTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { CatBancoDetailComponent } from '../../../../../../main/webapp/app/entities/cat-banco/cat-banco-detail.component';
import { CatBancoService } from '../../../../../../main/webapp/app/entities/cat-banco/cat-banco.service';
import { CatBanco } from '../../../../../../main/webapp/app/entities/cat-banco/cat-banco.model';

describe('Component Tests', () => {

    describe('CatBanco Management Detail Component', () => {
        let comp: CatBancoDetailComponent;
        let fixture: ComponentFixture<CatBancoDetailComponent>;
        let service: CatBancoService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PagoBancomerTestModule],
                declarations: [CatBancoDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    CatBancoService,
                    JhiEventManager
                ]
            }).overrideTemplate(CatBancoDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CatBancoDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CatBancoService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new CatBanco(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.catBanco).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
