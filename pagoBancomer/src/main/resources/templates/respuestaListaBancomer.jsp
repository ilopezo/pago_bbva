
<%@page import="com.worknest.pagobancomer.web.rest.util.Token"%>
<%@page import="java.util.List"%>
<%
    List<Token> listaTokens = (List<Token>) request.getAttribute("tokensRespuestas");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width" />
        <title>Servicios en línea</title>
        <link rel="stylesheet" href="pagoBancario.css" type="text/css" media="all" />
        <style>
            .margin1{
                margin-bottom: 3px;
            }
        </style>
    </head>
    <body class="cuerpo">
        <section id="titulo-seccion">
            <div class="row">
                <div id="titulo-columna" class="titulo large-12 columns">
                    <h3>Resultado de la aplicación de pagos</h3>
                </div>
            </div>
        </section>
        <section id="mainContent">
            <div class="row large-8 columns">
                <%
                    for (Token token : listaTokens) {
                %>
                <div data-alert class="alert-box <%=token.getOk() ? "success " : "alert "%> margin1">
                       <%=token.getMensaje()%> 
                </div>
                <%}%>
            </div>
        </div>
    </section>
</body>
</html>
