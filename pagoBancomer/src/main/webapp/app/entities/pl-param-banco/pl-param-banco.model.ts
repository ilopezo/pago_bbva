import { BaseEntity } from './../../shared';

export const enum TipoParamBanco {
    'IN',
    'OUT'
}

export class PlParamBanco implements BaseEntity {
    constructor(
        public id?: number,
        public nombre?: string,
        public tipo?: TipoParamBanco,
        public enUso?: boolean,
        public valorDefault?: string,
        public institucionBancariaId?: number,
    ) {
        this.enUso = false;
    }
}
