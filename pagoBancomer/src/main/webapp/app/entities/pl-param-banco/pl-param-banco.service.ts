import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { PlParamBanco } from './pl-param-banco.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PlParamBancoService {

    private resourceUrl = SERVER_API_URL + 'api/pl-param-bancos';

    constructor(private http: Http) { }

    create(plParamBanco: PlParamBanco): Observable<PlParamBanco> {
        const copy = this.convert(plParamBanco);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(plParamBanco: PlParamBanco): Observable<PlParamBanco> {
        const copy = this.convert(plParamBanco);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<PlParamBanco> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to PlParamBanco.
     */
    private convertItemFromServer(json: any): PlParamBanco {
        const entity: PlParamBanco = Object.assign(new PlParamBanco(), json);
        return entity;
    }

    /**
     * Convert a PlParamBanco to a JSON which can be sent to the server.
     */
    private convert(plParamBanco: PlParamBanco): PlParamBanco {
        const copy: PlParamBanco = Object.assign({}, plParamBanco);
        return copy;
    }
}
