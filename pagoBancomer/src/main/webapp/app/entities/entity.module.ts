import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { PagoBancomerPlUserClavesCatastralesModule } from './pl-user-claves-catastrales/pl-user-claves-catastrales.module';
import { PagoBancomerCatBancoModule } from './cat-banco/cat-banco.module';
import { PagoBancomerPlParamBancoModule } from './pl-param-banco/pl-param-banco.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        PagoBancomerPlUserClavesCatastralesModule,
        PagoBancomerCatBancoModule,
        PagoBancomerPlParamBancoModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PagoBancomerEntityModule {}
