import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { CatBanco } from './cat-banco.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class CatBancoService {

    private resourceUrl = SERVER_API_URL + 'api/cat-bancos';

    constructor(private http: Http) { }

    create(catBanco: CatBanco): Observable<CatBanco> {
        const copy = this.convert(catBanco);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(catBanco: CatBanco): Observable<CatBanco> {
        const copy = this.convert(catBanco);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<CatBanco> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to CatBanco.
     */
    private convertItemFromServer(json: any): CatBanco {
        const entity: CatBanco = Object.assign(new CatBanco(), json);
        return entity;
    }

    /**
     * Convert a CatBanco to a JSON which can be sent to the server.
     */
    private convert(catBanco: CatBanco): CatBanco {
        const copy: CatBanco = Object.assign({}, catBanco);
        return copy;
    }
}
