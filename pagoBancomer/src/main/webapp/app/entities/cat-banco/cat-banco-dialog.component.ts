import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CatBanco } from './cat-banco.model';
import { CatBancoPopupService } from './cat-banco-popup.service';
import { CatBancoService } from './cat-banco.service';

@Component({
    selector: 'jhi-cat-banco-dialog',
    templateUrl: './cat-banco-dialog.component.html'
})
export class CatBancoDialogComponent implements OnInit {

    catBanco: CatBanco;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private catBancoService: CatBancoService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.catBanco.id !== undefined) {
            this.subscribeToSaveResponse(
                this.catBancoService.update(this.catBanco));
        } else {
            this.subscribeToSaveResponse(
                this.catBancoService.create(this.catBanco));
        }
    }

    private subscribeToSaveResponse(result: Observable<CatBanco>) {
        result.subscribe((res: CatBanco) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: CatBanco) {
        this.eventManager.broadcast({ name: 'catBancoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-cat-banco-popup',
    template: ''
})
export class CatBancoPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private catBancoPopupService: CatBancoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.catBancoPopupService
                    .open(CatBancoDialogComponent as Component, params['id']);
            } else {
                this.catBancoPopupService
                    .open(CatBancoDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
