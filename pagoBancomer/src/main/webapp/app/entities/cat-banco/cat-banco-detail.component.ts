import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { CatBanco } from './cat-banco.model';
import { CatBancoService } from './cat-banco.service';

@Component({
    selector: 'jhi-cat-banco-detail',
    templateUrl: './cat-banco-detail.component.html'
})
export class CatBancoDetailComponent implements OnInit, OnDestroy {

    catBanco: CatBanco;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private catBancoService: CatBancoService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCatBancos();
    }

    load(id) {
        this.catBancoService.find(id).subscribe((catBanco) => {
            this.catBanco = catBanco;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCatBancos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'catBancoListModification',
            (response) => this.load(this.catBanco.id)
        );
    }
}
