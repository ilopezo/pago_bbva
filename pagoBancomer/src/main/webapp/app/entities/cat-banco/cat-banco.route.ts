import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CatBancoComponent } from './cat-banco.component';
import { CatBancoDetailComponent } from './cat-banco-detail.component';
import { CatBancoPopupComponent } from './cat-banco-dialog.component';
import { CatBancoDeletePopupComponent } from './cat-banco-delete-dialog.component';

export const catBancoRoute: Routes = [
    {
        path: 'cat-banco',
        component: CatBancoComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagoBancomerApp.catBanco.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'cat-banco/:id',
        component: CatBancoDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagoBancomerApp.catBanco.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const catBancoPopupRoute: Routes = [
    {
        path: 'cat-banco-new',
        component: CatBancoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagoBancomerApp.catBanco.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'cat-banco/:id/edit',
        component: CatBancoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagoBancomerApp.catBanco.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'cat-banco/:id/delete',
        component: CatBancoDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagoBancomerApp.catBanco.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
