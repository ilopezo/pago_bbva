import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CatBanco } from './cat-banco.model';
import { CatBancoPopupService } from './cat-banco-popup.service';
import { CatBancoService } from './cat-banco.service';

@Component({
    selector: 'jhi-cat-banco-delete-dialog',
    templateUrl: './cat-banco-delete-dialog.component.html'
})
export class CatBancoDeleteDialogComponent {

    catBanco: CatBanco;

    constructor(
        private catBancoService: CatBancoService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.catBancoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'catBancoListModification',
                content: 'Deleted an catBanco'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-cat-banco-delete-popup',
    template: ''
})
export class CatBancoDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private catBancoPopupService: CatBancoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.catBancoPopupService
                .open(CatBancoDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
