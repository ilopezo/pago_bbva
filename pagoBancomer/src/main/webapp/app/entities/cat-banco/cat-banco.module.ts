import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PagoBancomerSharedModule } from '../../shared';
import {
    CatBancoService,
    CatBancoPopupService,
    CatBancoComponent,
    CatBancoDetailComponent,
    CatBancoDialogComponent,
    CatBancoPopupComponent,
    CatBancoDeletePopupComponent,
    CatBancoDeleteDialogComponent,
    catBancoRoute,
    catBancoPopupRoute,
} from './';

const ENTITY_STATES = [
    ...catBancoRoute,
    ...catBancoPopupRoute,
];

@NgModule({
    imports: [
        PagoBancomerSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CatBancoComponent,
        CatBancoDetailComponent,
        CatBancoDialogComponent,
        CatBancoDeleteDialogComponent,
        CatBancoPopupComponent,
        CatBancoDeletePopupComponent,
    ],
    entryComponents: [
        CatBancoComponent,
        CatBancoDialogComponent,
        CatBancoPopupComponent,
        CatBancoDeleteDialogComponent,
        CatBancoDeletePopupComponent,
    ],
    providers: [
        CatBancoService,
        CatBancoPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PagoBancomerCatBancoModule {}
