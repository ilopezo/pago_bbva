import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { CatBanco } from './cat-banco.model';
import { CatBancoService } from './cat-banco.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-cat-banco',
    templateUrl: './cat-banco.component.html'
})
export class CatBancoComponent implements OnInit, OnDestroy {
catBancos: CatBanco[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private catBancoService: CatBancoService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.catBancoService.query().subscribe(
            (res: ResponseWrapper) => {
                this.catBancos = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCatBancos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: CatBanco) {
        return item.id;
    }
    registerChangeInCatBancos() {
        this.eventSubscriber = this.eventManager.subscribe('catBancoListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
