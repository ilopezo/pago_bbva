import { BaseEntity } from './../../shared';

export class CatBanco implements BaseEntity {
    constructor(
        public id?: number,
        public descripcion?: string,
    ) {
    }
}
