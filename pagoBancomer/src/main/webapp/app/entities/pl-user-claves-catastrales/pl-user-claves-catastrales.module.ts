import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PagoBancomerSharedModule } from '../../shared';
import { PagoBancomerAdminModule } from '../../admin/admin.module';
import {
    PlUserClavesCatastralesService,
    PlUserClavesCatastralesPopupService,
    PlUserClavesCatastralesComponent,
    PlUserClavesCatastralesDetailComponent,
    PlUserClavesCatastralesDialogComponent,
    PlUserClavesCatastralesPopupComponent,
    PlUserClavesCatastralesDeletePopupComponent,
    PlUserClavesCatastralesDeleteDialogComponent,
    plUserClavesCatastralesRoute,
    plUserClavesCatastralesPopupRoute,
} from './';

const ENTITY_STATES = [
    ...plUserClavesCatastralesRoute,
    ...plUserClavesCatastralesPopupRoute,
];

@NgModule({
    imports: [
        PagoBancomerSharedModule,
        PagoBancomerAdminModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PlUserClavesCatastralesComponent,
        PlUserClavesCatastralesDetailComponent,
        PlUserClavesCatastralesDialogComponent,
        PlUserClavesCatastralesDeleteDialogComponent,
        PlUserClavesCatastralesPopupComponent,
        PlUserClavesCatastralesDeletePopupComponent,
    ],
    entryComponents: [
        PlUserClavesCatastralesComponent,
        PlUserClavesCatastralesDialogComponent,
        PlUserClavesCatastralesPopupComponent,
        PlUserClavesCatastralesDeleteDialogComponent,
        PlUserClavesCatastralesDeletePopupComponent,
    ],
    providers: [
        PlUserClavesCatastralesService,
        PlUserClavesCatastralesPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PagoBancomerPlUserClavesCatastralesModule {}
