import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { PlUserClavesCatastrales } from './pl-user-claves-catastrales.model';
import { PlUserClavesCatastralesService } from './pl-user-claves-catastrales.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-pl-user-claves-catastrales',
    templateUrl: './pl-user-claves-catastrales.component.html'
})
export class PlUserClavesCatastralesComponent implements OnInit, OnDestroy {
plUserClavesCatastrales: PlUserClavesCatastrales[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private plUserClavesCatastralesService: PlUserClavesCatastralesService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.plUserClavesCatastralesService.query().subscribe(
            (res: ResponseWrapper) => {
                this.plUserClavesCatastrales = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPlUserClavesCatastrales();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PlUserClavesCatastrales) {
        return item.id;
    }
    registerChangeInPlUserClavesCatastrales() {
        this.eventSubscriber = this.eventManager.subscribe('plUserClavesCatastralesListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
