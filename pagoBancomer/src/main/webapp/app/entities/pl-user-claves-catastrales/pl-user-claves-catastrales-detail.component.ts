import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { PlUserClavesCatastrales } from './pl-user-claves-catastrales.model';
import { PlUserClavesCatastralesService } from './pl-user-claves-catastrales.service';

@Component({
    selector: 'jhi-pl-user-claves-catastrales-detail',
    templateUrl: './pl-user-claves-catastrales-detail.component.html'
})
export class PlUserClavesCatastralesDetailComponent implements OnInit, OnDestroy {

    plUserClavesCatastrales: PlUserClavesCatastrales;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private plUserClavesCatastralesService: PlUserClavesCatastralesService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPlUserClavesCatastrales();
    }

    load(id) {
        this.plUserClavesCatastralesService.find(id).subscribe((plUserClavesCatastrales) => {
            this.plUserClavesCatastrales = plUserClavesCatastrales;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPlUserClavesCatastrales() {
        this.eventSubscriber = this.eventManager.subscribe(
            'plUserClavesCatastralesListModification',
            (response) => this.load(this.plUserClavesCatastrales.id)
        );
    }
}
