import { BaseEntity } from './../../shared';

export class PlUserClavesCatastrales implements BaseEntity {
    constructor(
        public id?: number,
        public llave?: string,
        public userId?: number,
    ) {
    }
}
