import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { PlUserClavesCatastrales } from './pl-user-claves-catastrales.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PlUserClavesCatastralesService {

    private resourceUrl = SERVER_API_URL + 'api/pl-user-claves-catastrales';

    constructor(private http: Http) { }

    create(plUserClavesCatastrales: PlUserClavesCatastrales): Observable<PlUserClavesCatastrales> {
        const copy = this.convert(plUserClavesCatastrales);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(plUserClavesCatastrales: PlUserClavesCatastrales): Observable<PlUserClavesCatastrales> {
        const copy = this.convert(plUserClavesCatastrales);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<PlUserClavesCatastrales> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to PlUserClavesCatastrales.
     */
    private convertItemFromServer(json: any): PlUserClavesCatastrales {
        const entity: PlUserClavesCatastrales = Object.assign(new PlUserClavesCatastrales(), json);
        return entity;
    }

    /**
     * Convert a PlUserClavesCatastrales to a JSON which can be sent to the server.
     */
    private convert(plUserClavesCatastrales: PlUserClavesCatastrales): PlUserClavesCatastrales {
        const copy: PlUserClavesCatastrales = Object.assign({}, plUserClavesCatastrales);
        return copy;
    }
}
