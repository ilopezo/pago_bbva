import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PlUserClavesCatastrales } from './pl-user-claves-catastrales.model';
import { PlUserClavesCatastralesService } from './pl-user-claves-catastrales.service';

@Injectable()
export class PlUserClavesCatastralesPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private plUserClavesCatastralesService: PlUserClavesCatastralesService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.plUserClavesCatastralesService.find(id).subscribe((plUserClavesCatastrales) => {
                    this.ngbModalRef = this.plUserClavesCatastralesModalRef(component, plUserClavesCatastrales);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.plUserClavesCatastralesModalRef(component, new PlUserClavesCatastrales());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    plUserClavesCatastralesModalRef(component: Component, plUserClavesCatastrales: PlUserClavesCatastrales): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.plUserClavesCatastrales = plUserClavesCatastrales;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
