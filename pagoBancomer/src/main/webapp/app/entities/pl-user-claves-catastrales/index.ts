export * from './pl-user-claves-catastrales.model';
export * from './pl-user-claves-catastrales-popup.service';
export * from './pl-user-claves-catastrales.service';
export * from './pl-user-claves-catastrales-dialog.component';
export * from './pl-user-claves-catastrales-delete-dialog.component';
export * from './pl-user-claves-catastrales-detail.component';
export * from './pl-user-claves-catastrales.component';
export * from './pl-user-claves-catastrales.route';
