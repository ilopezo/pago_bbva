import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PlUserClavesCatastrales } from './pl-user-claves-catastrales.model';
import { PlUserClavesCatastralesPopupService } from './pl-user-claves-catastrales-popup.service';
import { PlUserClavesCatastralesService } from './pl-user-claves-catastrales.service';
import { User, UserService } from '../../shared';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-pl-user-claves-catastrales-dialog',
    templateUrl: './pl-user-claves-catastrales-dialog.component.html'
})
export class PlUserClavesCatastralesDialogComponent implements OnInit {

    plUserClavesCatastrales: PlUserClavesCatastrales;
    isSaving: boolean;

    users: User[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private plUserClavesCatastralesService: PlUserClavesCatastralesService,
        private userService: UserService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.userService.query()
            .subscribe((res: ResponseWrapper) => { this.users = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.plUserClavesCatastrales.id !== undefined) {
            this.subscribeToSaveResponse(
                this.plUserClavesCatastralesService.update(this.plUserClavesCatastrales));
        } else {
            this.subscribeToSaveResponse(
                this.plUserClavesCatastralesService.create(this.plUserClavesCatastrales));
        }
    }

    private subscribeToSaveResponse(result: Observable<PlUserClavesCatastrales>) {
        result.subscribe((res: PlUserClavesCatastrales) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: PlUserClavesCatastrales) {
        this.eventManager.broadcast({ name: 'plUserClavesCatastralesListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-pl-user-claves-catastrales-popup',
    template: ''
})
export class PlUserClavesCatastralesPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private plUserClavesCatastralesPopupService: PlUserClavesCatastralesPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.plUserClavesCatastralesPopupService
                    .open(PlUserClavesCatastralesDialogComponent as Component, params['id']);
            } else {
                this.plUserClavesCatastralesPopupService
                    .open(PlUserClavesCatastralesDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
