import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PlUserClavesCatastrales } from './pl-user-claves-catastrales.model';
import { PlUserClavesCatastralesPopupService } from './pl-user-claves-catastrales-popup.service';
import { PlUserClavesCatastralesService } from './pl-user-claves-catastrales.service';

@Component({
    selector: 'jhi-pl-user-claves-catastrales-delete-dialog',
    templateUrl: './pl-user-claves-catastrales-delete-dialog.component.html'
})
export class PlUserClavesCatastralesDeleteDialogComponent {

    plUserClavesCatastrales: PlUserClavesCatastrales;

    constructor(
        private plUserClavesCatastralesService: PlUserClavesCatastralesService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.plUserClavesCatastralesService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'plUserClavesCatastralesListModification',
                content: 'Deleted an plUserClavesCatastrales'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-pl-user-claves-catastrales-delete-popup',
    template: ''
})
export class PlUserClavesCatastralesDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private plUserClavesCatastralesPopupService: PlUserClavesCatastralesPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.plUserClavesCatastralesPopupService
                .open(PlUserClavesCatastralesDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
