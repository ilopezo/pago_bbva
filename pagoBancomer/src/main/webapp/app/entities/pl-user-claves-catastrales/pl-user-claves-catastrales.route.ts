import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PlUserClavesCatastralesComponent } from './pl-user-claves-catastrales.component';
import { PlUserClavesCatastralesDetailComponent } from './pl-user-claves-catastrales-detail.component';
import { PlUserClavesCatastralesPopupComponent } from './pl-user-claves-catastrales-dialog.component';
import { PlUserClavesCatastralesDeletePopupComponent } from './pl-user-claves-catastrales-delete-dialog.component';

export const plUserClavesCatastralesRoute: Routes = [
    {
        path: 'pl-user-claves-catastrales',
        component: PlUserClavesCatastralesComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagoBancomerApp.plUserClavesCatastrales.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'pl-user-claves-catastrales/:id',
        component: PlUserClavesCatastralesDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagoBancomerApp.plUserClavesCatastrales.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const plUserClavesCatastralesPopupRoute: Routes = [
    {
        path: 'pl-user-claves-catastrales-new',
        component: PlUserClavesCatastralesPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagoBancomerApp.plUserClavesCatastrales.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'pl-user-claves-catastrales/:id/edit',
        component: PlUserClavesCatastralesPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagoBancomerApp.plUserClavesCatastrales.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'pl-user-claves-catastrales/:id/delete',
        component: PlUserClavesCatastralesDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pagoBancomerApp.plUserClavesCatastrales.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
