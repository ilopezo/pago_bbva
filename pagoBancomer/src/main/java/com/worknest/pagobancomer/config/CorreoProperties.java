package com.worknest.pagobancomer.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "correos", ignoreUnknownFields = false)
public class CorreoProperties {

	private String textoLinkPredial;
	private String textoConceptosCobrosPredial;
	private String textoLinkTrasmisionDominio;
	private String textoConceptosCobrosTransmisionDominio;

	private String llaveOrigen;
	private String llaveTitulo;
	private String llaveSistema;
	private String llaveUrlDetallePago;
	private String llaveAvisoPrivacidad;
	private String llavePoliticasUso;
	private String llaveLogo;

	public String getLlaveLogo() {
		return llaveLogo;
	}

	public void setLlaveLogo(String llaveLogo) {
		this.llaveLogo = llaveLogo;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	private String clave;

	public String getLlavePoliticasUso() {
		return llavePoliticasUso;
	}

	public void setLlavePoliticasUso(String llavePoliticasUso) {
		this.llavePoliticasUso = llavePoliticasUso;
	}

	private String urlBase;

	public String getLlaveOrigen() {
		return llaveOrigen;
	}

	public void setLlaveOrigen(String llaveOrigen) {
		this.llaveOrigen = llaveOrigen;
	}

	public String getLlaveTitulo() {
		return llaveTitulo;
	}

	public void setLlaveTitulo(String llaveTitulo) {
		this.llaveTitulo = llaveTitulo;
	}

	public String getLlaveSistema() {
		return llaveSistema;
	}

	public void setLlaveSistema(String llaveSistema) {
		this.llaveSistema = llaveSistema;
	}

	public String getLlaveUrlDetallePago() {
		return llaveUrlDetallePago;
	}

	public void setLlaveUrlDetallePago(String llaveUrlDetallePago) {
		this.llaveUrlDetallePago = llaveUrlDetallePago;
	}

	public String getLlaveAvisoPrivacidad() {
		return llaveAvisoPrivacidad;
	}

	public void setLlaveAvisoPrivacidad(String llaveAvisoPrivacidad) {
		this.llaveAvisoPrivacidad = llaveAvisoPrivacidad;
	}

	public String getTextoLinkPredial() {
		return textoLinkPredial;
	}

	public void setTextoLinkPredial(String textoLinkPredial) {
		this.textoLinkPredial = textoLinkPredial;
	}

	public String getTextoConceptosCobrosPredial() {
		return textoConceptosCobrosPredial;
	}

	public void setTextoConceptosCobrosPredial(String textoConceptosCobrosPredial) {
		this.textoConceptosCobrosPredial = textoConceptosCobrosPredial;
	}

	public String getTextoLinkTrasmisionDominio() {
		return textoLinkTrasmisionDominio;
	}

	public void setTextoLinkTrasmisionDominio(String textoLinkTrasmisionDominio) {
		this.textoLinkTrasmisionDominio = textoLinkTrasmisionDominio;
	}

	public String getTextoConceptosCobrosTransmisionDominio() {
		return textoConceptosCobrosTransmisionDominio;
	}

	public void setTextoConceptosCobrosTransmisionDominio(String textoConceptosCobrosTransmisionDominio) {
		this.textoConceptosCobrosTransmisionDominio = textoConceptosCobrosTransmisionDominio;
	}

	public String getUrlBase() {
		return urlBase;
	}

	public void setUrlBase(String urlBase) {
		this.urlBase = urlBase;
	}

}
