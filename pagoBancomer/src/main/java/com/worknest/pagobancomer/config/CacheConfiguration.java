package com.worknest.pagobancomer.config;

import java.util.concurrent.TimeUnit;

import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.github.jhipster.config.JHipsterProperties;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache("users", jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.PersistentToken.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.User.class.getName() + ".persistentTokens", jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.User.class.getName() + ".claves", jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.CatGrupoConcepto.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.CatConcepto.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.Liquidacion.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.LiquidacionEmisor.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.LiquidacionPredial.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.LiquidacionDesConcepto.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.Liquidacion.class.getName() + ".desgloceConceptos", jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.PlCarro.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.PlUserClavesCatastrales.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.PlCarroDet.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.PlCarroHist.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.PlCarroDetHist.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.PlIntentoPago.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.CatBanco.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.PlParamBanco.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.PlParamEnvio.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.PlParamRespuesta.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.PlRespuestaBanco.class.getName(), jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.PlCarro.class.getName() + ".users", jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.PlCarro.class.getName() + ".detalles", jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.PlCarroHist.class.getName() + ".detalles", jcacheConfiguration);
            cm.createCache(com.worknest.pagobancomer.domain.Recibo.class.getName(), jcacheConfiguration);

            
            // jhipster-needle-ehcache-add-entry
        };
    }
}
