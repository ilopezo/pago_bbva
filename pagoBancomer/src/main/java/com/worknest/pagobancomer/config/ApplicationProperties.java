package com.worknest.pagobancomer.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Pago Bancomer.
 * <p>
 * Properties are configured in the application.yml file. See
 * {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
    //banco
    private String urlpasarela;
    private String clave;
    //sistema caja
    private String host;
    private String port;
    private String lpgenera;
    private String getadeudos;
    private String getliq;
    private String deleteLiq;
    private String pagarLiq;
    //usuario y contraseña para autenticacion a los sitemas
    private String usuario;
    private String pass;
    private String autenticate;
    //sistema usuarios
    private String urlusuario;
    private String login;
    private String account;    
    private String infoperfil;
    private String modiperfil;
   

    public String getHost() {
            return host;
    }

    public void setHost(String host) {
            this.host = host;
    }

    public String getUrlpasarela() {
            return urlpasarela;
    }

    public void setUrlpasarela(String urlpasarela) {
            this.urlpasarela = urlpasarela;
    }

    public String getClave() {
            return clave;
    }

    public void setClave(String clave) {
            this.clave = clave;
    }

    public String getPort() {
            return port;
    }

    public void setPort(String port) {
            this.port = port;
    }

    public String getAutenticate() {
            return autenticate;
    }

    public void setAutenticate(String autenticate) {
            this.autenticate = autenticate;
    }

    public String getLpgenera() {
            return lpgenera;
    }

    public void setLpgenera(String lpgenera) {
            this.lpgenera = lpgenera;
    }

    public String getUsuario() {
            return usuario;
    }

    public void setUsuario(String usuario) {
            this.usuario = usuario;
    }

    public String getPass() {
            return pass;
    }

    public String getUrlusuario() {
        return urlusuario;
    }

    public void setUrlusuario(String urlusuario) {
        this.urlusuario = urlusuario;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }    

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getInfoperfil() {
        return infoperfil;
    }

    public void setInfoperfil(String infoperfil) {
        this.infoperfil = infoperfil;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getDeleteLiq() {
        return deleteLiq;
    }

    public void setDeleteLiq(String deleteLiq) {
        this.deleteLiq = deleteLiq;
    }


    public String getPagarLiq() {
        return pagarLiq;
    }

    public void setPagarLiq(String pagarLiq) {
        this.pagarLiq = pagarLiq;
    }

    public String getGetadeudos() {
        return getadeudos;
    }

    public void setGetadeudos(String getadeudos) {
        this.getadeudos = getadeudos;
    }

    public String getModiperfil() {
        return modiperfil;
    }

    public void setModiperfil(String modiperfil) {
        this.modiperfil = modiperfil;
    }

    public String getGetliq() {
        return getliq;
    }

    public void setGetliq(String getliq) {
        this.getliq = getliq;
    }
    
}
