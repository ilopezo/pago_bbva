package com.worknest.pagobancomer.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.worknest.pagobancomer.domain.enumeration.StatusIntentoPago;

/**
 * A PlIntentoPago.
 */
@Entity
@Table(name = "pl_intento_pago")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PlIntentoPago implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "fecha", nullable = false)
    private ZonedDateTime fecha;

    @Column(name = "enviado")
    private Boolean enviado;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StatusIntentoPago status;

    @Size(max = 30)
    @Column(name = "auth", length = 30)
    private String auth;

    @ManyToOne
    private PlCarroHist historialcarro;

    public PlIntentoPago() {
    }

    public PlIntentoPago(Long id, ZonedDateTime fecha, Boolean enviado, StatusIntentoPago status, String auth, PlCarroHist historialcarro) {
        this.id = id;
        this.fecha = fecha;
        this.enviado = enviado;
        this.status = status;
        this.auth = auth;
        this.historialcarro = historialcarro;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getFecha() {
        return fecha;
    }

    public PlIntentoPago fecha(ZonedDateTime fecha) {
        this.fecha = fecha;
        return this;
    }

    public void setFecha(ZonedDateTime fecha) {
        this.fecha = fecha;
    }

    public Boolean isEnviado() {
        return enviado;
    }

    public PlIntentoPago enviado(Boolean enviado) {
        this.enviado = enviado;
        return this;
    }

    public void setEnviado(Boolean enviado) {
        this.enviado = enviado;
    }

    public StatusIntentoPago getStatus() {
        return status;
    }

    public PlIntentoPago status(StatusIntentoPago status) {
        this.status = status;
        return this;
    }

    public void setStatus(StatusIntentoPago status) {
        this.status = status;
    }

    public String getAuth() {
        return auth;
    }

    public PlIntentoPago auth(String auth) {
        this.auth = auth;
        return this;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public PlCarroHist getHistorialcarro() {
        return historialcarro;
    }

    public PlIntentoPago historialcarro(PlCarroHist plCarroHist) {
        this.historialcarro = plCarroHist;
        return this;
    }

    public void setHistorialcarro(PlCarroHist plCarroHist) {
        this.historialcarro = plCarroHist;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PlIntentoPago plIntentoPago = (PlIntentoPago) o;
        if (plIntentoPago.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plIntentoPago.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlIntentoPago{" +
            "id=" + getId() +
            ", fecha='" + getFecha() + "'" +
            ", enviado='" + isEnviado() + "'" +
            ", status='" + getStatus() + "'" +
            ", auth='" + getAuth() + "'" +
            "}";
    }
}
