package com.worknest.pagobancomer.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A PlRespuestaBanco.
 */
@Entity
@Table(name = "pl_respuesta_banco")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PlRespuestaBanco implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "exitoso", nullable = false)
    private Boolean exitoso;

    @NotNull
    @Column(name = "fecha", nullable = false)
    private ZonedDateTime fecha;

    @ManyToOne
    private PlIntentoPago intentoPago;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isExitoso() {
        return exitoso;
    }

    public PlRespuestaBanco exitoso(Boolean exitoso) {
        this.exitoso = exitoso;
        return this;
    }

    public void setExitoso(Boolean exitoso) {
        this.exitoso = exitoso;
    }

    public ZonedDateTime getFecha() {
        return fecha;
    }

    public PlRespuestaBanco fecha(ZonedDateTime fecha) {
        this.fecha = fecha;
        return this;
    }

    public void setFecha(ZonedDateTime fecha) {
        this.fecha = fecha;
    }

    public PlIntentoPago getIntentoPago() {
        return intentoPago;
    }

    public PlRespuestaBanco intentoPago(PlIntentoPago plIntentoPago) {
        this.intentoPago = plIntentoPago;
        return this;
    }

    public void setIntentoPago(PlIntentoPago plIntentoPago) {
        this.intentoPago = plIntentoPago;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PlRespuestaBanco plRespuestaBanco = (PlRespuestaBanco) o;
        if (plRespuestaBanco.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plRespuestaBanco.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlRespuestaBanco{" +
            "id=" + getId() +
            ", exitoso='" + isExitoso() + "'" +
            ", fecha='" + getFecha() + "'" +
            "}";
    }
}
