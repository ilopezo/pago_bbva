package com.worknest.pagobancomer.domain.enumeration;

/**
 * The Tel enumeration.
 */
public enum Tel {
    FIJO, MOVIL, FAX
}
