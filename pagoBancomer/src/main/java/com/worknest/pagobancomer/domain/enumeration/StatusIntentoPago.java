package com.worknest.pagobancomer.domain.enumeration;

/**
 * The StatusIntentoPago enumeration.
 */
public enum StatusIntentoPago {
    DISPERSA, ACEPTADA, RECHAZADA, ERROR
}
