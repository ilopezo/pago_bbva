package com.worknest.pagobancomer.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.worknest.pagobancomer.domain.enumeration.EstatusRecibo;

import io.swagger.annotations.ApiModel;

/**
 * Recibo
 */
@ApiModel(description = "Recibo")
@Entity
@Table(name = "recibo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Recibo implements Serializable {

    private static final long serialVersionUID = 1L;

    public Recibo() {
    }

    public Recibo(ZonedDateTime fecha, EstatusRecibo estatus, Liquidacion liquidacion) {
        this.fecha = fecha;
        this.estatus = estatus;
        this.liquidacion = liquidacion;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "fecha", nullable = false)
    private ZonedDateTime fecha;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "estatus", nullable = false)
    private EstatusRecibo estatus;

    @NotNull
    @Column(name = "folio_id", nullable = false)
    private Long folioId;

    @NotNull
    @Column(name = "caja_turno_id", nullable = false)
    private Long cajaTurnoId;

    @ManyToOne(optional = false)
    @NotNull
    private Liquidacion liquidacion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getFecha() {
        return fecha;
    }

    public Recibo fecha(ZonedDateTime fecha) {
        this.fecha = fecha;
        return this;
    }

    public void setFecha(ZonedDateTime fecha) {
        this.fecha = fecha;
    }

    public EstatusRecibo getEstatus() {
        return estatus;
    }

    public Recibo estatus(EstatusRecibo estatus) {
        this.estatus = estatus;
        return this;
    }

    public void setEstatus(EstatusRecibo estatus) {
        this.estatus = estatus;
    }

    public Liquidacion getLiquidacion() {
        return liquidacion;
    }

    public Recibo liquidacion(Liquidacion liquidacion) {
        this.liquidacion = liquidacion;
        return this;
    }

    public void setLiquidacion(Liquidacion liquidacion) {
        this.liquidacion = liquidacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Recibo recibo = (Recibo) o;
        if (recibo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), recibo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Recibo{" +
            "id=" + getId() +
            ", fecha='" + getFecha() + "'" +
            ", estatus='" + getEstatus() + "'" +
            "}";
    }
}
