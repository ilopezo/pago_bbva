package com.worknest.pagobancomer.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A PlCarro.
 */
@Entity
@Table(name = "pl_carro")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PlCarro implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "fecha", nullable = false)
    private LocalDate fecha;

    @NotNull
    @Column(name = "user_id", nullable = true)
    private Long user;

    @OneToMany(mappedBy = "carro")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PlCarroDet> detalles = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public PlCarro fecha(LocalDate fecha) {
        this.fecha = fecha;
        return this;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Long getUser() {
        return user;
    }

    public PlCarro user(Long user) {
        this.user = user;
        return this;
    }

    public void setUser(Long user) {
        this.user = user;
    }

    public Set<PlCarroDet> getDetalles() {
        return detalles;
    }

    public PlCarro detalles(Set<PlCarroDet> plCarroDets) {
        this.detalles = plCarroDets;
        return this;
    }

    public PlCarro addDetalle(PlCarroDet plCarroDet) {
        this.detalles.add(plCarroDet);
        plCarroDet.setCarro(this);
        return this;
    }

    public PlCarro removeDetalle(PlCarroDet plCarroDet) {
        this.detalles.remove(plCarroDet);
        plCarroDet.setCarro(null);
        return this;
    }

    public void setDetalles(Set<PlCarroDet> plCarroDets) {
        this.detalles = plCarroDets;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PlCarro plCarro = (PlCarro) o;
        if (plCarro.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plCarro.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlCarro{" +
            "id=" + getId() +
            ", fecha='" + getFecha() + "'" +
            "}";
    }
}
