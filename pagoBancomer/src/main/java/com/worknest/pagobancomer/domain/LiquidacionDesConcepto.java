package com.worknest.pagobancomer.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A LiquidacionDesConcepto.
 */
@Entity
@Table(name = "liquidacion_des_concepto")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LiquidacionDesConcepto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "orden", nullable = false)
    private Integer orden;

    @NotNull
    @Column(name = "importe", precision=10, scale=2, nullable = false)
    private BigDecimal importe;

    @ManyToOne(optional = false)
    @NotNull
    private Liquidacion liquidacion;

    @ManyToOne(optional = false)
    @NotNull
    private CatConcepto concepto;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getOrden() {
        return orden;
    }

    public LiquidacionDesConcepto orden(Integer orden) {
        this.orden = orden;
        return this;
    }


    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public LiquidacionDesConcepto importe(BigDecimal importe) {
        this.importe = importe;
        return this;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public Liquidacion getLiquidacion() {
        return liquidacion;
    }

    public LiquidacionDesConcepto liquidacion(Liquidacion liquidacion) {
        this.liquidacion = liquidacion;
        return this;
    }

    public void setLiquidacion(Liquidacion liquidacion) {
        this.liquidacion = liquidacion;
    }

    public CatConcepto getConcepto() {
        return concepto;
    }

    public LiquidacionDesConcepto concepto(CatConcepto catConcepto) {
        this.concepto = catConcepto;
        return this;
    }

    public void setConcepto(CatConcepto catConcepto) {
        this.concepto = catConcepto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LiquidacionDesConcepto liquidacionDesConcepto = (LiquidacionDesConcepto) o;
        if (liquidacionDesConcepto.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), liquidacionDesConcepto.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LiquidacionDesConcepto{" +
            "id=" + getId() +
            ", orden='" + getOrden() + "'" +
            ", importe='" + getImporte() + "'" +
            "}";
    }
}
