package com.worknest.pagobancomer.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A PlCarroHist.
 */
@Entity
@Table(name = "pl_carro_hist")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PlCarroHist implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "fecha_envio", nullable = false)
    private ZonedDateTime fechaEnvio;

    @NotNull
    @Size(max = 250)
    @Column(name = "referencia", length = 250, nullable = false)
    private String referencia;

    @ManyToOne
    private PlCarro carro;

    @OneToMany(mappedBy = "carro")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PlCarroDetHist> detalles = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getFechaEnvio() {
        return fechaEnvio;
    }

    public PlCarroHist fechaEnvio(ZonedDateTime fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
        return this;
    }

    public void setFechaEnvio(ZonedDateTime fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public String getReferencia() {
        return referencia;
    }

    public PlCarroHist referencia(String referencia) {
        this.referencia = referencia;
        return this;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public PlCarro getCarro() {
        return carro;
    }

    public PlCarroHist carro(PlCarro plCarro) {
        this.carro = plCarro;
        return this;
    }

    public void setCarro(PlCarro plCarro) {
        this.carro = plCarro;
    }

    public Set<PlCarroDetHist> getDetalles() {
        return detalles;
    }

    public PlCarroHist detalles(Set<PlCarroDetHist> plCarroDetHists) {
        this.detalles = plCarroDetHists;
        return this;
    }

    public PlCarroHist addDetalle(PlCarroDetHist plCarroDetHist) {
        this.detalles.add(plCarroDetHist);
        plCarroDetHist.setCarro(this);
        return this;
    }

    public PlCarroHist removeDetalle(PlCarroDetHist plCarroDetHist) {
        this.detalles.remove(plCarroDetHist);
        plCarroDetHist.setCarro(null);
        return this;
    }

    public void setDetalles(Set<PlCarroDetHist> plCarroDetHists) {
        this.detalles = plCarroDetHists;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PlCarroHist plCarroHist = (PlCarroHist) o;
        if (plCarroHist.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plCarroHist.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlCarroHist{" +
            "id=" + getId() +
            ", fechaEnvio='" + getFechaEnvio() + "'" +
            ", referencia='" + getReferencia() + "'" +
            "}";
    }
}
