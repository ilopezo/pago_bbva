package com.worknest.pagobancomer.domain.enumeration;

/**
 * The TipoLiquidacion enumeration.
 */
public enum TipoLiquidacion {
    PREDIAL,CATASTRAL
}
