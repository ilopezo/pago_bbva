package com.worknest.pagobancomer.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.worknest.pagobancomer.domain.enumeration.TipoParamBanco;

/**
 * A PlParamBanco.
 */
@Entity
@Table(name = "pl_param_banco")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PlParamBanco implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 100)
    @Column(name = "nombre", length = 100, nullable = false)
    private String nombre;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo", nullable = false)
    private TipoParamBanco tipo;

    @NotNull
    @Column(name = "en_uso", nullable = false)
    private Boolean enUso;

    @Size(max = 128)
    @Column(name = "valor_default", length = 128)
    private String valorDefault;

    @ManyToOne
    private CatBanco institucionBancaria;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public PlParamBanco nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public TipoParamBanco getTipo() {
        return tipo;
    }

    public PlParamBanco tipo(TipoParamBanco tipo) {
        this.tipo = tipo;
        return this;
    }

    public void setTipo(TipoParamBanco tipo) {
        this.tipo = tipo;
    }

    public Boolean isEnUso() {
        return enUso;
    }

    public PlParamBanco enUso(Boolean enUso) {
        this.enUso = enUso;
        return this;
    }

    public void setEnUso(Boolean enUso) {
        this.enUso = enUso;
    }

    public String getValorDefault() {
        return valorDefault;
    }

    public PlParamBanco valorDefault(String valorDefault) {
        this.valorDefault = valorDefault;
        return this;
    }

    public void setValorDefault(String valorDefault) {
        this.valorDefault = valorDefault;
    }

    public CatBanco getInstitucionBancaria() {
        return institucionBancaria;
    }

    public PlParamBanco institucionBancaria(CatBanco catBanco) {
        this.institucionBancaria = catBanco;
        return this;
    }

    public void setInstitucionBancaria(CatBanco catBanco) {
        this.institucionBancaria = catBanco;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PlParamBanco plParamBanco = (PlParamBanco) o;
        if (plParamBanco.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plParamBanco.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlParamBanco{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", tipo='" + getTipo() + "'" +
            ", enUso='" + isEnUso() + "'" +
            ", valorDefault='" + getValorDefault() + "'" +
            "}";
    }
}
