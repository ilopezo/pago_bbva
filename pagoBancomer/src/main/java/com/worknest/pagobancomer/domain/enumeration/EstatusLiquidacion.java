package com.worknest.pagobancomer.domain.enumeration;

/**
 * The EstatusLiquidacion enumeration.
 */
public enum EstatusLiquidacion {
    PAGADA,CANCELADA,VENCIDA,VIGENTE,SOLICITUD_ELIMINACION
}
