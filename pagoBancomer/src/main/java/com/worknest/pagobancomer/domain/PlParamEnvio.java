package com.worknest.pagobancomer.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A PlParamEnvio.
 */
@Entity
@Table(name = "pl_param_envio")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PlParamEnvio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 100)
    @Column(name = "valor", length = 100, nullable = false)
    private String valor;

    @ManyToOne
    private PlParamBanco paramBanco;

    @ManyToOne
    private PlIntentoPago intentoPago;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public PlParamEnvio valor(String valor) {
        this.valor = valor;
        return this;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public PlParamBanco getParamBanco() {
        return paramBanco;
    }

    public PlParamEnvio paramBanco(PlParamBanco plParamBanco) {
        this.paramBanco = plParamBanco;
        return this;
    }

    public void setParamBanco(PlParamBanco plParamBanco) {
        this.paramBanco = plParamBanco;
    }

    public PlIntentoPago getIntentoPago() {
        return intentoPago;
    }

    public PlParamEnvio intentoPago(PlIntentoPago plIntentoPago) {
        this.intentoPago = plIntentoPago;
        return this;
    }

    public void setIntentoPago(PlIntentoPago plIntentoPago) {
        this.intentoPago = plIntentoPago;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PlParamEnvio plParamEnvio = (PlParamEnvio) o;
        if (plParamEnvio.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plParamEnvio.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlParamEnvio{" +
            "id=" + getId() +
            ", valor='" + getValor() + "'" +
            "}";
    }
}
