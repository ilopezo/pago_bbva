package com.worknest.pagobancomer.domain.enumeration;

/**
 * The EdoCivil enumeration.
 */
public enum EdoCivil {
    SOLTERO, CASADO, VIUDO, DIVORCIADO, UNION_LIBRE, COMPROMETIDO
}
