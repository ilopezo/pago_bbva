package com.worknest.pagobancomer.domain.enumeration;

public enum EstatusRecibo {
	CANCELADO,PENDIENTE_CANCELACION,ACTIVO
}
