package com.worknest.pagobancomer.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A LiquidacionPredial.
 */
@Entity
@Table(name = "liquidacion_predial")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LiquidacionPredial implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 31)
    @Column(name = "clavecatastral", length = 31, nullable = false)
    private String clavecatastral;

    @Size(max = 31)
    @Column(name = "clavecatastralant", length = 31)
    private String clavecatastralant;

    @Column(name = "valorcatastral", precision=10, scale=2)
    private BigDecimal valorcatastral;

    @NotNull
    @Max(value = 99999)
    @Column(name = "bin_inicial", nullable = false)
    private Integer binInicial;

    @NotNull
    @Max(value = 99999)
    @Column(name = "bim_final", nullable = false)
    private Integer bimFinal;

    @Column(name = "sup_terreno", precision=10, scale=2)
    private BigDecimal supTerreno;

    @Column(name = "sup_construccion", precision=10, scale=2)
    private BigDecimal supConstruccion;

    @NotNull
    @Size(max = 511)
    @Column(name = "domicilio", length = 511, nullable = false)
    private String domicilio;

    @NotNull
    @Size(max = 511)
    @Column(name = "colonia", length = 511, nullable = false)
    private String colonia;

    @Size(max = 20)
    @Column(name = "tipo_predio", length = 20)
    private String tipoPredio;

    @NotNull
    @Size(max = 511)
    @Column(name = "concepto", length = 511, nullable = false)
    private String concepto;

    @NotNull
    @Size(max = 511)
    @Column(name = "propietarios", length = 511, nullable = false)
    private String propietarios;

    @Size(max = 13)
    @Column(name = "rfc", length = 13)
    private String rfc;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private Liquidacion liquidacion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClavecatastral() {
        return clavecatastral;
    }

    public LiquidacionPredial clavecatastral(String clavecatastral) {
        this.clavecatastral = clavecatastral;
        return this;
    }

    public void setClavecatastral(String clavecatastral) {
        this.clavecatastral = clavecatastral;
    }

    public String getClavecatastralant() {
        return clavecatastralant;
    }

    public LiquidacionPredial clavecatastralant(String clavecatastralant) {
        this.clavecatastralant = clavecatastralant;
        return this;
    }

    public void setClavecatastralant(String clavecatastralant) {
        this.clavecatastralant = clavecatastralant;
    }

    public BigDecimal getValorcatastral() {
        return valorcatastral;
    }

    public LiquidacionPredial valorcatastral(BigDecimal valorcatastral) {
        this.valorcatastral = valorcatastral;
        return this;
    }

    public void setValorcatastral(BigDecimal valorcatastral) {
        this.valorcatastral = valorcatastral;
    }

    public Integer getBinInicial() {
        return binInicial;
    }

    public LiquidacionPredial binInicial(Integer binInicial) {
        this.binInicial = binInicial;
        return this;
    }

    public void setBinInicial(Integer binInicial) {
        this.binInicial = binInicial;
    }

    public Integer getBimFinal() {
        return bimFinal;
    }

    public LiquidacionPredial bimFinal(Integer bimFinal) {
        this.bimFinal = bimFinal;
        return this;
    }

    public void setBimFinal(Integer bimFinal) {
        this.bimFinal = bimFinal;
    }

    public BigDecimal getSupTerreno() {
        return supTerreno;
    }

    public LiquidacionPredial supTerreno(BigDecimal supTerreno) {
        this.supTerreno = supTerreno;
        return this;
    }

    public void setSupTerreno(BigDecimal supTerreno) {
        this.supTerreno = supTerreno;
    }

    public BigDecimal getSupConstruccion() {
        return supConstruccion;
    }

    public LiquidacionPredial supConstruccion(BigDecimal supConstruccion) {
        this.supConstruccion = supConstruccion;
        return this;
    }

    public void setSupConstruccion(BigDecimal supConstruccion) {
        this.supConstruccion = supConstruccion;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public LiquidacionPredial domicilio(String domicilio) {
        this.domicilio = domicilio;
        return this;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getColonia() {
        return colonia;
    }

    public LiquidacionPredial colonia(String colonia) {
        this.colonia = colonia;
        return this;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getTipoPredio() {
        return tipoPredio;
    }

    public LiquidacionPredial tipoPredio(String tipoPredio) {
        this.tipoPredio = tipoPredio;
        return this;
    }

    public void setTipoPredio(String tipoPredio) {
        this.tipoPredio = tipoPredio;
    }

    public String getConcepto() {
        return concepto;
    }

    public LiquidacionPredial concepto(String concepto) {
        this.concepto = concepto;
        return this;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getPropietarios() {
        return propietarios;
    }

    public LiquidacionPredial propietarios(String propietarios) {
        this.propietarios = propietarios;
        return this;
    }

    public void setPropietarios(String propietarios) {
        this.propietarios = propietarios;
    }

    public String getRfc() {
        return rfc;
    }

    public LiquidacionPredial rfc(String rfc) {
        this.rfc = rfc;
        return this;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public Liquidacion getLiquidacion() {
        return liquidacion;
    }

    public LiquidacionPredial liquidacion(Liquidacion liquidacion) {
        this.liquidacion = liquidacion;
        return this;
    }

    public void setLiquidacion(Liquidacion liquidacion) {
        this.liquidacion = liquidacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LiquidacionPredial liquidacionPredial = (LiquidacionPredial) o;
        if (liquidacionPredial.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), liquidacionPredial.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LiquidacionPredial{" +
            "id=" + getId() +
            ", clavecatastral='" + getClavecatastral() + "'" +
            ", clavecatastralant='" + getClavecatastralant() + "'" +
            ", valorcatastral='" + getValorcatastral() + "'" +
            ", binInicial='" + getBinInicial() + "'" +
            ", bimFinal='" + getBimFinal() + "'" +
            ", supTerreno='" + getSupTerreno() + "'" +
            ", supConstruccion='" + getSupConstruccion() + "'" +
            ", domicilio='" + getDomicilio() + "'" +
            ", colonia='" + getColonia() + "'" +
            ", tipoPredio='" + getTipoPredio() + "'" +
            ", concepto='" + getConcepto() + "'" +
            ", propietarios='" + getPropietarios() + "'" +
            ", rfc='" + getRfc() + "'" +
            "}";
    }
}
