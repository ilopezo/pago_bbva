package com.worknest.pagobancomer.domain.enumeration;

/**
 * The TipoEmail enumeration.
 */
public enum TipoEmail {
    PERSONAL, NOTIFICACION, FISCAL
}
