package com.worknest.pagobancomer.domain.enumeration;

/**
 * The TipoDireccion enumeration.
 */
public enum TipoDireccion {
    DOMICILIO, NOTIFICACION, FISCAL, SUCURSAL
}
