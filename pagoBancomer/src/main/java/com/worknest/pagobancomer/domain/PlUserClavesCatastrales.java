package com.worknest.pagobancomer.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A PlUserClavesCatastrales.
 */
@Entity
@Table(name = "pl_user_claves_catastrales")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PlUserClavesCatastrales implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 31)
    @Column(name = "llave", length = 31, nullable = false)
    private String llave;

    @ManyToOne
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLlave() {
        return llave;
    }

    public PlUserClavesCatastrales llave(String llave) {
        this.llave = llave;
        return this;
    }

    public void setLlave(String llave) {
        this.llave = llave;
    }

    public User getUser() {
        return user;
    }

    public PlUserClavesCatastrales user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PlUserClavesCatastrales plUserClavesCatastrales = (PlUserClavesCatastrales) o;
        if (plUserClavesCatastrales.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plUserClavesCatastrales.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlUserClavesCatastrales{" +
            "id=" + getId() +
            ", llave='" + getLlave() + "'" +
            "}";
    }
}
