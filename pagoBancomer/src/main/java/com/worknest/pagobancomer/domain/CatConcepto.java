package com.worknest.pagobancomer.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;


/**
 * A CatConcepto.
 */
@Entity
@Table(name = "cat_concepto")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CatConcepto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 15)
    @Column(name = "clave", length = 15, nullable = false)
    private String clave;

    @NotNull
    @Size(max = 255)
    @Column(name = "descripcion", length = 255, nullable = false)
    private String descripcion;

    @Column(name = "id_cri")
    private Integer idCRI;

    @NotNull
    @Min(value = 0)
    @Max(value = 4)
    @Column(name = "redondeo", nullable = false)
    private Integer redondeo;

    @NotNull
    @Column(name = "aplica_descuento_aprovechamiento", nullable = false)
    private Boolean aplicaDescuentoAprovechamiento;

    @NotNull
    @Column(name = "aplica_descuento_impuesto", nullable = false)
    private Boolean aplicaDescuentoImpuesto;

    @NotNull
    @Column(name = "aplica_actualizacion", nullable = false)
    private Boolean aplicaActualizacion;

    @ManyToOne(optional = false)
    @NotNull
    private CatGrupoConcepto grupo;

    @NotNull
    @Column(name = "categoria_id", nullable = false)
    private Long categoriaId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClave() {
        return clave;
    }

    public CatConcepto clave(String clave) {
        this.clave = clave;
        return this;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public CatConcepto descripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdCRI() {
        return idCRI;
    }

    public CatConcepto idCRI(Integer idCRI) {
        this.idCRI = idCRI;
        return this;
    }

    public void setIdCRI(Integer idCRI) {
        this.idCRI = idCRI;
    }

    public Integer getRedondeo() {
        return redondeo;
    }

    public CatConcepto redondeo(Integer redondeo) {
        this.redondeo = redondeo;
        return this;
    }

    public void setRedondeo(Integer redondeo) {
        this.redondeo = redondeo;
    }

    public Boolean isAplicaDescuentoAprovechamiento() {
        return aplicaDescuentoAprovechamiento;
    }

    public CatConcepto aplicaDescuentoAprovechamiento(Boolean aplicaDescuentoAprovechamiento) {
        this.aplicaDescuentoAprovechamiento = aplicaDescuentoAprovechamiento;
        return this;
    }

    public void setAplicaDescuentoAprovechamiento(Boolean aplicaDescuentoAprovechamiento) {
        this.aplicaDescuentoAprovechamiento = aplicaDescuentoAprovechamiento;
    }

    public Boolean isAplicaDescuentoImpuesto() {
        return aplicaDescuentoImpuesto;
    }

    public CatConcepto aplicaDescuentoImpuesto(Boolean aplicaDescuentoImpuesto) {
        this.aplicaDescuentoImpuesto = aplicaDescuentoImpuesto;
        return this;
    }

    public void setAplicaDescuentoImpuesto(Boolean aplicaDescuentoImpuesto) {
        this.aplicaDescuentoImpuesto = aplicaDescuentoImpuesto;
    }

    public Boolean isAplicaActualizacion() {
        return aplicaActualizacion;
    }

    public CatConcepto aplicaActualizacion(Boolean aplicaActualizacion) {
        this.aplicaActualizacion = aplicaActualizacion;
        return this;
    }

    public void setAplicaActualizacion(Boolean aplicaActualizacion) {
        this.aplicaActualizacion = aplicaActualizacion;
    }

    public CatGrupoConcepto getGrupo() {
        return grupo;
    }

    public CatConcepto grupo(CatGrupoConcepto catGrupoConcepto) {
        this.grupo = catGrupoConcepto;
        return this;
    }

    public void setGrupo(CatGrupoConcepto catGrupoConcepto) {
        this.grupo = catGrupoConcepto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CatConcepto catConcepto = (CatConcepto) o;
        if (catConcepto.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), catConcepto.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CatConcepto{" +
            "id=" + getId() +
            ", clave='" + getClave() + "'" +
            ", descripcion='" + getDescripcion() + "'" +
            ", idCRI='" + getIdCRI() + "'" +
            ", redondeo='" + getRedondeo() + "'" +
            ", aplicaDescuentoAprovechamiento='" + isAplicaDescuentoAprovechamiento() + "'" +
            ", aplicaDescuentoImpuesto='" + isAplicaDescuentoImpuesto() + "'" +
            ", aplicaActualizacion='" + isAplicaActualizacion() + "'" +
            "}";
    }
}
