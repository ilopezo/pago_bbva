package com.worknest.pagobancomer.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A CatBanco.
 */
@Entity
@Table(name = "cat_banco")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CatBanco implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 10)
    @Column(name = "clave", length = 10, nullable = false)
    private String clave;

    @NotNull
    @Size(max = 60)
    @Column(name = "descripcion", length = 60, nullable = false)
    private String descripcion;

    @NotNull
    @Column(name = "referencia", nullable = false)
    private Boolean referencia;

    @Size(max = 32)
    @Column(name = "param_convenio_1", length = 32)
    private String paramConvenio1;

    @Size(max = 32)
    @Column(name = "param_convenio_2", length = 32)
    private String paramConvenio2;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClave() {
        return clave;
    }

    public CatBanco clave(String clave) {
        this.clave = clave;
        return this;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public CatBanco descripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean isReferencia() {
        return referencia;
    }

    public CatBanco referencia(Boolean referencia) {
        this.referencia = referencia;
        return this;
    }

    public void setReferencia(Boolean referencia) {
        this.referencia = referencia;
    }

    public String getParamConvenio1() {
        return paramConvenio1;
    }

    public CatBanco paramConvenio1(String paramConvenio1) {
        this.paramConvenio1 = paramConvenio1;
        return this;
    }

    public void setParamConvenio1(String paramConvenio1) {
        this.paramConvenio1 = paramConvenio1;
    }

    public String getParamConvenio2() {
        return paramConvenio2;
    }

    public CatBanco paramConvenio2(String paramConvenio2) {
        this.paramConvenio2 = paramConvenio2;
        return this;
    }

    public void setParamConvenio2(String paramConvenio2) {
        this.paramConvenio2 = paramConvenio2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CatBanco catBanco = (CatBanco) o;
        if (catBanco.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), catBanco.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CatBanco{" +
            "id=" + getId() +
            ", clave='" + getClave() + "'" +
            ", descripcion='" + getDescripcion() + "'" +
            ", referencia='" + isReferencia() + "'" +
            ", paramConvenio1='" + getParamConvenio1() + "'" +
            ", paramConvenio2='" + getParamConvenio2() + "'" +
            "}";
    }
}
