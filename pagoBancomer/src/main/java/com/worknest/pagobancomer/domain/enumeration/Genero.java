package com.worknest.pagobancomer.domain.enumeration;

/**
 * The Genero enumeration.
 */
public enum Genero {
    MASCULINO, FEMENINO
}
