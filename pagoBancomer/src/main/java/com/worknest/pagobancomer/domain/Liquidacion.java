package com.worknest.pagobancomer.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.worknest.pagobancomer.domain.enumeration.EstatusLiquidacion;
import com.worknest.pagobancomer.domain.enumeration.TipoLiquidacion;


/**
 * A Liquidacion.
 */
@Entity
@Table(name = "liquidacion")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Liquidacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "numero_liquidacion")
    private Long numeroLiquidacion;

    @NotNull
    @Column(name = "fecha_creacion", nullable = false)
    private LocalDate fechaCreacion;

    @NotNull
    @Column(name = "vigencia", nullable = false)
    private LocalDate vigencia;

    @Size(max = 31)
    @Column(name = "referencia", length = 31)
    private String referencia;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_liquidacion", nullable = false)
    private TipoLiquidacion tipoLiquidacion;

    @NotNull
    @Size(max = 511)
    @Column(name = "concepto", length = 511, nullable = false)
    private String concepto;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "total", precision=10, scale=2, nullable = false)
    private BigDecimal total;

    @NotNull
    @Size(max = 120)
    @Column(name = "total_letra", length = 120, nullable = false)
    private String totalLetra;

    @Column(name = "redondeo")
    private Float redondeo;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "estatus", nullable = false)
    private EstatusLiquidacion estatus;

    @ManyToOne(optional = false)
    @NotNull
    private LiquidacionEmisor catEmisor;

    @ManyToOne(optional = false)
    @NotNull
    private CatGrupoConcepto grupo;

    @OneToOne(mappedBy = "liquidacion",cascade=CascadeType.ALL, orphanRemoval=true)
    @JsonIgnore
    private LiquidacionPredial liquidacionPredial;

    @OneToMany(mappedBy = "liquidacion", cascade=CascadeType.ALL, orphanRemoval=true)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<LiquidacionDesConcepto> desgloceConceptos = new HashSet<>();
   
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumeroLiquidacion() {
        return numeroLiquidacion;
    }

    public Liquidacion numeroLiquidacion(Long numeroLiquidacion) {
        this.numeroLiquidacion = numeroLiquidacion;
        return this;
    }

    public void setNumeroLiquidacion(Long numeroLiquidacion) {
        this.numeroLiquidacion = numeroLiquidacion;
    }

    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    public Liquidacion fechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
        return this;
    }

    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public LocalDate getVigencia() {
        return vigencia;
    }

    public Liquidacion vigencia(LocalDate vigencia) {
        this.vigencia = vigencia;
        return this;
    }

    public void setVigencia(LocalDate vigencia) {
        this.vigencia = vigencia;
    }

    public String getReferencia() {
        return referencia;
    }

    public Liquidacion referencia(String referencia) {
        this.referencia = referencia;
        return this;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public TipoLiquidacion getTipoLiquidacion() {
        return tipoLiquidacion;
    }

    public Liquidacion tipoLiquidacion(TipoLiquidacion tipoLiquidacion) {
        this.tipoLiquidacion = tipoLiquidacion;
        return this;
    }

    public void setTipoLiquidacion(TipoLiquidacion tipoLiquidacion) {
        this.tipoLiquidacion = tipoLiquidacion;
    }

    public String getConcepto() {
        return concepto;
    }

    public Liquidacion concepto(String concepto) {
        this.concepto = concepto;
        return this;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public Liquidacion total(BigDecimal total) {
        this.total = total;
        return this;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getTotalLetra() {
        return totalLetra;
    }

    public Liquidacion totalLetra(String totalLetra) {
        this.totalLetra = totalLetra;
        return this;
    }

    public void setTotalLetra(String totalLetra) {
        this.totalLetra = totalLetra;
    }

    public Float getRedondeo() {
        return redondeo;
    }

    public Liquidacion redondeo(Float redondeo) {
        this.redondeo = redondeo;
        return this;
    }

    public void setRedondeo(Float redondeo) {
        this.redondeo = redondeo;
    }

    public EstatusLiquidacion getEstatus() {
        return estatus;
    }

    public Liquidacion estatus(EstatusLiquidacion estatus) {
        this.estatus = estatus;
        return this;
    }

    public void setEstatus(EstatusLiquidacion estatus) {
        this.estatus = estatus;
    }

    public LiquidacionEmisor getCatEmisor() {
        return catEmisor;
    }

    public Liquidacion catEmisor(LiquidacionEmisor liquidacionEmisor) {
        this.catEmisor = liquidacionEmisor;
        return this;
    }

    public void setCatEmisor(LiquidacionEmisor liquidacionEmisor) {
        this.catEmisor = liquidacionEmisor;
    }

    public CatGrupoConcepto getGrupo() {
        return grupo;
    }

    public Liquidacion grupo(CatGrupoConcepto catGrupoConcepto) {
        this.grupo = catGrupoConcepto;
        return this;
    }

    public void setGrupo(CatGrupoConcepto catGrupoConcepto) {
        this.grupo = catGrupoConcepto;
    }

    public LiquidacionPredial getLiquidacionPredial() {
        return liquidacionPredial;
    }

    public Liquidacion liquidacionPredial(LiquidacionPredial liquidacionPredial) {
        this.liquidacionPredial = liquidacionPredial;
        return this;
    }

    public void setLiquidacionPredial(LiquidacionPredial liquidacionPredial) {
        this.liquidacionPredial = liquidacionPredial;
    }

    public Set<LiquidacionDesConcepto> getDesgloceConceptos() {
        return desgloceConceptos;
    }

    public Liquidacion desgloceConceptos(Set<LiquidacionDesConcepto> liquidacionDesConceptos) {
        this.desgloceConceptos = liquidacionDesConceptos;
        return this;
    }

    public Liquidacion addDesgloceConcepto(LiquidacionDesConcepto liquidacionDesConcepto) {
        this.desgloceConceptos.add(liquidacionDesConcepto);
        liquidacionDesConcepto.setLiquidacion(this);
        return this;
    }

    public Liquidacion removeDesgloceConcepto(LiquidacionDesConcepto liquidacionDesConcepto) {
        this.desgloceConceptos.remove(liquidacionDesConcepto);
        liquidacionDesConcepto.setLiquidacion(null);
        return this;
    }

    public void setDesgloceConceptos(Set<LiquidacionDesConcepto> liquidacionDesConceptos) {
        this.desgloceConceptos = liquidacionDesConceptos;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Liquidacion liquidacion = (Liquidacion) o;
        if (liquidacion.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), liquidacion.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Liquidacion{" +
            "id=" + getId() +
            ", numeroLiquidacion='" + getNumeroLiquidacion() + "'" +
            ", fechaCreacion='" + getFechaCreacion() + "'" +
            ", vigencia='" + getVigencia() + "'" +
            ", referencia='" + getReferencia() + "'" +
            ", tipoLiquidacion='" + getTipoLiquidacion() + "'" +
            ", concepto='" + getConcepto() + "'" +
            ", total='" + getTotal() + "'" +
            ", totalLetra='" + getTotalLetra() + "'" +
            ", redondeo='" + getRedondeo() + "'" +
            ", estatus='" + getEstatus() + "'" +
            "}";
    }
}
