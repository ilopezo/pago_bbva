package com.worknest.pagobancomer.domain.enumeration;

/**
 * The TipoParamBanco enumeration.
 */
public enum TipoParamBanco {
    IN, OUT
}
