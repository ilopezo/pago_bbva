package com.worknest.pagobancomer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.worknest.pagobancomer.domain.PlIntentoPago;
import com.worknest.pagobancomer.domain.PlCarroHist;
import com.worknest.pagobancomer.domain.enumeration.StatusIntentoPago;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


/**
 * Spring Data JPA repository for the PlIntentoPago entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlIntentoPagoRepository extends JpaRepository<PlIntentoPago, Long> {

    @Query(value="select top (1) * from pl_intento_pago where historialcarro_id = ?1 and enviado = ?2 Order by id Desc", nativeQuery = true)
    PlIntentoPago findByUltimoCarroHist(PlCarroHist carroHist,Boolean booleano);

    @Query("select COALESCE( '1' , '0') from PlIntentoPago p where p.auth = ?1")
    String verificarAutorizacion(String auth);
       
    @Query(value="SELECT new com.worknest.pagobancomer.domain.PlIntentoPago (inp.id, inp.fecha, inp.enviado, inp.status, inp.auth, inp.historialcarro) FROM PlIntentoPago inp INNER JOIN PlCarroHist pch ON pch.id = inp.historialcarro.id INNER JOIN PlCarro pc ON pc.id = pch.carro.id WHERE pc.id = :idCarro AND inp.status = :status")
    Page<PlIntentoPago> findByHistorialcarroAndStatus(@Param("idCarro")Long idCarro,@Param("status") StatusIntentoPago status, Pageable pageable);
     
    @Query(value="SELECT new com.worknest.pagobancomer.domain.PlIntentoPago (inp.id, inp.fecha, inp.enviado, inp.status, inp.auth, inp.historialcarro) FROM PlIntentoPago inp INNER JOIN PlCarroHist pch ON pch.id = inp.historialcarro.id INNER JOIN PlCarro pc ON pc.id = pch.carro.id WHERE pc.id = :idCarro AND inp.status = :status")
    List<PlIntentoPago> findByHistorialcarroAndStatusPaginas(@Param("idCarro")Long idCarro,@Param("status") StatusIntentoPago status);
}
