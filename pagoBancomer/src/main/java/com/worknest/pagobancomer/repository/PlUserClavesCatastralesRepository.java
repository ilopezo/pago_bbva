package com.worknest.pagobancomer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.worknest.pagobancomer.domain.PlUserClavesCatastrales;

/**
 * Spring Data JPA repository for the PlUserClavesCatastrales entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlUserClavesCatastralesRepository extends JpaRepository<PlUserClavesCatastrales, Long> {

    @Query("select pl_user_claves_catastrales from PlUserClavesCatastrales pl_user_claves_catastrales where pl_user_claves_catastrales.user.login = ?#{principal.username}")
    List<PlUserClavesCatastrales> findByUserIsCurrentUser();

}
