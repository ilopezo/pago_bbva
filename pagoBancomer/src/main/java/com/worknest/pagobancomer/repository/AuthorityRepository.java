package com.worknest.pagobancomer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.worknest.pagobancomer.domain.Authority;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
