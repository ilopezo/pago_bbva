package com.worknest.pagobancomer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.worknest.pagobancomer.domain.PlParamEnvio;


/**
 * Spring Data JPA repository for the PlParamEnvio entity.
 */
@Repository
public interface PlParamEnvioRepository extends JpaRepository<PlParamEnvio, Long> {

 

}
