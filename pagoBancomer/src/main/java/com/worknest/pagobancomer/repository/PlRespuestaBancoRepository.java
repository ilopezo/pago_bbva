package com.worknest.pagobancomer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.worknest.pagobancomer.domain.PlRespuestaBanco;


/**
 * Spring Data JPA repository for the PlRespuestaBanco entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlRespuestaBancoRepository extends JpaRepository<PlRespuestaBanco, Long> {

}
