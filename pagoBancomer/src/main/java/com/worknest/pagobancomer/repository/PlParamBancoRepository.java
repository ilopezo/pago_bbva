package com.worknest.pagobancomer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.worknest.pagobancomer.domain.PlParamBanco;


/**
 * Spring Data JPA repository for the PlParamBanco entity.
 */

@Repository
public interface PlParamBancoRepository extends JpaRepository<PlParamBanco, Long> {

    @Query("select c from PlParamBanco c where c.enUso=true and c.institucionBancaria.clave= :claveBanco and c.tipo = 'OUT'")
    List<PlParamBanco> getParamsActivosByClaveBanco(@Param("claveBanco") String claveBanco);
    
    @Query("select c from PlParamBanco c where c.enUso=true and c.institucionBancaria.clave= :claveBanco and c.tipo = 'IN'")
    List<PlParamBanco> getParamsActivosByBanco(@Param("claveBanco") String claveBanco);
        
    @Query(value="select * from pl_param_banco where nombre = ?1 and tipo = ?2 and institucion_bancaria_id = ?3", nativeQuery = true)
    PlParamBanco findByNombre(String nombre,String tipo, String clave);

}
