package com.worknest.pagobancomer.repository;

import com.worknest.pagobancomer.domain.PlCarro;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.worknest.pagobancomer.domain.PlCarroHist;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;


/**
 * Spring Data JPA repository for the PlCarroHist entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlCarroHistRepository extends JpaRepository<PlCarroHist, Long> {
    //se busca el ultimo carroHist creado para el carro del usuario 
    @Query(value="select top (1) * from pl_carro_hist where carro_id = ?1 Order By id Desc", nativeQuery = true)
    PlCarroHist findByCarroOrderByIdDesc(Long carro);
        
    List<PlCarroHist> findByCarro(PlCarro carro);
    
}
