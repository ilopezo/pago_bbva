package com.worknest.pagobancomer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.worknest.pagobancomer.domain.PlParamRespuesta;
import com.worknest.pagobancomer.domain.enumeration.TipoParamBanco;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


/**
 * Spring Data JPA repository for the PlParamRespuesta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlParamRespuestaRepository extends JpaRepository<PlParamRespuesta, Long> {
    @Query(value="SELECT new com.worknest.pagobancomer.domain.PlParamRespuesta(pr.valor) FROM PlParamRespuesta pr INNER JOIN PlParamBanco pb ON pr.paramBanco.id = pb.id "
            +" INNER JOIN PlRespuestaBanco rb ON rb.id = pr.respuestaBanco.id"
            +" INNER JOIN PlIntentoPago ip ON ip.id = rb.intentoPago.id"
            +" WHERE pb.nombre LIKE %:amount% AND pb.tipo=:tipo AND ip.id=:intentopago")
    PlParamRespuesta buscarImporte(@Param("intentopago") Long intentopago,@Param("tipo")TipoParamBanco tipo, @Param("amount") String amount);
}
