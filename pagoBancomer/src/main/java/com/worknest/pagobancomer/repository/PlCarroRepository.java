package com.worknest.pagobancomer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.worknest.pagobancomer.domain.PlCarro;


/**
 * Spring Data JPA repository for the PlCarro entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlCarroRepository extends JpaRepository<PlCarro, Long> {
    //busca si existe un carro para el usuario
    PlCarro findByUser(Long idUsuario);
    
}
