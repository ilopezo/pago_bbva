package com.worknest.pagobancomer.repository;

import com.worknest.pagobancomer.service.dto.PlCarroHistDTO;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.worknest.pagobancomer.domain.PlCarroDetHist;


/**
 * Spring Data JPA repository for the PlCarroDetHist entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlCarroDetHistRepository extends JpaRepository<PlCarroDetHist, Long> {
    
    @Query(value="select * from pl_carro_det_hist where carro_id = ?1 and liquidacion_id = ?2", nativeQuery = true)
    PlCarroDetHist buscarDetallerCarroHist(Long idCarroHist, Long idLiquidacion);
    
    List<PlCarroDetHist> findByCarro(PlCarroHistDTO plCarroHistDTO);

    @Modifying
    @Query("update PlCarroDetHist p set p.liquidacion = null where p.llave = ?1 and p.carro.carro.id = ?2 and p.liquidacion.estatus != 'PAGADA' ")
    void updateAllByLlave(String llave, Long id);

    @Query("select p from PlCarroDetHist p where p.llave = ?1 and p.liquidacion.estatus != 'PAGADA' ")
    List<PlCarroDetHist> getAllByLlave(String llave);

    @Query("select p from PlCarroDetHist p where p.carro.id = ?1")
    List<PlCarroDetHist> getAllByIdCarroHist(Long idCarroHist);
}
