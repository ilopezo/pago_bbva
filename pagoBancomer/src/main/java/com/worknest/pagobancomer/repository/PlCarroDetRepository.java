package com.worknest.pagobancomer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.worknest.pagobancomer.domain.PlCarroDet;


/**
 * Spring Data JPA repository for the PlCarroDet entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlCarroDetRepository extends JpaRepository<PlCarroDet, Long> {
	
    List<PlCarroDet> findByCarroId(Long idCarro);
    
    List<PlCarroDet> findByLlave(String llave);
    
    @Query("select cd from PlCarroDet cd where cd.llave = ?1 and cd.carro.id = ?2")
    List<PlCarroDet> findByLlaveCarro(String llave, Long idCarro);

    @Modifying(clearAutomatically = true)
    @Query("delete from PlCarroDet cd where cd.llave = ?1 and cd.carro.id = ?2")
    void deleteAllByLlave(String llave, Long idCarro);

    @Query("select cd from PlCarroDet cd where cd.liquidacion.id = ?1")
    List<PlCarroDet> findByLiquidacion(Long idLiquidacion);
}
