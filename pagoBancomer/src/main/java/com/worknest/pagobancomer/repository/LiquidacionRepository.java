package com.worknest.pagobancomer.repository;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.worknest.pagobancomer.domain.Liquidacion;


/**
 * Spring Data JPA repository for the Liquidacion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LiquidacionRepository extends JpaRepository<Liquidacion, Long> {

    @Query("select a from Liquidacion a left join fetch a.desgloceConceptos c where a.vigencia >= ?2 AND a.numeroLiquidacion=?1 and a.id not in (select r.liquidacion.id from Recibo r) ")
    Optional<Liquidacion> findOneWithDesgloceConceptosByNumeroLiq(Long numeroLiquidacion, LocalDate vigenciaMax);

}
