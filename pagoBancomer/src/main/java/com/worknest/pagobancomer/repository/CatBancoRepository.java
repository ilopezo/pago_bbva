package com.worknest.pagobancomer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.worknest.pagobancomer.domain.CatBanco;


/**
 * Spring Data JPA repository for the CatBanco entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CatBancoRepository extends JpaRepository<CatBanco, Long> {

}
