package com.worknest.pagobancomer.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.worknest.pagobancomer.domain.CatBanco;
import com.worknest.pagobancomer.repository.CatBancoRepository;
import com.worknest.pagobancomer.service.CatBancoService;
import com.worknest.pagobancomer.service.dto.CatBancoDTO;
import com.worknest.pagobancomer.service.mapper.CatBancoMapper;

/**
 * Service Implementation for managing CatBanco.
 */
@Service
@Transactional
public class CatBancoServiceImpl implements CatBancoService{

    private final Logger log = LoggerFactory.getLogger(CatBancoServiceImpl.class);

    private final CatBancoRepository catBancoRepository;

    private final CatBancoMapper catBancoMapper;

    public CatBancoServiceImpl(CatBancoRepository catBancoRepository, CatBancoMapper catBancoMapper) {
        this.catBancoRepository = catBancoRepository;
        this.catBancoMapper = catBancoMapper;
    }

    /**
     * Save a catBanco.
     *
     * @param catBancoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public CatBancoDTO save(CatBancoDTO catBancoDTO) {
        log.debug("Request to save CatBanco : {}", catBancoDTO);
        CatBanco catBanco = catBancoMapper.toEntity(catBancoDTO);
        catBanco = catBancoRepository.save(catBanco);
        return catBancoMapper.toDto(catBanco);
    }

    /**
     *  Get all the catBancos.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<CatBancoDTO> findAll() {
        log.debug("Request to get all CatBancos");
        return catBancoRepository.findAll().stream()
            .map(catBancoMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one catBanco by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public CatBancoDTO findOne(Long id) {
        log.debug("Request to get CatBanco : {}", id);
        CatBanco catBanco = catBancoRepository.findOne(id);
        return catBancoMapper.toDto(catBanco);
    }

    /**
     *  Delete the  catBanco by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CatBanco : {}", id);
        catBancoRepository.delete(id);
    }
}
