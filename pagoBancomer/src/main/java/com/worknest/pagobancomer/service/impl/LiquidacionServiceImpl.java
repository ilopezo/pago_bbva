package com.worknest.pagobancomer.service.impl;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.worknest.pagobancomer.service.dto.LiquidacionDesConceptoDTO;
import com.worknest.pagobancomer.domain.Liquidacion;
import com.worknest.pagobancomer.repository.LiquidacionRepository;
import com.worknest.pagobancomer.service.LiquidacionService;
import com.worknest.pagobancomer.service.dto.LiquidacionDTO;
import com.worknest.pagobancomer.service.mapper.LiquidacionDesConceptoMapper;
import com.worknest.pagobancomer.service.mapper.LiquidacionMapper;

/**
 * Service Implementation for managing Liquidacion.
 */
@Service
@Transactional
public class LiquidacionServiceImpl implements LiquidacionService{

    private final Logger log = LoggerFactory.getLogger(LiquidacionServiceImpl.class);

    private final LiquidacionRepository liquidacionRepository;

    private final LiquidacionMapper liquidacionMapper;
	private final LiquidacionDesConceptoMapper liquidacionDesConceptoMapper;
    
    public LiquidacionServiceImpl(LiquidacionRepository liquidacionRepository, LiquidacionMapper liquidacionMapper,
    		LiquidacionDesConceptoMapper liquidacionDesConceptoMapper) {
        this.liquidacionRepository = liquidacionRepository;
        this.liquidacionMapper = liquidacionMapper;
		this.liquidacionDesConceptoMapper = liquidacionDesConceptoMapper;
    }

    /**
     * Save a liquidacion.
     *
     * @param liquidacionDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public LiquidacionDTO save(LiquidacionDTO liquidacionDTO) {
        log.debug("Request to save Liquidacion : {}", liquidacionDTO);
        Liquidacion liquidacion = liquidacionMapper.toEntity(liquidacionDTO);
        liquidacion = liquidacionRepository.save(liquidacion);
        return liquidacionMapper.toDto(liquidacion);
    }

    /**
     *  Get all the liquidacions.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<LiquidacionDTO> findAll() {
        log.debug("Request to get all Liquidacions");
        return liquidacionRepository.findAll().stream()
            .map(liquidacionMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one liquidacion by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public LiquidacionDTO findOne(Long id) {
        log.debug("Request to get Liquidacion : {}", id);
        Liquidacion liquidacion = liquidacionRepository.findOne(id);
        return liquidacionMapper.toDto(liquidacion);
    }

    /**
     *  Delete the  liquidacion by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Liquidacion : {}", id);
        liquidacionRepository.delete(id);
    }

    /**
     * buscar por "numeroLiquidacion" la liquidacion.
     *
     * @param numeroLiquidacion el numero de liquidacion
     * @return the entity
     */
    @Override
    public LiquidacionDTO findOneByNumLiq(Long numeroLiquidacion) {
        log.debug("Request to get Liquidacion numeroLiquidacion: {}", numeroLiquidacion);

        Optional<Liquidacion> liquidacion = liquidacionRepository.findOneWithDesgloceConceptosByNumeroLiq(numeroLiquidacion, LocalDate.now());
        if (!liquidacion.isPresent()) {
            return null;
        }

        LiquidacionDTO liqdto = liquidacionMapper.toDto(liquidacion.get());

        List<LiquidacionDesConceptoDTO> liquidacionDesConcepto = liquidacion.get().getDesgloceConceptos().stream()
                        .map(liquidacionDesConceptoMapper::toDto).collect(Collectors.toCollection(LinkedList::new));

        liqdto.setLiquidacionDesConcepto(liquidacionDesConcepto);

        //liqdto.setLiquidacionPredial(liquidacionPredialMapper.toDto(liquidacion.get().getLiquidacionPredial()));

        return liqdto;
    }
}
