package com.worknest.pagobancomer.service;

import java.util.List;

import com.worknest.pagobancomer.service.dto.PlUserClavesCatastralesDTO;

/**
 * Service Interface for managing PlUserClavesCatastrales.
 */
public interface PlUserClavesCatastralesService {

    /**
     * Save a plUserClavesCatastrales.
     *
     * @param plUserClavesCatastralesDTO the entity to save
     * @return the persisted entity
     */
    PlUserClavesCatastralesDTO save(PlUserClavesCatastralesDTO plUserClavesCatastralesDTO);

    /**
     *  Get all the plUserClavesCatastrales.
     *
     *  @return the list of entities
     */
    List<PlUserClavesCatastralesDTO> findAll();

    /**
     *  Get the "id" plUserClavesCatastrales.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PlUserClavesCatastralesDTO findOne(Long id);

    /**
     *  Delete the "id" plUserClavesCatastrales.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
