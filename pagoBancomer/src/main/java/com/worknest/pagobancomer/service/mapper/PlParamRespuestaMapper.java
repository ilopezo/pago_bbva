package com.worknest.pagobancomer.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.worknest.pagobancomer.domain.PlParamRespuesta;
import com.worknest.pagobancomer.service.dto.PlParamRespuestaDTO;

/**
 * Mapper for the entity PlParamRespuesta and its DTO PlParamRespuestaDTO.
 */
@Mapper(componentModel = "spring", uses = {PlParamBancoMapper.class, PlRespuestaBancoMapper.class})
public interface PlParamRespuestaMapper extends EntityMapper<PlParamRespuestaDTO, PlParamRespuesta> {

    @Mapping(source = "paramBanco.id", target = "paramBancoId")
    @Mapping(source = "respuestaBanco.id", target = "respuestaBancoId")
    PlParamRespuestaDTO toDto(PlParamRespuesta plParamRespuesta); 

    @Mapping(source = "paramBancoId", target = "paramBanco")
    @Mapping(source = "respuestaBancoId", target = "respuestaBanco")
    PlParamRespuesta toEntity(PlParamRespuestaDTO plParamRespuestaDTO);

    default PlParamRespuesta fromId(Long id) {
        if (id == null) {
            return null;
        }
        PlParamRespuesta plParamRespuesta = new PlParamRespuesta();
        plParamRespuesta.setId(id);
        return plParamRespuesta;
    }
}
