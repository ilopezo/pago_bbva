package com.worknest.pagobancomer.service;

import com.worknest.pagobancomer.service.dto.DatosConciliacionDTO;
import java.util.List;

import com.worknest.pagobancomer.service.dto.PlRespuestaBancoDTO;
import com.worknest.pagobancomer.web.rest.errors.ExceptionAPI;

import java.util.Map;

/**
 * Service Interface for managing PlRespuestaBanco.
 */
public interface PlRespuestaBancoService {

    /**
     * Save a plRespuestaBanco.
     *
     * @param plRespuestaBancoDTO the entity to save
     * @return the persisted entity
     */
    PlRespuestaBancoDTO save(PlRespuestaBancoDTO plRespuestaBancoDTO);

    /**
     *  Get all the plRespuestaBancos.
     *
     *  @return the list of entities
     */
    List<PlRespuestaBancoDTO> findAll();

    /**
     *  Get the "id" plRespuestaBanco.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PlRespuestaBancoDTO findOne(Long id);

    /**
     *  Delete the "id" plRespuestaBanco.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    String guardarRespuesta(Map<String, String> respuestaBanco, Boolean aceptado) throws ExceptionAPI, Exception ;
   
    Map<String,String> obtenerDatos(DatosConciliacionDTO datos)throws ExceptionAPI;    
}
