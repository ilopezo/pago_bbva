package com.worknest.pagobancomer.service;

import com.worknest.pagobancomer.service.dto.HistorialDTO;
import com.worknest.pagobancomer.service.dto.LiquidacionDTO;
import java.util.List;

import com.worknest.pagobancomer.service.dto.PlCarroDTO;
import com.worknest.pagobancomer.web.rest.errors.ExceptionAPI;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing PlCarro.
 */
public interface PlCarroService {

    /**
     * Save a plCarro.
     *
     * @param plCarroDTO the entity to save
     * @return the persisted entity
     */
    PlCarroDTO save(PlCarroDTO plCarroDTO);

    /**
     *  Get all the plCarros.
     *
     *  @return the list of entities
     */
    List<PlCarroDTO> findAll();

    /**
     *  Get the "id" plCarro.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PlCarroDTO findOne(Long id);

    /**
     *  Delete the "id" plCarro.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
    
    //obtiene el carro del usuario logeado
    PlCarroDTO getCarro() throws ExceptionAPI, Exception;
    
    Object obtenerAdeudos(String clvCatastral) throws ExceptionAPI, Exception;
    
    LiquidacionDTO obtenerLiquidacion(String numLiquidacion) throws ExceptionAPI, Exception;
    
    List<HistorialDTO> obtenerHistorial(Pageable pageable) throws ExceptionAPI, Exception;
    
    //Metodo que devuelve la cantidad de paginas
    double getNumPaginas(int numeroPaginasDTO) throws ExceptionAPI;

}
