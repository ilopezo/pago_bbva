package com.worknest.pagobancomer.service.impl;

import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.worknest.pagobancomer.domain.PlCarro;
import com.worknest.pagobancomer.domain.PlCarroHist;
import com.worknest.pagobancomer.repository.PlCarroHistRepository;
import com.worknest.pagobancomer.repository.PlCarroRepository;
import com.worknest.pagobancomer.security.SecurityUtils;
import com.worknest.pagobancomer.service.PlCarroHistService;
import com.worknest.pagobancomer.service.dto.PlCarroDTO;
import com.worknest.pagobancomer.service.dto.PlCarroHistDTO;
import com.worknest.pagobancomer.service.mapper.PlCarroHistMapper;
import com.worknest.pagobancomer.web.rest.errors.ExceptionAPI;

/**
 * Service Implementation for managing PlCarroHist.
 */
@Service
@Transactional
public class PlCarroHistServiceImpl implements PlCarroHistService{

    private final Logger log = LoggerFactory.getLogger(PlCarroHistServiceImpl.class);

    private final PlCarroHistRepository plCarroHistRepository;

    private final PlCarroHistMapper plCarroHistMapper;
    
    private final PlCarroRepository plCarroRepository;

    public PlCarroHistServiceImpl(PlCarroHistRepository plCarroHistRepository, PlCarroHistMapper plCarroHistMapper, 
    		PlCarroRepository plCarroRepository) {
        this.plCarroHistRepository = plCarroHistRepository;
        this.plCarroHistMapper = plCarroHistMapper;
        this.plCarroRepository = plCarroRepository;
    }

    /**
     * Save a plCarroHist.
     *
     * @param plCarroHistDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PlCarroHistDTO save(PlCarroHistDTO plCarroHistDTO) {
        log.debug("Request to save PlCarroHist : {}", plCarroHistDTO);
        PlCarroHist plCarroHist = plCarroHistMapper.toEntity(plCarroHistDTO);
        plCarroHist = plCarroHistRepository.save(plCarroHist);
        return plCarroHistMapper.toDto(plCarroHist);
    }

    /**
     *  Get all the plCarroHists.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PlCarroHistDTO> findAll() {
        log.debug("Request to get all PlCarroHists");
        return plCarroHistRepository.findAll().stream()
            .map(plCarroHistMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one plCarroHist by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PlCarroHistDTO findOne(Long id) {
        log.debug("Request to get PlCarroHist : {}", id);
        PlCarroHist plCarroHist = plCarroHistRepository.findOne(id);
        return plCarroHistMapper.toDto(plCarroHist);
    }

    /**
     *  Delete the  plCarroHist by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PlCarroHist : {}", id);
        plCarroHistRepository.delete(id);
    }

    /**
     * crea el carroHist para el carro del usuario logeado
     * @return CarroHistDTO
     * @throws ExceptionAPI
     * @throws Exception 
     */
    @Override
    public PlCarroHistDTO crearCarroHist(Long idCarro) throws ExceptionAPI, Exception {  	
       
        //Se busca el ultimo historial del carro del usuario
        PlCarroHist plCarroHist = plCarroHistRepository.findByCarroOrderByIdDesc(idCarro);
        
        if (plCarroHist == null) { //si no se encontro un historial para el carro se le crea uno
            // Se crea un nuevo historial de carro (carroHist) con la fecha actual y la referencia vacia
            PlCarroHistDTO carroHist = new PlCarroHistDTO(ZonedDateTime.now(), "", idCarro);
            plCarroHist = plCarroHistMapper.toEntity(carroHist);
            plCarroHist = plCarroHistRepository.save(plCarroHist);
        }               
        return plCarroHistMapper.toDto(plCarroHist);
    }
}
