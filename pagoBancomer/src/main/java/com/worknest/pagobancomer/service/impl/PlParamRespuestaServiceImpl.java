package com.worknest.pagobancomer.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.worknest.pagobancomer.domain.PlParamRespuesta;
import com.worknest.pagobancomer.repository.PlParamRespuestaRepository;
import com.worknest.pagobancomer.service.PlParamRespuestaService;
import com.worknest.pagobancomer.service.dto.PlParamRespuestaDTO;
import com.worknest.pagobancomer.service.mapper.PlParamRespuestaMapper;

/**
 * Service Implementation for managing PlParamRespuesta.
 */
@Service
@Transactional
public class PlParamRespuestaServiceImpl implements PlParamRespuestaService{

    private final Logger log = LoggerFactory.getLogger(PlParamRespuestaServiceImpl.class);

    private final PlParamRespuestaRepository plParamRespuestaRepository;

    private final PlParamRespuestaMapper plParamRespuestaMapper;

    public PlParamRespuestaServiceImpl(PlParamRespuestaRepository plParamRespuestaRepository, PlParamRespuestaMapper plParamRespuestaMapper) {
        this.plParamRespuestaRepository = plParamRespuestaRepository;
        this.plParamRespuestaMapper = plParamRespuestaMapper;
    }

    /**
     * Save a plParamRespuesta.
     *
     * @param plParamRespuestaDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PlParamRespuestaDTO save(PlParamRespuestaDTO plParamRespuestaDTO) {
        log.debug("Request to save PlParamRespuesta : {}", plParamRespuestaDTO);
        PlParamRespuesta plParamRespuesta = plParamRespuestaMapper.toEntity(plParamRespuestaDTO);
        plParamRespuesta = plParamRespuestaRepository.save(plParamRespuesta);
        return plParamRespuestaMapper.toDto(plParamRespuesta);
    }

    /**
     *  Get all the plParamRespuestas.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PlParamRespuestaDTO> findAll() {
        log.debug("Request to get all PlParamRespuestas");
        return plParamRespuestaRepository.findAll().stream()
            .map(plParamRespuestaMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one plParamRespuesta by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PlParamRespuestaDTO findOne(Long id) {
        log.debug("Request to get PlParamRespuesta : {}", id);
        PlParamRespuesta plParamRespuesta = plParamRespuestaRepository.findOne(id);
        return plParamRespuestaMapper.toDto(plParamRespuesta);
    }

    /**
     *  Delete the  plParamRespuesta by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PlParamRespuesta : {}", id);
        plParamRespuestaRepository.delete(id);
    }

    /**
     * Guarda el parametro respuesta que proporciona el banco
     * @param plParamRespuesta
     * @return 
     */
    @Override
    public PlParamRespuesta guardarPlParamRespuesta(PlParamRespuesta plParamRespuesta) {
        plParamRespuesta = plParamRespuestaRepository.save(plParamRespuesta);
        return plParamRespuesta;
    }
}
