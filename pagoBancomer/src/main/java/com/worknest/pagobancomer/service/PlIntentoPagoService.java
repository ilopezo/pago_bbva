package com.worknest.pagobancomer.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import com.worknest.pagobancomer.service.dto.PlCarroHistDTO;
import com.worknest.pagobancomer.domain.PlIntentoPago;
import com.worknest.pagobancomer.service.dto.PlIntentoPagoDTO;
import com.worknest.pagobancomer.web.rest.errors.ExceptionAPI;
import java.util.Map;

/**
 * Service Interface for managing PlIntentoPago.
 */
public interface PlIntentoPagoService {

    /**
     * Save a plIntentoPago.
     *
     * @param plIntentoPagoDTO the entity to save
     * @return the persisted entity
     */
    PlIntentoPagoDTO save(PlIntentoPagoDTO plIntentoPagoDTO);

    /**
     *  Get all the plIntentoPagos.
     *
     *  @return the list of entities
     */
    List<PlIntentoPagoDTO> findAll();

    /**
     *  Get the "id" plIntentoPago.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PlIntentoPagoDTO findOne(Long id);
    
    PlIntentoPago findById(Long id);

    /**
     *  Delete the "id" plIntentoPago.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    //se crea un nuevo intento de pago
    Map<String, String> crearIntentoPago(PlCarroHistDTO carroHist) throws ExceptionAPI, Exception;
    
    // validacion clave
    boolean validarDatosReciboBancomer(Map<String, String> param, boolean saltarValidacionFirma) throws Exception, InvalidKeyException, UnsupportedEncodingException, NoSuchAlgorithmException;

    // verifica que la autorizacion no haya sido aplicada
    boolean verificaAutorizacion(String string);    
}
