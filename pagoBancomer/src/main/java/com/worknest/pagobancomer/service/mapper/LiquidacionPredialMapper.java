package com.worknest.pagobancomer.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.worknest.pagobancomer.domain.LiquidacionPredial;
import com.worknest.pagobancomer.service.dto.LiquidacionPredialDTO;

/**
 * Mapper for the entity LiquidacionPredial and its DTO LiquidacionPredialDTO.
 */
@Mapper(componentModel = "spring", uses = {LiquidacionMapper.class, })
public interface LiquidacionPredialMapper {

	@Mapping(source = "liquidacion.id", target = "liquidacionId")
    @Mapping(source = "liquidacion.numeroLiquidacion", target = "liquidacionNumeroLiquidacion")
    LiquidacionPredialDTO toDto(LiquidacionPredial liquidacionPredial); 

    LiquidacionPredial toEntity(LiquidacionPredialDTO liquidacionPredialDTO); 
    default LiquidacionPredial fromId(Long id) {
        if (id == null) {
            return null;
        }
        LiquidacionPredial liquidacionPredial = new LiquidacionPredial();
        liquidacionPredial.setId(id);
        return liquidacionPredial;
    }
}
