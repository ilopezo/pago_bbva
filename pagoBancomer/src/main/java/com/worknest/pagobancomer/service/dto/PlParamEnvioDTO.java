package com.worknest.pagobancomer.service.dto;


import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * A DTO for the PlParamEnvio entity.
 */
public class PlParamEnvioDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 100)
    private String valor;

    private Long paramBancoId;

    private Long intentoPagoId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Long getParamBancoId() {
        return paramBancoId;
    }

    public void setParamBancoId(Long plParamBancoId) {
        this.paramBancoId = plParamBancoId;
    }

    public Long getIntentoPagoId() {
        return intentoPagoId;
    }

    public void setIntentoPagoId(Long plIntentoPagoId) {
        this.intentoPagoId = plIntentoPagoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PlParamEnvioDTO plParamEnvioDTO = (PlParamEnvioDTO) o;
        if(plParamEnvioDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plParamEnvioDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlParamEnvioDTO{" +
            "id=" + getId() +
            ", valor='" + getValor() + "'" +
            "}";
    }
}
