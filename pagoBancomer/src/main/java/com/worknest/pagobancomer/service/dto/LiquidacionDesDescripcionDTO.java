package com.worknest.pagobancomer.service.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class LiquidacionDesDescripcionDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

    @NotNull
    private Integer orden;

    @NotNull
    @Size(max = 254)
    private String descripcion;

    private Long liquidacionId;

    private String liquidacionNumeroLiquidacion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getLiquidacionId() {
        return liquidacionId;
    }

    public void setLiquidacionId(Long liquidacionId) {
        this.liquidacionId = liquidacionId;
    }

    public String getLiquidacionNumeroLiquidacion() {
        return liquidacionNumeroLiquidacion;
    }

    public void setLiquidacionNumeroLiquidacion(String liquidacionNumeroLiquidacion) {
        this.liquidacionNumeroLiquidacion = liquidacionNumeroLiquidacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LiquidacionDesDescripcionDTO liquidacionDesDescripcionDTO = (LiquidacionDesDescripcionDTO) o;
        if(liquidacionDesDescripcionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), liquidacionDesDescripcionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LiquidacionDesDescripcionDTO{" +
            "id=" + getId() +
            ", orden='" + getOrden() + "'" +
            ", descripcion='" + getDescripcion() + "'" +
            "}";
    }
}
