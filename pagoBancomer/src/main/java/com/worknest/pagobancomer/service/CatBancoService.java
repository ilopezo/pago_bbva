package com.worknest.pagobancomer.service;

import java.util.List;

import com.worknest.pagobancomer.service.dto.CatBancoDTO;

/**
 * Service Interface for managing CatBanco.
 */
public interface CatBancoService {

    /**
     * Save a catBanco.
     *
     * @param catBancoDTO the entity to save
     * @return the persisted entity
     */
    CatBancoDTO save(CatBancoDTO catBancoDTO);

    /**
     *  Get all the catBancos.
     *
     *  @return the list of entities
     */
    List<CatBancoDTO> findAll();

    /**
     *  Get the "id" catBanco.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    CatBancoDTO findOne(Long id);

    /**
     *  Delete the "id" catBanco.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
