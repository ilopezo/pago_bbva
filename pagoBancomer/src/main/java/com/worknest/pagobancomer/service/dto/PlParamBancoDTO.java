package com.worknest.pagobancomer.service.dto;


import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.worknest.pagobancomer.domain.enumeration.TipoParamBanco;

/**
 * A DTO for the PlParamBanco entity.
 */
public class PlParamBancoDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 100)
    private String nombre;

    @NotNull
    private TipoParamBanco tipo;

    @NotNull
    private Boolean enUso;

    @Size(max = 128)
    private String valorDefault;

    private Long institucionBancariaId;

    private String institucionBancariaDescripcion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public TipoParamBanco getTipo() {
        return tipo;
    }

    public void setTipo(TipoParamBanco tipo) {
        this.tipo = tipo;
    }

    public Boolean isEnUso() {
        return enUso;
    }

    public void setEnUso(Boolean enUso) {
        this.enUso = enUso;
    }

    public String getValorDefault() {
        return valorDefault;
    }

    public void setValorDefault(String valorDefault) {
        this.valorDefault = valorDefault;
    }

    public Long getInstitucionBancariaId() {
        return institucionBancariaId;
    }

    public void setInstitucionBancariaId(Long catBancoId) {
        this.institucionBancariaId = catBancoId;
    }

    public String getInstitucionBancariaDescripcion() {
        return institucionBancariaDescripcion;
    }

    public void setInstitucionBancariaDescripcion(String catBancoDescripcion) {
        this.institucionBancariaDescripcion = catBancoDescripcion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PlParamBancoDTO plParamBancoDTO = (PlParamBancoDTO) o;
        if(plParamBancoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plParamBancoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlParamBancoDTO{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", tipo='" + getTipo() + "'" +
            ", enUso='" + isEnUso() + "'" +
            ", valorDefault='" + getValorDefault() + "'" +
            "}";
    }
}
