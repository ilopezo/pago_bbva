package com.worknest.pagobancomer.service;

import java.util.List;

import com.worknest.pagobancomer.service.dto.PlCarroDetDTO;
import com.worknest.pagobancomer.service.dto.PlCarroDetHistDTO;
import com.worknest.pagobancomer.service.dto.PlCarroHistDTO;
import com.worknest.pagobancomer.web.rest.errors.ExceptionAPI;

/**
 * Service Interface for managing PlCarroDetHist.
 */
public interface PlCarroDetHistService {

    /**
     * Save a plCarroDetHist.
     *
     * @param plCarroDetHistDTO the entity to save
     * @return the persisted entity
     */
    PlCarroDetHistDTO save(PlCarroDetHistDTO plCarroDetHistDTO);

    /**
     *  Get all the plCarroDetHists.
     *
     *  @return the list of entities
     */
    List<PlCarroDetHistDTO> findAll();

    /**
     *  Get the "id" plCarroDetHist.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PlCarroDetHistDTO findOne(Long id);

    /**
     *  Delete the "id" plCarroDetHist.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
    
    PlCarroHistDTO guardarConceptos(List<PlCarroDetDTO> listaConceptos, PlCarroHistDTO carroHist) throws ExceptionAPI, Exception;

    List<PlCarroDetHistDTO> buscarPorIdCarroHist(PlCarroHistDTO plCarroHist);
    
    List<PlCarroDetHistDTO> obtenerDetallesHist(Long idIntentoPago) throws ExceptionAPI;
}
