package com.worknest.pagobancomer.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import com.worknest.pagobancomer.domain.PlCarro;
import com.worknest.pagobancomer.domain.PlCarroHist;
import com.worknest.pagobancomer.domain.PlIntentoPago;
import com.worknest.pagobancomer.domain.PlParamRespuesta;
import com.worknest.pagobancomer.domain.enumeration.StatusIntentoPago;
import com.worknest.pagobancomer.domain.enumeration.TipoParamBanco;
import com.worknest.pagobancomer.repository.PlCarroHistRepository;
import com.worknest.pagobancomer.repository.PlCarroRepository;
import com.worknest.pagobancomer.repository.PlIntentoPagoRepository;
import com.worknest.pagobancomer.repository.PlParamRespuestaRepository;
import com.worknest.pagobancomer.security.SecurityUtils;
import com.worknest.pagobancomer.service.PlCarroService;
import com.worknest.pagobancomer.service.dto.AuthenticateDTO;
import com.worknest.pagobancomer.service.dto.HistorialDTO;
import com.worknest.pagobancomer.service.dto.LiquidacionDTO;
import com.worknest.pagobancomer.service.dto.PlCarroDTO;
import com.worknest.pagobancomer.service.dto.TokenDTO;
import com.worknest.pagobancomer.service.mapper.PlCarroMapper;
import com.worknest.pagobancomer.service.vo.LiquidacionAdeudoPredialVO;
import com.worknest.pagobancomer.web.rest.errors.ExceptionAPI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;


/**
 * Service Implementation for managing PlCarro.
 */
@Service
@Transactional
public class PlCarroServiceImpl implements PlCarroService{

    private final Logger log = LoggerFactory.getLogger(PlCarroServiceImpl.class);

    private final PlCarroRepository plCarroRepository;

    private final PlCarroMapper plCarroMapper;
    
    @Autowired
    private PlCarroHistRepository plCarroHistRepository;
    
    @Autowired
    private PlParamRespuestaRepository plParamRespuestaRepository;
    
    @Autowired
    private PlIntentoPagoRepository plIntentoPagoRepository;
    
    //variables para consumir servicios de otros sistemas
    private String _token;
   
    @Value("${application.host}") private String _host;
    @Value("${application.port}") private String _port;     
    @Value("${application.autenticate}") private String _autenticate;
    @Value("${application.usuario}") private String _usuario;
    @Value("${application.pass}") private String _pass;
    @Value("${application.getadeudos}") private String _getadeudos;
    @Value("${application.getliq}") private String _getliq;

    public PlCarroServiceImpl(PlCarroRepository plCarroRepository, PlCarroMapper plCarroMapper) {
        this.plCarroRepository = plCarroRepository;
        this.plCarroMapper = plCarroMapper;
    }

    /**
     * Save a plCarro.
     *
     * @param plCarroDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PlCarroDTO save(PlCarroDTO plCarroDTO) {
        log.debug("Request to save PlCarro : {}", plCarroDTO);
        PlCarro plCarro = plCarroMapper.toEntity(plCarroDTO);
        plCarro = plCarroRepository.save(plCarro);
        return plCarroMapper.toDto(plCarro);
    }

    /**
     *  Get all the plCarros.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PlCarroDTO> findAll() {
        log.debug("Request to get all PlCarros");
        return plCarroRepository.findAll().stream()
            .map(plCarroMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one plCarro by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PlCarroDTO findOne(Long id) {
        log.debug("Request to get PlCarro : {}", id);
        PlCarro plCarro = plCarroRepository.findOne(id);
        return plCarroMapper.toDto(plCarro);
    }

    /**
     *  Delete the  plCarro by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PlCarro : {}", id);
        plCarroRepository.delete(id);
    }

    /**
     * Busca el carro de el usuario logueado
     * @return
     * @throws ExceptionAPI
     * @throws Exception 
     */
    @Override
    public PlCarroDTO getCarro() throws ExceptionAPI, Exception {
        //log.debug("°°°°°°°°°° 1 °°°°°°°°°°°°°°°");
        //Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        log.debug("°°°°°°°°°°°°°°°°°°°°°°°°°" + SecurityUtils.getCurrentUserLogin());
        Long idUsuario = Long.parseLong(SecurityUtils.getCurrentUserLogin());               
        // busca el carro
        PlCarro plCarro = this.plCarroRepository.findByUser(idUsuario);
        // si no se encuentran conceptos en el carro se devuelve un mensaje de error
        if (plCarro == null) {
            // se arroja una excepcion personalizada con mensaje personalizado
            throw new ExceptionAPI(HttpStatus.ACCEPTED, "No se encontro el carro");
        }
        return this.plCarroMapper.toDto(plCarro);
    }
    
    /**
     * Metodo que obtiene los adeudos de una clave catastral
     * @param clvCatastral
     * @return
     * @throws ExceptionAPI
     * @throws Exception 
     */
    @Override
    public Object obtenerAdeudos(String clvCatastral) throws ExceptionAPI, Exception {
        this._token = this.generaToken(this._usuario, this._pass).getId_token();
        // Se prepara la entidad de envio con los datos necesarios
        LiquidacionAdeudoPredialVO envio = new LiquidacionAdeudoPredialVO(
                                                    null, // Bimestre Inicial
                                                    null, // Bimestre Final
                                                    clvCatastral,  // Clave Catastral
                                                    true,  // Es agrupada
                                                    false, // Es pago en linea
                                                    false); // Guardar Liquidacion

        // Se consulta el WS que genera el token
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + this._token);

        //Genera la url para consumir el WS que obtiene los adeudos
        String uri = this._host + ":" + this._port + this._getadeudos;

        HttpEntity<?> entity = new HttpEntity<>(envio, headers);

        // El WS obtiene los adeudos en base a los datos de envio
        ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, HttpMethod.POST, entity,
                        new ParameterizedTypeReference<Object>() {});
        if (responseEntity.getBody()==null) {
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST, "No se encontraron adeudos");
        }
        return responseEntity.getBody(); 
    }
    
    /**
     * Metodo que obtiene una liquidacion a traves de su numero de liquidacion
     * @param numLiquidacion
     * @return
     * @throws ExceptionAPI
     * @throws Exception 
     */
    @Override
    public LiquidacionDTO obtenerLiquidacion(String numLiquidacion) throws ExceptionAPI, Exception {
        this._token = this.generaToken(this._usuario, this._pass).getId_token();
        
        // Se consulta el WS que genera el token
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + this._token);

        //Genera la url para consumir el WS que obtiene una liquidacion
        String uri = this._host + ":" + this._port + this._getliq + numLiquidacion;

        HttpEntity<?> entity = new HttpEntity<>(numLiquidacion, headers);

        // El WS obtiene la liquidacion en base a el numero que se envio
        ResponseEntity<LiquidacionDTO> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, entity,
                        new ParameterizedTypeReference<LiquidacionDTO>() {});
        if (responseEntity.getBody()==null) {
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST, "No se encontro la liquidacion");
        }
        return responseEntity.getBody(); 
    }
    /**
     * Genera token para comunicacion con WS acapulco-caja
     * @param usuario usuario para generar token 
     * @param pass clave de acceso para generar token
     * @return token obtenido
     */
    public TokenDTO generaToken(String usuario, String pass) {
        // se definie las credenciales FIJAS
        AuthenticateDTO auth = new AuthenticateDTO();
        auth.setUsername(usuario); // user
        auth.setRememberMe(true);
        auth.setPassword(pass);    // user
        // se definie los headers
        Map<String, String> vars = new HashMap<>();
        vars.put("Content-Type", "application/json");
        // se consulta el ws que genera el token
        RestTemplate rt = new RestTemplate();
        rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        rt.getMessageConverters().add(new StringHttpMessageConverter());
        String uri = this._host + ":" + this._port + this._autenticate;
        TokenDTO token = new TokenDTO();
        token = rt.postForObject(uri, auth, TokenDTO.class, vars);
        // regresa el token
        return token;
    }

    /**
     * Metodo que busca el historial del usuario que lo desea
     * @param pageable
     * @return
     * @throws ExceptionAPI
     * @throws Exception 
     */
    @Override
    public List<HistorialDTO> obtenerHistorial(Pageable pageable) throws ExceptionAPI, Exception {
        pageable = new PageRequest(pageable.getPageNumber()-1, pageable.getPageSize(), null);
        List<HistorialDTO> historial = new ArrayList<>();        
        Long idUsuario = Long.parseLong(SecurityUtils.getCurrentUserLogin()); 
        PlCarro carro = plCarroRepository.findByUser(idUsuario);
        log.debug("-----------------------"+idUsuario+"-------"+carro.getId());
        if (carro==null) {
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST, "No hay carrito");
        }
        Page<PlIntentoPago> plIntentoPago = plIntentoPagoRepository.findByHistorialcarroAndStatus(carro.getId(), StatusIntentoPago.ACEPTADA, pageable);
        log.debug("-----------------------"+plIntentoPago.getTotalElements());
        if (plIntentoPago==null) {
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST, "No hay historial de pagos");
        }
        for (PlIntentoPago intentoPago : plIntentoPago) {
            log.debug("-----------------------"+intentoPago.toString());                       
            PlParamRespuesta plRespuesta=plParamRespuestaRepository.buscarImporte(intentoPago.getId(),TipoParamBanco.IN,"mp_amount"); 
            String importe = plRespuesta.getValor();
            HistorialDTO hist = new HistorialDTO(intentoPago.getId(),intentoPago.getFecha(),intentoPago.getStatus(),importe);
            historial.add(hist);
        }
        
        return historial;
    }
    
    /**
     * Metodo que obtiene el numero de paginas que  contiene el paginador
     * @param numeroPaginasDTO
     * @return
     * @throws ExceptionAPI 
     */
    @Override
    public double getNumPaginas(int numeroPaginasDTO) throws ExceptionAPI {
        double paginas = 0;
        Long idUsuario = Long.parseLong(SecurityUtils.getCurrentUserLogin()); 
        PlCarro carro = plCarroRepository.findByUser(idUsuario);
        if (carro==null) {
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST, "No hay carrito");
        }
        List<PlIntentoPago> plIntentoPago = plIntentoPagoRepository.findByHistorialcarroAndStatusPaginas(carro.getId(), StatusIntentoPago.ACEPTADA);
        //realizamos la operacion para obtener en numero de paginas
        float numDePaginas = (float)(plIntentoPago.size())/(float)numeroPaginasDTO;
        return paginas = Math.ceil(numDePaginas);  
    }
}
