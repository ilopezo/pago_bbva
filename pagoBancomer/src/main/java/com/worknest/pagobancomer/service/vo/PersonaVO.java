/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worknest.pagobancomer.service.vo;

import com.worknest.pagobancomer.domain.enumeration.EdoCivil;
import com.worknest.pagobancomer.domain.enumeration.Genero;
import java.time.LocalDate;

/**
 *
 * @author WorkNest9
 */
public class PersonaVO {
    private Long id;
    
    private String curp;
    
    private String nombre;
    
    private String apaterno;
    
    private String amaterno;
    
    private Genero sexo;
    
    private LocalDate fechanac;
    
    private EdoCivil edoCivil;
    
    public PersonaVO() {
    }

    public PersonaVO(String curp, String nombre, String apaterno, String amaterno, Genero sexo, LocalDate fechanac, EdoCivil edoCivil) {
        this.curp = curp;
        this.nombre = nombre;
        this.apaterno = apaterno;
        this.amaterno = amaterno;
        this.sexo = sexo;
        this.fechanac = fechanac;
        this.edoCivil = edoCivil;
    }

    public PersonaVO(Long id, String curp, String nombre, String apaterno, String amaterno, Genero sexo, LocalDate fechanac, EdoCivil edoCivil) {
        this.id = id;
        this.curp = curp;
        this.nombre = nombre;
        this.apaterno = apaterno;
        this.amaterno = amaterno;
        this.sexo = sexo;
        this.fechanac = fechanac;
        this.edoCivil = edoCivil;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApaterno() {
        return apaterno;
    }

    public void setApaterno(String apaterno) {
        this.apaterno = apaterno;
    }

    public String getAmaterno() {
        return amaterno;
    }

    public void setAmaterno(String amaterno) {
        this.amaterno = amaterno;
    }

    public Genero getSexo() {
        return sexo;
    }

    public void setSexo(Genero sexo) {
        this.sexo = sexo;
    }

    public LocalDate getFechanac() {
        return fechanac;
    }

    public void setFechanac(LocalDate fechanac) {
        this.fechanac = fechanac;
    }

    public EdoCivil getEdoCivil() {
        return edoCivil;
    }

    public void setEdoCivil(EdoCivil edoCivil) {
        this.edoCivil = edoCivil;
    }
    
    @Override
    public String toString() {
        return "Persona{" +
            "id=" + getId() +
            ", curp='" + getCurp() + "'" +
            ", nombre='" + getNombre() + "'" +
            ", apaterno='" + getApaterno() + "'" +
            ", amaterno='" + getAmaterno() + "'" +
            ", sexo='" + getSexo() + "'" +
            ", fechanac='" + getFechanac() + "'" +
            ", edoCivil='" + getEdoCivil() + "'" +
            "}";
    }
}
