package com.worknest.pagobancomer.service.impl;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.worknest.pagobancomer.domain.PlIntentoPago;
import com.worknest.pagobancomer.service.CorreosService;
import com.worknest.pagobancomer.service.dto.CuerpoCorreo;
import com.worknest.pagobancomer.web.rest.util.UtilCadenas;
import com.worknest.pagobancomer.web.rest.util.UtilFechas;
import org.springframework.http.HttpStatus;

@Service
@Transactional
public class CorreosServiceImpl implements CorreosService {

    private final Logger log = LoggerFactory.getLogger(CorreosServiceImpl.class);
    
    @Value("${correos.llaveOrigen}") private String _llaveOrigen;
    @Value("${correos.llaveTitulo}") private String _llaveTitulo;
    @Value("${correos.llaveSistema}") private String _llaveSistema;
    @Value("${correos.llaveUrlDetallePago}") private String _llaveUrlDetallePago;
    @Value("${correos.llaveAvisoPrivacidad}") private String _llaveAvisoPrivacidad;
    @Value("${correos.llavePoliticasUso}") private String _llavePoliticasUso;
    @Value("${correos.clave}") private String _clave;
    @Value("${correos.llaveLogo}") private String _llaveLogo;
    
    public CorreosServiceImpl() {
    	
    }
    
    @Override
    public Boolean enviarCorreo(PlIntentoPago intentoPago, BigDecimal importe, String nombre, String authCode,
                            String textoConceptos, String link, String ruta, String correo, String urlBase) {
        Boolean exito = true;
        try {
            // Mandar a llamar pagos en sidueer y mesa de ejecucion.
            CuerpoCorreo cuerpoCorreo = new CuerpoCorreo();
            cuerpoCorreo.setFechaPago(Date.from(intentoPago.getFecha().toInstant())); // fecha de pago
            cuerpoCorreo.setNombreUsuario(nombre);

            // No. de transaccion
            cuerpoCorreo.setNoTransaccion(authCode);
            cuerpoCorreo.setNoPago(intentoPago.getId() != null ? intentoPago.getId().intValue() : null); // id intento pago
            cuerpoCorreo.setConceptoCobro(textoConceptos);
            cuerpoCorreo.setTextoLink(link);
            cuerpoCorreo.setTotal(importe); // importe
            cuerpoCorreo.setUrlBase(urlBase);
            enviarCorreoPagoRealizado(cuerpoCorreo, ruta, correo);

        } catch (Exception e) {
            exito=false;
        }
        
        return exito;
    }

    /**
    * Método que permite enviar un correo electrónico.
    * @param cuerpoCorreo
    * @param url
    * @param correoUsuario
    * relacionada con la operación.
    */
    private void enviarCorreoPagoRealizado(CuerpoCorreo cuerpoCorreo, String url, String correoUsuario) throws Exception{
        String direccionOrigen = this._llaveOrigen;
        String claveCorreo = this._clave;
        String titulo = this._llaveTitulo;
        // String sistema = this._llaveSistema;
        String urlDetalle = this._llaveUrlDetallePago;

        cuerpoCorreo.setAvisoPrivacidad(this._llaveAvisoPrivacidad);
        cuerpoCorreo.setPoliticasUso(this._llavePoliticasUso);
        cuerpoCorreo.setUrlDetallePago(cuerpoCorreo.getUrlBase() + urlDetalle + cuerpoCorreo.getNoPago() + ".html");

        enviarCorreoTextoFormateado(direccionOrigen, claveCorreo, correoUsuario, titulo, getCuerpoTextoFormateadoParaHTML(cuerpoCorreo), url);
    }
	
    /**
    * Métod que permite enviar correos electronicos con el texto formateado con html
    * @param correoOrigen correo de donde va a salir el correo electrónico
    * @param claveCorreo clave del correo donde va a salir el correo electrónico
    * @param correoDestino correo electrónico a donde se va a enviar el correo
    * @param asunto mensaje del asunto del correo
    * @param texto es el cuerpo del correo electrónico
    * @param url es la url base de donde se van a obtener las imagenes
    * @return Regresa un objeto Token. Si la función termina correctamente, la
    * propiedad <code>ok</code> es verdadera y la propiedad <code>objeto</code>
    * contiene un null. Si ocurre un fallo, la propiedad <code>ok</code> es
    * falso y la propiedad <code>mensaje</code> tiene un mensaje para el
    * usuario final.
    */
    private void enviarCorreoTextoFormateado(final String correoOrigen, final String claveCorreo, String correoDestino, String asunto,
                                                String texto, String url) throws Exception{

        try {
            // Se crea un objeto properties para configurar el servidor de correo
            Properties parametros = new Properties();
            parametros.put("mail.smtp.host", "smtp.gmail.com");
            parametros.put("mail.smtp.port", "587");
            parametros.put("mail.transport.protocol", "smtp");
            parametros.put("mail.smtp.auth", "true");
            parametros.put("mail.smtp.starttls.enable", "true");
            parametros.setProperty("mail.user", correoOrigen);
            parametros.setProperty("mail.password", claveCorreo);
            // Se crea un objeto de sesion con el servidor de correo
            Session sesion = Session.getInstance(parametros, new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(correoOrigen, claveCorreo);
                }
            });
            // Se crea el mensaje a enviar
            Message mensaje = new MimeMessage(sesion);
            // Se crean los objetos Adress correspondientes a las direcciones de correo involucradas
            Address desde = new InternetAddress(correoOrigen);
            Address para = new InternetAddress(correoDestino);
            // Se asigna el origen del correo
            mensaje.setFrom(desde);
            // Se asigna el destinatario
            mensaje.setRecipient(Message.RecipientType.TO, para);
            // Se asigna el asunto del correo
            mensaje.setSubject(asunto);

            // This HTML mail have to 2 part, the BODY and the embedded image
            MimeMultipart multipart = new MimeMultipart();
            //first part the HTML
            BodyPart cuerpoCorreo = new MimeBodyPart();
            cuerpoCorreo.setContent(texto, "text/html; charset=utf-8");

            //add it
            multipart.addBodyPart(cuerpoCorreo);
            //second part the image
            cuerpoCorreo = new MimeBodyPart();
            DataSource dataSource = new FileDataSource(url + this._llaveLogo);
            cuerpoCorreo.setDataHandler(new DataHandler(dataSource));
            cuerpoCorreo.addHeader("Content-Type", "image/png");
            cuerpoCorreo.setHeader("Content-ID", "<image>");

            //add it
            multipart.addBodyPart(cuerpoCorreo);

            // Se asigna el texto del correo
            // put everything together
            mensaje.setContent(multipart);
            // Se envia el mensaje
            Transport transport = sesion.getTransport("smtp");
            transport.connect("smtp.gmail.com", correoOrigen, claveCorreo);
            mensaje.setFrom(para);
            transport.sendMessage(mensaje, mensaje.getAllRecipients());

        } catch (MessagingException e) {
            // En caso de error se notifica
            log.error(e.getMessage(), e.getMessage());
            throw new Exception(e);
        }
    }

    /**
    * Método que permite crear el cuerpo del correo del pago de los conceptos
    * del impuesto predial
    *
    * @param cuerpoCorreo objeto de tipo CuerpoCorreo
    * @return regresa un objeto de tipo String con los datos formateados del
    * cuerpo del correo
    */
    private String getCuerpoTextoFormateadoParaHTML(CuerpoCorreo cuerpoCorreo) throws Exception{
        StringBuilder textoFormateado = new StringBuilder();
        //Inicio de Encabezados
        textoFormateado.append("<div align=\"center\">")
                        .append("<table width=\"100%\" height=\"\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">")
                        .append("<tbody>")
                        .append("<tr>")
                        .append("<td align=\"center\">")
                        .append("<table width=\"520\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-size:14px;font-weight:400;text-align:left;background-color:#e9e9e9;color:#333;border-color: #d0d0d0;border-style: solid;border-width: 1px;\">")
                        .append("<tbody>");

        //logo e Info del ayto
        textoFormateado.append("<tr>")
                        .append("<td>")
                        .append("<table>")
                        .append("<tbody>")
                        .append("<tr>")
                        //image
                        .append("<td><img width=\"122\" height=\"110\" src=\"cid:image\"></td>")
                        .append("<td style=\"width:100%;\">")
                        //Info ayto
                        .append("<table style=\"text-align:center;\" width=\"100%\">")
                        .append("<tbody>")
                        .append("<tr><td style=\"color:#333;font-weight:300;\"><b>GOBIERNO MUNICIPAL</b></td></tr>")
                        .append("<tr><td style=\"color:#333;font-weight:300;\"><b>")
                        .append(cuerpoCorreo.getConceptoCobro())
                        .append("</b></td></tr>")
                        .append("<tr><td style=\"color:#333;font-weight:300;\"><b>Sistema de Servicios Generales en L&iacute;nea</b></td></tr>")
                        .append("</tbody>")
                        .append("</table>")
                        .append("</td>")
                        .append("</tr>")
                        .append("</tbody>")
                        .append("</table>")
                        .append("</td>")
                        .append("</tr>");
        //end logo e Info del ayto
        //Información de los  detalles
        textoFormateado.append("<tr>")
                        .append("<td style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;text-align:left; background-color:#e9e9e9;border-top-width:1px;border-top-style:solid;border-top-color:#bfc1c5;color:#333;padding:0 30px 0 30px\">")
                        .append("<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;padding-bottom:120px\">")
                        .append("<tbody>")
                        .append("<tr>")
                        .append("<td>")
                        //texto detalles
                        .append("<table  cellspacing=\"0\" cellpadding=\"0\" border=\"0\">")
                        .append("<tbody>")
                        .append("<tr>")
                        .append("<td style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-weight:200;font-size:24px;color:#333;padding:9px 0 25px;margin:0px;display:block\">")
                        .append("Detalles del pago realizado")
                        .append("</td>")
                        .append("</tr>")
                        .append("</tbody>")
                        .append("</table>")
                        // End texto detalles
                        //Inicio Detalles del pago
                        .append("<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">")
                        .append("<tbody>")
                        //Inicio row detalle
                        .append("<tr>")
                        .append("<td height=\"22\" style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-weight:300;color:#333;padding: 2px 0;width: 273px;\">")
                        .append("Fecha de pago")
                        .append("</td>")
                        .append("<td> <b style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-weight:300;color:#333;padding: 2px 0;\">")
                        .append(UtilFechas.convertirFecha(cuerpoCorreo.getFechaPago(), UtilFechas.FormatosFecha.FORMATO_LITTLE_ENDIAN_CON_HORA))
                        .append("</b></td>")
                        .append("</tr>")
                        //Fin row detalle
                        //Inicio row detalle
                        .append("<tr>")
                        .append("<td height=\"22\" style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-weight:300;color:#333;padding: 2px 0;\">")
                        .append("Nombre del contribuyente")
                        .append("</td>")
                        .append("<td> <b style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-weight:300;color:#333;padding: 2px 0;\">")
                        .append(cuerpoCorreo.getNombreUsuario())
                        .append("</b></td>")
                        .append("</tr>")
                        //Fin row detalle
                        //Inicio row detalle
                        .append("<tr>")
                        .append("<td height=\"22\" style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-weight:300;color:#333;padding: 2px 0;\">")
                        .append("No. de transacci&oacute;n")
                        .append("</td>")
                        .append("<td> <b style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-weight:300;color:#333;padding: 2px 0;\">")
                        .append(cuerpoCorreo.getNoTransaccion())
                        .append("</b></td>")
                        .append("</tr>")
                        //Fin row detalle
                        //Inicio row detalle
                        .append("<tr>")
                        .append("<td height=\"22\" style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-weight:300;color:#333;padding: 2px 0;\">")
                        .append("No. de pago")
                        .append("</td>")
                        .append("<td> <b style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-weight:300;color:#333;padding: 2px 0;\">")
                        .append(cuerpoCorreo.getNoPago())
                        .append("</b></td>")
                        .append("</tr>")
                        //Fin row detalle 
                        //Inicio row detalle
                        .append("<tr>")
                        .append("<td height=\"22\" style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-weight:300;color:#333;padding: 2px 0;\">")
                        .append("Concepto de cobro")
                        .append("</td>")
                        .append("<td> <b style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-weight:300;color:#333;padding: 2px 0;\">")
                        .append(cuerpoCorreo.getConceptoCobro())
                        .append("</b></td>")
                        .append("</tr>")
                        //Fin row detalle
                        //Inicio row detalle
                        .append("<tr>")
                        .append("<td height=\"22\" style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-weight:300;color:#333;padding: 2px 0;\">")
                        .append("Importe del pago")
                        .append("</td>")
                        .append("<td> <b style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-weight:600;color:#333;text-decoration:underline;padding: 2px 0;\">")
                        .append(UtilCadenas.formatoContable(cuerpoCorreo.getTotal(), false)).append(" MXN")
                        .append("</b></td>")
                        .append("</tr>")
                        //Fin row detalle                
                        //Inicio row detalle
                        .append("<tr>")
                        .append("<td height=\"22\" ><a target=\"_blank\" href=\"").append(cuerpoCorreo.getUrlDetallePago()).append("\" style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;color:#0182d3;font-size:12px;text-decoration:underline;\">")
                        .append(cuerpoCorreo.getTextoLink())
                        .append("</a></td>")
                        .append("</tr>")
                        //Fin row detalle
                        .append("</tbody>")
                        .append("</table>")
                //End Detalles del pago
                //leyenda detalles facturar electronicamente
                //                .append("<table  height=\"\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">")
                //                .append("<tbody>")
                //                .append("<tr>")
                //                .append("<td style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-weight:200;font-size:17px;color:#333;padding:19px 0 8px;margin:0px;display:block\">")
                //                .append("Datos para facturar electr&oacute;nicamente su pago")
                //                .append("</td>")
                //                .append("</tr>")
                //                .append("</tbody>")
                //                .append("</table>")
                //                // End leyenda detalles factuar electronicamente
                //                //inicio texto detalles factuar electronicamente
                //                .append("<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">")
                //                .append("<tbody>")
                //                //Inicio row detalle facturar
                //                .append("<tr>")
                //                .append("<td style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-weight:300;color:#333;padding: 2px 0;width: 58px;\">")
                //                .append("Folio")
                //                .append("</td>")
                //                .append("<td> <b style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-weight:600;color:#333;padding: 2px 0;\">")
                //                .append(cuerpoCorreo.getFolio())
                //                .append("</b></td>")
                //                .append("</tr>")
                //                //Fin row detalle facturar
                //                //Inicio row detalle facturar
                //                .append("<tr>")
                //                .append("<td style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-weight:300;color:#333;padding: 2px 0;\">")
                //                .append("Token")
                //                .append("</td>")
                //                .append("<td> <b style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-weight:600;color:#333;padding: 2px 0;\">")
                //                .append(cuerpoCorreo.getToken())
                //                .append("</b></td>")
                //                .append("</tr>")
                //                //Fin row detalle facturar
                //                .append("</tbody>")
                //                .append("</table>")
                //end texto detalles factuar electronicamente
                        .append("</td>")
                        .append("</tr>")
                        .append("</tbody>")
                        .append("</table>")
                        .append("</td>")
                        .append("</tr>");
        // end Información de los  detalles

        //Inicio seccion footer
        textoFormateado.append("<tr>")
                        .append("<td style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-size:12px; color:#333; border-top:1px solid #bfc1c5;padding:10px 29px 0 9px;background-color: #dedede;\">")
                        .append("<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif\">")
                        .append("<tbody>")
                        .append("<tr>")
                        .append(" <td valign=\"top\"> ")
                        .append("<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">")
                        .append("<tbody>")
                        .append("<tr>")
                        .append("<td style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-size:12px;padding-bottom:5px; color:#838383; text-align:justify;\">")
                        .append("Este correo ha sido enviado a usted debido a que se realiz&oacute; un pago en l&iacute;nea, y su cuenta de acceso tiene asociado este correo electr&oacute;nico. Le recordamos que puede revisar las <a href=\"").append(cuerpoCorreo.getPoliticasUso()).append("\" style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-size:12px;padding-bottom:5px; color:#838383; text-decoration:underline; \">politicas de uso</a> de este sistema, as&iacute; como el <a href=\"").append(cuerpoCorreo.getAvisoPrivacidad()).append("\"  style=\"font-family:'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;font-size:12px;padding-bottom:5px; color:#838383; text-decoration:underline;\">aviso de privacidad</a>. ")
                        .append("<br /><br />")
                        .append("<span style=\"font-size: 15px;\">Soporte t&eacute;cnico</span>")
                        .append("<br />")
                        .append("Tel&eacute;fono: (999) 999 999 ")
                        .append("<br />")
                        .append("Correo electr&oacute;nico: sistemas@municipio.gob.mx")
                        .append("</td>")
                        .append("</tr>")
                        .append("</tbody>")
                        .append("</table>")
                        .append("</td>")
                        .append("</tr>")
                        .append("</tbody>")
                        .append("</table>")
                        .append("</td>")
                        .append("</tr>");
                // end seccion footer

//Cierre de etiquetas Encabezados
        textoFormateado.append("</tbody>")
                        .append("</table>")
                        .append("</td>")
                        .append("</tr>")
                        .append("</tbody>")
                        .append("</table>")
                        .append("</div>");

        return textoFormateado.toString();
    }

}
