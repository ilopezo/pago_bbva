package com.worknest.pagobancomer.service.dto;


import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * A DTO for the PlUserClavesCatastrales entity.
 */
public class PlUserClavesCatastralesDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 31)
    private String llave;

    private Long userId;

    private String userLogin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLlave() {
        return llave;
    }

    public void setLlave(String llave) {
        this.llave = llave;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PlUserClavesCatastralesDTO plUserClavesCatastralesDTO = (PlUserClavesCatastralesDTO) o;
        if(plUserClavesCatastralesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plUserClavesCatastralesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlUserClavesCatastralesDTO{" +
            "id=" + getId() +
            ", llave='" + getLlave() + "'" +
            "}";
    }
}
