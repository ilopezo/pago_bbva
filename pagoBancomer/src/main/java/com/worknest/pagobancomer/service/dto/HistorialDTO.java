/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worknest.pagobancomer.service.dto;

import com.worknest.pagobancomer.domain.enumeration.StatusIntentoPago;
import java.time.ZonedDateTime;

/**
 *
 * @author WorkNest9
 */
public class HistorialDTO {
    private Long idPago;
    
    private ZonedDateTime fecha;
    
    private StatusIntentoPago estatus;
    
    private String importe;

    public HistorialDTO() {
    }

    public HistorialDTO(Long idPago, ZonedDateTime fecha, StatusIntentoPago estatus, String importe) {
        this.idPago = idPago;
        this.fecha = fecha;
        this.estatus = estatus;
        this.importe = importe;
    }

    public Long getIdPago() {
        return idPago;
    }

    public void setIdPago(Long idPago) {
        this.idPago = idPago;
    }

    public ZonedDateTime getFecha() {
        return fecha;
    }

    public void setFecha(ZonedDateTime fecha) {
        this.fecha = fecha;
    }

    public StatusIntentoPago getEstatus() {
        return estatus;
    }

    public void setEstatus(StatusIntentoPago estatus) {
        this.estatus = estatus;
    }

    public String getImporte() {
        return importe;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }
    
}
