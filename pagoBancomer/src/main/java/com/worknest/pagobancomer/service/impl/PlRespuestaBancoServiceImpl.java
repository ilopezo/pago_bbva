package com.worknest.pagobancomer.service.impl;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.worknest.pagobancomer.domain.PlCarro;
import com.worknest.pagobancomer.domain.PlCarroDet;
import com.worknest.pagobancomer.domain.PlCarroHist;
import com.worknest.pagobancomer.domain.PlIntentoPago;
import com.worknest.pagobancomer.domain.PlParamBanco;
import com.worknest.pagobancomer.domain.PlParamRespuesta;
import com.worknest.pagobancomer.domain.PlRespuestaBanco;
import com.worknest.pagobancomer.domain.User;
import com.worknest.pagobancomer.domain.enumeration.StatusIntentoPago;
import com.worknest.pagobancomer.repository.PlCarroDetRepository;
import com.worknest.pagobancomer.repository.PlCarroHistRepository;
import com.worknest.pagobancomer.repository.PlCarroRepository;
import com.worknest.pagobancomer.repository.PlIntentoPagoRepository;
import com.worknest.pagobancomer.repository.PlParamBancoRepository;
import com.worknest.pagobancomer.repository.PlParamRespuestaRepository;
import com.worknest.pagobancomer.repository.PlRespuestaBancoRepository;
import com.worknest.pagobancomer.service.CorreosService;
import com.worknest.pagobancomer.service.PlRespuestaBancoService;
import com.worknest.pagobancomer.service.dto.AuthenticateDTO;
import com.worknest.pagobancomer.service.dto.DatosConciliacionDTO;
import com.worknest.pagobancomer.service.dto.LiquidacionDTO;
import com.worknest.pagobancomer.service.dto.PagoLineaDTO;
import com.worknest.pagobancomer.service.dto.PlRespuestaBancoDTO;
import com.worknest.pagobancomer.service.dto.TokenDTO;
import com.worknest.pagobancomer.service.mapper.PlRespuestaBancoMapper;
import com.worknest.pagobancomer.web.rest.errors.ExceptionAPI;
import java.text.DecimalFormat;

/**
 * Service Implementation for managing PlRespuestaBanco.
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class PlRespuestaBancoServiceImpl implements PlRespuestaBancoService{

    private final Logger log = LoggerFactory.getLogger(PlRespuestaBancoServiceImpl.class);

    private final PlRespuestaBancoRepository plRespuestaBancoRepository;

    private final PlRespuestaBancoMapper plRespuestaBancoMapper;
    
    private final PlCarroRepository plCarroRepository;
    
    private final PlCarroHistRepository plCarroHistRepository;
    
    private final PlIntentoPagoRepository plIntentoPagoRepository;
    
    private final PlParamBancoRepository plParamBancoRepository;
    
    private final PlParamRespuestaRepository plParamRespuestaRepository;

    private final PlCarroDetRepository plCarroDetRepository;

    //private final ApplicationProperties applicationProperties;
    //private final CorreoProperties correoProperties;
    private String _token;

    @Autowired
    ServletContext context;
    
    private final CorreosService correosService;
    
    @Value("${application.host}") private String _host;
    @Value("${application.port}") private String _port;
    @Value("${application.autenticate}") private String _autenticate;
    @Value("${application.lpgenera}") private String _lpgenera;
    @Value("${application.deleteLiq}") private String _deleteLiq;
    @Value("${application.usuario}") private String _usuario;
    @Value("${application.pass}") private String _pass;
    @Value("${application.pagarLiq}") private String _pagarLiq;
    @Value("${application.urlusuario}") private String _urlusuario;
    @Value("${application.account}") private String _account;
    
    @Value("${correos.textoLinkPredial}") private String _textoLinkPredial;
    @Value("${correos.textoConceptosCobrosPredial}") private String _textoConceptosCobrosPredial;
    @Value("${correos.urlBase}") private String _urlBase;

    public PlRespuestaBancoServiceImpl(PlRespuestaBancoRepository plRespuestaBancoRepository, PlRespuestaBancoMapper plRespuestaBancoMapper,
                                        PlCarroRepository plCarroRepository, PlCarroHistRepository plCarroHistRepository,PlIntentoPagoRepository plIntentoPagoRepository,
                                        PlParamBancoRepository plParamBancoRepository,  PlParamRespuestaRepository plParamRespuestaRepository, PlCarroDetRepository plCarroDetRepository,
                                        /*ApplicationProperties applicationProperties, CorreoProperties correoProperties*/
                                        CorreosService correosService) {
        this.plRespuestaBancoRepository = plRespuestaBancoRepository;
        this.plRespuestaBancoMapper = plRespuestaBancoMapper;
        this.plCarroRepository = plCarroRepository;
        this.plCarroHistRepository = plCarroHistRepository;
        this.plIntentoPagoRepository = plIntentoPagoRepository;
        this.plParamBancoRepository = plParamBancoRepository;
        this.plParamRespuestaRepository = plParamRespuestaRepository;
        this.plCarroDetRepository = plCarroDetRepository;
        //this.applicationProperties = applicationProperties;
        //this.correoProperties = correoProperties;
        this.correosService = correosService;
    }

    /**
     * Save a plRespuestaBanco.
     *
     * @param plRespuestaBancoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PlRespuestaBancoDTO save(PlRespuestaBancoDTO plRespuestaBancoDTO) {
        log.debug("Request to save PlRespuestaBanco : {}", plRespuestaBancoDTO);
        PlRespuestaBanco plRespuestaBanco = plRespuestaBancoMapper.toEntity(plRespuestaBancoDTO);
        plRespuestaBanco = plRespuestaBancoRepository.save(plRespuestaBanco);
        return plRespuestaBancoMapper.toDto(plRespuestaBanco);
    }

    /**
     *  Get all the plRespuestaBancos.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PlRespuestaBancoDTO> findAll() {
        log.debug("Request to get all PlRespuestaBancos");
        return plRespuestaBancoRepository.findAll().stream()
            .map(plRespuestaBancoMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one plRespuestaBanco by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PlRespuestaBancoDTO findOne(Long id) {
        log.debug("Request to get PlRespuestaBanco : {}", id);
        PlRespuestaBanco plRespuestaBanco = plRespuestaBancoRepository.findOne(id);
        return plRespuestaBancoMapper.toDto(plRespuestaBanco);
    }

    /**
     *  Delete the  plRespuestaBanco by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PlRespuestaBanco : {}", id);
        plRespuestaBancoRepository.delete(id);
    }

    /**
     * Guarda la respuesta del banco cuando el pago fue aceptado o rechazado
     * @param respuestaBanco contiene los parametros 
     * @param aceptado si el pago fue aceptado es true
     * @return 
     * @throws com.worknest.pagobancomer.web.rest.errors.ExceptionAPI
     */
    @Override
    public String guardarRespuesta(Map<String, String> respuestaBanco, Boolean aceptado) throws ExceptionAPI, Exception {   	
    	
        String mensaje="";
    	String auth = "";
        PlRespuestaBanco plRespuestaBanco = new PlRespuestaBanco(); //se crea una respuesta banco para guardar los datos
        PlParamBanco paramBanco = new PlParamBanco();
        PlCarro carro= new PlCarro();
        PlCarroHist carroHist= new PlCarroHist();
        PlIntentoPago intentoPago = new PlIntentoPago();
        Long idCarro=null;
        //se recorre el map con la respuesta que envia el banco para crear la respuesta banco
        for (Map.Entry respuesta : respuestaBanco.entrySet()) {
            if (respuesta.getKey().toString().equalsIgnoreCase("mp_order")) {
                //se busca el carro por el id que contiene como valor el mp_order
                idCarro=Long.parseLong(respuesta.getValue().toString());
                carro = plCarroRepository.findOne(idCarro);
                //se busca el ultimo registro del carroHist con el carro de la persona que realizo el pago
                carroHist = plCarroHistRepository.findByCarroOrderByIdDesc(carro.getId()); 
                //se busca el intento de pago del carroHist que tenga el campo enviado false
                intentoPago = plIntentoPagoRepository.findByUltimoCarroHist(carroHist,false);
                intentoPago.setEnviado(true); //se cambia si fue enviado a true
                if (aceptado) {//si el pago fue aceptado cambia el estatus del intento pago
                   intentoPago.setStatus(StatusIntentoPago.ACEPTADA);
                } else{ //de lo contrario el estatus del intento cambia a rechazada
                    intentoPago.setStatus(StatusIntentoPago.RECHAZADA);
                }
                plRespuestaBanco.setIntentoPago(intentoPago);     //se guarda la relacion entre el intento y la respuesta del banco
                plRespuestaBanco.setExitoso(aceptado);            //se guarda si el pago fue exitoso o no
                plRespuestaBanco.setFecha(ZonedDateTime.now());   //se guarda la fecha actual
                plRespuestaBancoRepository.save(plRespuestaBanco);//se guarda la respuesta del banco en la base de datos
            } else if (respuesta.getKey().toString().equalsIgnoreCase("mp_authorization")) {
            	auth = respuesta.getValue().toString();
                intentoPago.setAuth(respuesta.getValue().toString());   //se guarda el valor de la authorizacion en el intento pago
            }
        }
        plIntentoPagoRepository.save(intentoPago); //se guardan los cambios del intento pago
        //se recorre el map con la respuesta que envia el banco para guardar los parametros de respuesta
        
        List<PlParamRespuesta> plParamRespuestaL = new ArrayList<PlParamRespuesta>();
        
        for (Map.Entry respuesta : respuestaBanco.entrySet()) {
            //se busca el valor del key del map en la lista de parametros banco
            PlParamRespuesta plParamRespuesta = new PlParamRespuesta();
            paramBanco = plParamBancoRepository.findByNombre(respuesta.getKey().toString(),"IN","00001");
            plParamRespuesta.setParamBanco(paramBanco);           //se agrega el id del parametro banco
            plParamRespuesta.setValor(respuesta.getValue().toString()); //se agrega el valor que envia el banco
            plParamRespuesta.setRespuestaBanco(plRespuestaBanco); //se agrega el id de la respuesta banco     
            //Se agrega a la lista de parametros que se guardará
            plParamRespuestaL.add(plParamRespuesta);
        }
        //se guardan la lista de los parametros de respuesta en la base de datos
        plParamRespuestaRepository.save(plParamRespuestaL);
        
        if(aceptado){
            // L I B E R A R   A D E U D O S
            // busca el carro del usuario que realizo el pago
            PlCarro plCarro = this.plCarroRepository.findOne(idCarro);
            // si no se encuentran conceptos en el carro se devuelve un mensaje de error
            if (plCarro == null) {
                // se arroja una excepcion personalizada con mensaje personalizado
               throw new ExceptionAPI(HttpStatus.BAD_REQUEST, "No se encontró el carro");
            }
            // Se buscan los conceptos que se borraran
            List<PlCarroDet> conceptos = this.plCarroDetRepository.findByCarroId(plCarro.getId());
            
            for (PlCarroDet concepto : conceptos) {
                log.debug("-----------------"+concepto.getLiquidacion()+"-----"+conceptos.size());
            }
            if (conceptos.isEmpty()) {
                // se arroja una excepcion personalizada con mensaje personalizado
                throw new ExceptionAPI(HttpStatus.BAD_REQUEST, "No se encuentra los conceptos");
            }
            List<Long> ids = new ArrayList<>() ;//= conceptos.stream().map(m -> m.getLiquidacion().getId()).collect(Collectors.toList());
            
            conceptos.forEach((concepto) -> {
                ids.add(concepto.getLiquidacion().getId());
            });
            
            for (Long id : ids) {
                log.debug("-----------------"+id+"-----"+ids.size());
            }
            this.plCarroDetRepository.delete(conceptos);

            // se liberan los adeudos ejecutando el WS que liquida adeudos
            
            PagoLineaDTO envio = new PagoLineaDTO();
            envio.setAuth(auth);
            envio.setIdLiqs(ids); 
       /*     try {
                // genera token
                // validacion
                if (this._token == null)
                    this._token = this.generaToken(this._usuario, this._pass).getId_token();

                // Se consulta el WS que genera el token
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                headers.set("Authorization", "Bearer " + this._token);

                //Genera la url para consumir el WS que genera liquidaciones
                String uri = new String(this._host + ":" + this._port + this._pagarLiq);

                HttpEntity<?> entity = new HttpEntity<PagoLineaDTO>(envio, headers);

                // El WS genera las liquidaciones en base a los datos de envio
                restTemplate.exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<List<LiquidacionDTO>>() {});

            } catch (HttpStatusCodeException e) {
                throw new ExceptionAPI(e.getStatusCode(), "Problemas al Pagar las liquidaciones: " + e.getMessage());
            }
            /* ******** ENVIO DE CORREO ********** */
            Boolean exitoso =correosService.enviarCorreo(intentoPago, //se manda el objeto intento pago
                                                        new BigDecimal(respuestaBanco.get("mp_amount")),// se manda el importe
                                                        obtenerInfoUsuario(carro.getUser().toString()).getFirstName(), // se manda el nombre del usuario
                                                        auth, //se el codigo de authorizacion
                                                        _textoConceptosCobrosPredial, //se manda el concepto de pago
                                                        this._textoLinkPredial, // se manda el enlace de predial
                                                        context.getRealPath("/"), // se manda la ruta
                                                        respuestaBanco.get("mp_email"),// se manda el correo del usuario
                                                        _urlBase);//se manda el urlBase
            mensaje="Pago realizado para la referencia: " + respuestaBanco.get("mp_reference");
            if (!exitoso) {
                mensaje+=" Problemas al enviar el correo " ;
            }
        }else{
            mensaje ="Pago rechazado para la referencia: " + respuestaBanco.get("mp_reference");
        }
        return mensaje;
    }
    
    /**
     * Genera token para comunicacion con WS acapulco-caja
     * @param usuario usuario para generar token 
     * @param pass clave de acceso para generar token
     * @return token obtenido
     */
    public TokenDTO generaToken(String usuario, String pass) {

        // se definie las credenciales FIJAS
        AuthenticateDTO auth = new AuthenticateDTO();

        auth.setUsername(usuario); // user
        auth.setRememberMe(true);
        auth.setPassword(pass);    // user

        // se definie los headers
        Map<String, String> vars = new HashMap<>();
        vars.put("Content-Type", "application/json");

        // se consulta el ws que genera el token
        RestTemplate rt = new RestTemplate();
        rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        rt.getMessageConverters().add(new StringHttpMessageConverter());

        String uri = new String(this._host + ":" + this._port + this._autenticate);

        TokenDTO token = new TokenDTO();

        token = rt.postForObject(uri, auth, TokenDTO.class, vars);

        // regresa el token
        return token;
    }
    
    public User obtenerInfoUsuario(String userId){
        
        this._token = this.generaToken(this._usuario, this._pass).getId_token();
        
        // Se consulta el WS que genera el token
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + this._token);

        //Genera la url para consumir el WS que busque la informacion del usuario
        String uri = new String(this._urlusuario + this._account);
        log.debug("----------------------------------"+userId);

        HttpEntity<?> entity = new HttpEntity<String>(userId, headers);
        // El WS genera las liquidaciones en base a los datos de envio
        
        ResponseEntity<User> usuario = restTemplate.exchange(uri, HttpMethod.POST, entity, User.class);
        
        User user = usuario.getBody();
        log.debug("----------------------------------"+user.toString());
        return user;
    }

    @Override
    public Map<String, String> obtenerDatos(DatosConciliacionDTO datos) throws ExceptionAPI {

        Map<String,String> datosParametros = new HashMap<>();
        log.debug("--------------1---------------------");
        List<PlParamBanco> parametrosDefault = plParamBancoRepository.getParamsActivosByClaveBanco("00001");    	

    	for(PlParamBanco param : parametrosDefault){	
            switch (param.getNombre()) {
                case "mp_account":
                    datosParametros.put("mp_account", param.getValorDefault());
                    break;
                case "mp_currency":
                    datosParametros.put("mp_currency", param.getValorDefault());
                    break;
                default:                   
                    break;
            }
    	}
        log.debug("---------------2--------------------");
    	//DecimalFormat df = new DecimalFormat("#.00");
    	List<PlParamBanco> parametros = plParamBancoRepository.getParamsActivosByBanco("00001");    	
        log.debug("-------------------3----------------");
    	for(PlParamBanco param : parametros){	
            switch (param.getNombre()) {
                case "mp_date":
                    datosParametros.put("mp_date", datos.getFecha());
                    break;
                case "mp_node":
                    datosParametros.put("mp_node", datos.getNode());
                    break;
                case "mp_concept":
                    datosParametros.put("mp_concept", datos.getConcepto());
                    break;
                case "mp_paymentmethod":
                    datosParametros.put("mp_paymentmethod", datos.getTipoPago());
                    break;
                case "mp_reference":
                    datosParametros.put("mp_reference", datos.getReferencia());
                    break;
                case "mp_order":
                    datosParametros.put("mp_order", datos.getNumOrden());
                    break;
                case "mp_authorization":
                    datosParametros.put("mp_authorization",datos.getAutorizacion() );
                    break; 
                case "mp_saleid":       
                    datosParametros.put("mp_saleid", datos.getIdVenta());
                    break; 	
                case "mp_amount":                   
                    datosParametros.put("mp_amount",  datos.getImporte());
                    break;                
                case "mp_bankname":                    
                    datosParametros.put("mp_bankname", datos.getNombreBanco() );
                    break;
                case "mp_cardholdername":                   
                    datosParametros.put("mp_cardholdername", datos.getNombrePagador());
                    break;   
                case "mp_email":                   
                    datosParametros.put("mp_email", datos.getEmail());
                    break;   
                case "mp_phone":                   
                    datosParametros.put("mp_phone", datos.getTelefono() );
                    break;     
                default:                   
                    break;
            }
    	}
        return datosParametros;
    }
}
