/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worknest.pagobancomer.service.vo;

/**
 *
 * @author WorkNest9
 */
public class RelPersonaEmailVO {
    private Long id;
    
    private EmailVO email;
    
    private PersonaVO persona;

    public RelPersonaEmailVO() {
    }

    public RelPersonaEmailVO(Long id, EmailVO email, PersonaVO persona) {
        this.id = id;
        this.email = email;
        this.persona = persona;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EmailVO getEmail() {
        return email;
    }

    public void setEmail(EmailVO email) {
        this.email = email;
    }

    public PersonaVO getPersona() {
        return persona;
    }

    public void setPersona(PersonaVO persona) {
        this.persona = persona;
    }
    
    
}
