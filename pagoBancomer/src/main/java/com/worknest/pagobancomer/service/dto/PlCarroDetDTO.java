package com.worknest.pagobancomer.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the PlCarroDet entity.
 */
public class PlCarroDetDTO implements Serializable {

	private Long id;

	@NotNull
	private LocalDate fechaVigencia;

	@NotNull
	private BigDecimal importe;

	@NotNull
	@Size(max = 31)
	private String llave;

	@Size(max = 300)
	private String concepto;

	@NotNull
	private Boolean generoUs;

	private Long carroId;

	private Long liquidacionId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getFechaVigencia() {
		return fechaVigencia;
	}

	public void setFechaVigencia(LocalDate fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	public String getLlave() {
		return llave;
	}

	public void setLlave(String llave) {
		this.llave = llave;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public Boolean isGeneroUs() {
		return generoUs;
	}

	public void setGeneroUs(Boolean generoUs) {
		this.generoUs = generoUs;
	}

	public Long getCarroId() {
		return carroId;
	}

	public void setCarroId(Long plCarroId) {
		this.carroId = plCarroId;
	}

	public Long getLiquidacionId() {
		return liquidacionId;
	}

	public void setLiquidacionId(Long liquidacionId) {
		this.liquidacionId = liquidacionId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		PlCarroDetDTO plCarroDetDTO = (PlCarroDetDTO) o;
		if (plCarroDetDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), plCarroDetDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "PlCarroDetDTO{" + "id=" + getId() + ", fechaVigencia='" + getFechaVigencia() + "'" + ", importe='"
				+ getImporte() + "'" + ", llave='" + getLlave() + "'" + ", concepto='" + getConcepto() + "'"
				+ ", generoUs='" + isGeneroUs() + "'" + "}";
	}

	public PlCarroDetDTO(LocalDate fechaVigencia, BigDecimal importe, String llave, String concepto, Boolean generoUs,
			Long carroId, Long liquidacionId) {
		this.fechaVigencia = fechaVigencia;
		this.importe = importe;
		this.llave = llave;
		this.concepto = concepto;
		this.generoUs = generoUs;
		this.carroId = carroId;
		this.liquidacionId = liquidacionId;
	}

	public PlCarroDetDTO() {

	}
}
