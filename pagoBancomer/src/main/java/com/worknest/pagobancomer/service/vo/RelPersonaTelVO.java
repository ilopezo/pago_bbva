/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worknest.pagobancomer.service.vo;

/**
 *
 * @author WorkNest9
 */
public class RelPersonaTelVO {
    private Long id;
    
    private PerfilVO telefono;
    
    private PersonaVO persona;

    public RelPersonaTelVO() {
    }

    public RelPersonaTelVO(Long id, PerfilVO telefono, PersonaVO persona) {
        this.id = id;
        this.telefono = telefono;
        this.persona = persona;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PerfilVO getTelefono() {
        return telefono;
    }

    public void setTelefono(PerfilVO telefono) {
        this.telefono = telefono;
    }

    public PersonaVO getPersona() {
        return persona;
    }

    public void setPersona(PersonaVO persona) {
        this.persona = persona;
    }
    
}
