package com.worknest.pagobancomer.service.vo;

import java.io.Serializable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class LiquidacionAdeudoPredialVO implements Serializable{

	private static final long serialVersionUID = 1L;

	@NotNull
	@Max(value = 69999)
	@Min(value = 11900)
	private Integer bimIni;
	
	@NotNull
	@Max(value = 69999)
	@Min(value = 11900)
	private Integer bimFin;
	
	@NotNull
	@Size(max = 31, min=8)
	private String cveCatastral;
	
	private Boolean esPagoEnLinea;
	
	private Boolean guardaLiquidacion;
	
	private Boolean esAgrupada;

	public LiquidacionAdeudoPredialVO(Integer bimIni, Integer bimFin, String cveCatastral, Boolean esAgrupada, Boolean esPagoEnLinea, Boolean guardaLiquidacion){
		this.bimIni = bimIni;
		this.bimFin = bimFin;
		this.cveCatastral = cveCatastral;
		this.esAgrupada = esAgrupada;
		this.esPagoEnLinea = esPagoEnLinea;
		this.guardaLiquidacion = guardaLiquidacion;
	}

	public Integer getBimIni() {
		return bimIni;
	}

	public void setBimIni(Integer bimIni) {
		this.bimIni = bimIni;
	}

	public Integer getBimFin() {
		return bimFin;
	}

	public void setBimFin(Integer bimFin) {
		this.bimFin = bimFin;
	}

	public String getCveCatastral() {
		return cveCatastral;
	}

	public void setCveCatastral(String cveCatastral) {
		this.cveCatastral = cveCatastral;
	}

	public Boolean getEsPagoEnLinea() {
		return esPagoEnLinea== null?false:esPagoEnLinea ;
	}

	public void setEsPagoEnLinea(Boolean esPagoEnLinea) {
		this.esPagoEnLinea = esPagoEnLinea;
	}

	public Boolean getGuardaLiquidacion() {
		return guardaLiquidacion;
	}

	public void setGuardaLiquidacion(Boolean guardaLiquidacion) {
		this.guardaLiquidacion = guardaLiquidacion;
	}

	public Boolean getEsAgrupada() {
		return esAgrupada;
	}

	public void setEsAgrupada(Boolean esAgrupada) {
		this.esAgrupada = esAgrupada;
	}

	@Override
	public String toString() {
		return "LiquidacionAdeudoPredialVO [bimIni=" + bimIni + ", bimFin=" + bimFin + ", cveCatastral=" + cveCatastral
				+ ", esPagoEnLinea=" + esPagoEnLinea + ", guardaLiquidacion=" + guardaLiquidacion + ", esAgrupada="
				+ esAgrupada + "]";
	}
	
}