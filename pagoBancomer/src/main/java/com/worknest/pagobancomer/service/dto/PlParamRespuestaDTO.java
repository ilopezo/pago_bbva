package com.worknest.pagobancomer.service.dto;


import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * A DTO for the PlParamRespuesta entity.
 */
public class PlParamRespuestaDTO implements Serializable {

	private Long id;

    @NotNull
    @Size(max = 100)
    private String valor;

    private Long paramBancoId;

    private Long respuestaBancoId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Long getParamBancoId() {
        return paramBancoId;
    }

    public void setParamBancoId(Long plParamBancoId) {
        this.paramBancoId = plParamBancoId;
    }

    public Long getRespuestaBancoId() {
        return respuestaBancoId;
    }

    public void setRespuestaBancoId(Long plRespuestaBancoId) {
        this.respuestaBancoId = plRespuestaBancoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PlParamRespuestaDTO plParamRespuestaDTO = (PlParamRespuestaDTO) o;
        if(plParamRespuestaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plParamRespuestaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlParamRespuestaDTO{" +
            "id=" + getId() +
            ", valor='" + getValor() + "'" +
            "}";
    }
}
