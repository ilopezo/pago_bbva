package com.worknest.pagobancomer.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.worknest.pagobancomer.domain.PlParamEnvio;
import com.worknest.pagobancomer.repository.PlParamEnvioRepository;
import com.worknest.pagobancomer.service.PlParamEnvioService;
import com.worknest.pagobancomer.service.dto.PlParamEnvioDTO;
import com.worknest.pagobancomer.service.mapper.PlParamEnvioMapper;

/**
 * Service Implementation for managing PlParamEnvio.
 */
@Service
@Transactional
public class PlParamEnvioServiceImpl implements PlParamEnvioService{

    private final Logger log = LoggerFactory.getLogger(PlParamEnvioServiceImpl.class);

    private final PlParamEnvioRepository plParamEnvioRepository;

    private final PlParamEnvioMapper plParamEnvioMapper;

    public PlParamEnvioServiceImpl(PlParamEnvioRepository plParamEnvioRepository, PlParamEnvioMapper plParamEnvioMapper) {
        this.plParamEnvioRepository = plParamEnvioRepository;
        this.plParamEnvioMapper = plParamEnvioMapper;
    }

    /**
     * Save a plParamEnvio.
     *
     * @param plParamEnvioDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PlParamEnvioDTO save(PlParamEnvioDTO plParamEnvioDTO) {
        log.debug("Request to save PlParamEnvio : {}", plParamEnvioDTO);
        PlParamEnvio plParamEnvio = plParamEnvioMapper.toEntity(plParamEnvioDTO);
        plParamEnvio = plParamEnvioRepository.save(plParamEnvio);
        return plParamEnvioMapper.toDto(plParamEnvio);
    }

    /**
     *  Get all the plParamEnvios.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PlParamEnvioDTO> findAll() {
        log.debug("Request to get all PlParamEnvios");
        return plParamEnvioRepository.findAll().stream()
            .map(plParamEnvioMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one plParamEnvio by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PlParamEnvioDTO findOne(Long id) {
        log.debug("Request to get PlParamEnvio : {}", id);
        PlParamEnvio plParamEnvio = plParamEnvioRepository.findOne(id);
        return plParamEnvioMapper.toDto(plParamEnvio);
    }

    /**
     *  Delete the  plParamEnvio by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PlParamEnvio : {}", id);
        plParamEnvioRepository.delete(id);
    }
}
