/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worknest.pagobancomer.service.vo;

import com.worknest.pagobancomer.domain.enumeration.TipoEmail;

/**
 *
 * @author WorkNest9
 */
public class EmailVO {
    private Long id;
    
    private String correoe;
    
    private TipoEmail tipo;

    public EmailVO() {
    }

    public EmailVO(Long id, String correoe, TipoEmail tipo) {
        this.id = id;
        this.correoe = correoe;
        this.tipo = tipo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCorreoe() {
        return correoe;
    }

    public void setCorreoe(String correoe) {
        this.correoe = correoe;
    }

    public TipoEmail getTipo() {
        return tipo;
    }

    public void setTipo(TipoEmail tipo) {
        this.tipo = tipo;
    }

}
