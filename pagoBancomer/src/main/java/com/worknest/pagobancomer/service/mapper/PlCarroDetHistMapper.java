package com.worknest.pagobancomer.service.mapper;

import com.worknest.pagobancomer.domain.*;
import com.worknest.pagobancomer.service.dto.PlCarroDetHistDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity PlCarroDetHist and its DTO PlCarroDetHistDTO.
 */
@Mapper(componentModel = "spring", uses = {PlCarroHistMapper.class, LiquidacionMapper.class})
public interface PlCarroDetHistMapper extends EntityMapper<PlCarroDetHistDTO, PlCarroDetHist> {

    @Mapping(source = "carro.id", target = "carroId")
    @Mapping(source = "liquidacion.id", target = "liquidacionId")
    PlCarroDetHistDTO toDto(PlCarroDetHist plCarroDetHist); 

    @Mapping(source = "carroId", target = "carro")
    @Mapping(source = "liquidacionId", target = "liquidacion")
    PlCarroDetHist toEntity(PlCarroDetHistDTO plCarroDetHistDTO);

    default PlCarroDetHist fromId(Long id) {
        if (id == null) {
            return null;
        }
        PlCarroDetHist plCarroDetHist = new PlCarroDetHist();
        plCarroDetHist.setId(id);
        return plCarroDetHist;
    }
}
