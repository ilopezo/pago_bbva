package com.worknest.pagobancomer.service;

import java.util.List;

import com.worknest.pagobancomer.service.dto.AgregarConceptoDTO;
import com.worknest.pagobancomer.service.dto.PlCarroDTO;
import com.worknest.pagobancomer.service.dto.PlCarroDetDTO;
import com.worknest.pagobancomer.web.rest.errors.ExceptionAPI;

/**
 * Service Interface for managing PlCarroDet.
 */
public interface PlCarroDetService {

    /**
     * Save a plCarroDet.
     *
     * @param plCarroDetDTO the entity to save
     * @return the persisted entity
     */
    PlCarroDetDTO save(PlCarroDetDTO plCarroDetDTO);

    /**
     *  Get all the plCarroDets.
     *
     *  @return the list of entities
     */
    List<PlCarroDetDTO> findAll();

    /**
     *  Get the "id" plCarroDet.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PlCarroDetDTO findOne(Long id);

    /**
     *  Delete the "id" plCarroDet.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
    
    //guarda en el carro los detalles del un nuevo concepto
    void guardarCarroDet(AgregarConceptoDTO nuevoConcepto, Long idCarro) throws ExceptionAPI, Exception;
    
    //obtiene los conceptos de un carro
    List<PlCarroDetDTO> buscarPorCarro(Long idCarro) throws ExceptionAPI, Exception;
    
    //borra concepto por llave
    void borrarConcepto(String llave, Long idCarro, boolean generoUs, List<Long> envio) throws ExceptionAPI, Exception;
    
    // borrar detalle
    boolean borraDetalle(String llave, PlCarroDTO carro) throws ExceptionAPI, Exception;

    // borrar detalle hist
    void borraDetalleHist(String llave) throws ExceptionAPI, Exception;

    // obtiene los detalles 
    List<PlCarroDetDTO> getDetallesByLlaveCarro(String llave, Long id) throws ExceptionAPI, Exception;
    
    void deleteLiquidacion(Long idLiquidacion)throws ExceptionAPI;
}
