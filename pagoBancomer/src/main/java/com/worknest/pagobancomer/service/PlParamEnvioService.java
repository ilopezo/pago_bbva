package com.worknest.pagobancomer.service;

import java.util.List;

import com.worknest.pagobancomer.service.dto.PlParamEnvioDTO;

/**
 * Service Interface for managing PlParamEnvio.
 */
public interface PlParamEnvioService {

    /**
     * Save a plParamEnvio.
     *
     * @param plParamEnvioDTO the entity to save
     * @return the persisted entity
     */
    PlParamEnvioDTO save(PlParamEnvioDTO plParamEnvioDTO);

    /**
     *  Get all the plParamEnvios.
     *
     *  @return the list of entities
     */
    List<PlParamEnvioDTO> findAll();

    /**
     *  Get the "id" plParamEnvio.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PlParamEnvioDTO findOne(Long id);

    /**
     *  Delete the "id" plParamEnvio.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
