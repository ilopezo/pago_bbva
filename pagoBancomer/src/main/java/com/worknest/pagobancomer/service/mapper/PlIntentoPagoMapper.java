package com.worknest.pagobancomer.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.worknest.pagobancomer.domain.PlIntentoPago;
import com.worknest.pagobancomer.service.dto.PlIntentoPagoDTO;

/**
 * Mapper for the entity PlIntentoPago and its DTO PlIntentoPagoDTO.
 */
@Mapper(componentModel = "spring", uses = {PlCarroHistMapper.class})
public interface PlIntentoPagoMapper extends EntityMapper<PlIntentoPagoDTO, PlIntentoPago> {

    @Mapping(source = "historialcarro.id", target = "historialcarroId")
    PlIntentoPagoDTO toDto(PlIntentoPago plIntentoPago); 

    @Mapping(source = "historialcarroId", target = "historialcarro")
    PlIntentoPago toEntity(PlIntentoPagoDTO plIntentoPagoDTO);

    default PlIntentoPago fromId(Long id) {
        if (id == null) {
            return null;
        }
        PlIntentoPago plIntentoPago = new PlIntentoPago();
        plIntentoPago.setId(id);
        return plIntentoPago;
    }
}
