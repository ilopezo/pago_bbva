package com.worknest.pagobancomer.service.dto;


import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the PlRespuestaBanco entity.
 */
public class PlRespuestaBancoDTO implements Serializable {

    private Long id;

    @NotNull
    private Boolean exitoso;

    @NotNull
    private ZonedDateTime fecha;

    private Long intentoPagoId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isExitoso() {
        return exitoso;
    }

    public void setExitoso(Boolean exitoso) {
        this.exitoso = exitoso;
    }

    public ZonedDateTime getFecha() {
        return fecha;
    }

    public void setFecha(ZonedDateTime fecha) {
        this.fecha = fecha;
    }

    public Long getIntentoPagoId() {
        return intentoPagoId;
    }

    public void setIntentoPagoId(Long plIntentoPagoId) {
        this.intentoPagoId = plIntentoPagoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PlRespuestaBancoDTO plRespuestaBancoDTO = (PlRespuestaBancoDTO) o;
        if(plRespuestaBancoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plRespuestaBancoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlRespuestaBancoDTO{" +
            "id=" + getId() +
            ", exitoso='" + isExitoso() + "'" +
            ", fecha='" + getFecha() + "'" +
            "}";
    }
}
