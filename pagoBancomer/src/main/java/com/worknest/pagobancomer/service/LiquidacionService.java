package com.worknest.pagobancomer.service;

import java.util.List;

import com.worknest.pagobancomer.service.dto.LiquidacionDTO;

/**
 * Service Interface for managing Liquidacion.
 */
public interface LiquidacionService {

    /**
     * Save a liquidacion.
     *
     * @param liquidacionDTO the entity to save
     * @return the persisted entity
     */
    LiquidacionDTO save(LiquidacionDTO liquidacionDTO);

    /**
     *  Get all the liquidacions.
     *
     *  @return the list of entities
     */
    List<LiquidacionDTO> findAll();

    /**
     *  Get the "id" liquidacion.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    LiquidacionDTO findOne(Long id);

    /**
     *  Delete the "id" liquidacion.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
    
    /**
     * buscar por "numeroLiquidacion" la liquidacion.
     *
     * @param numeroLiquidacion el numero de liquidacion
     * @return the entity
     */
    LiquidacionDTO findOneByNumLiq(Long numeroLiquidacion);
}
