package com.worknest.pagobancomer.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.worknest.pagobancomer.domain.Liquidacion;
import com.worknest.pagobancomer.service.dto.LiquidacionDTO;

/**
 * Mapper for the entity Liquidacion and its DTO LiquidacionDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LiquidacionMapper extends EntityMapper<LiquidacionDTO, Liquidacion> {

    

    @Mapping(target = "liquidacionPredial", ignore = true)
    @Mapping(target = "liquidacionDesConcepto", ignore = true)
    LiquidacionDTO toDto(Liquidacion liquidacion); 


    @Mapping(target = "liquidacionPredial", ignore = true)
    @Mapping(target = "desgloceConceptos", ignore = true)
    Liquidacion toEntity(LiquidacionDTO liquidacionDTO); 
    
    

    default Liquidacion fromId(Long id) {
        if (id == null) {
            return null;
        }
        Liquidacion liquidacion = new Liquidacion();
        liquidacion.setId(id);
        return liquidacion;
    }
}
