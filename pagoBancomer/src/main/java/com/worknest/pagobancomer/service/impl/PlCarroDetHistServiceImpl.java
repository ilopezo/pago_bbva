package com.worknest.pagobancomer.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.worknest.pagobancomer.domain.PlCarroDetHist;
import com.worknest.pagobancomer.domain.PlCarroHist;
import com.worknest.pagobancomer.domain.PlIntentoPago;
import com.worknest.pagobancomer.repository.PlCarroDetHistRepository;
import com.worknest.pagobancomer.repository.PlCarroHistRepository;
import com.worknest.pagobancomer.repository.PlIntentoPagoRepository;
import com.worknest.pagobancomer.service.PlCarroDetHistService;
import com.worknest.pagobancomer.service.dto.PlCarroDetDTO;
import com.worknest.pagobancomer.service.dto.PlCarroDetHistDTO;
import com.worknest.pagobancomer.service.dto.PlCarroHistDTO;
import com.worknest.pagobancomer.service.mapper.PlCarroDetHistMapper;
import com.worknest.pagobancomer.service.mapper.PlCarroHistMapper;
import com.worknest.pagobancomer.web.rest.errors.ExceptionAPI;
import java.time.ZonedDateTime;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Service Implementation for managing PlCarroDetHist.
 */
@Service
@Transactional
public class PlCarroDetHistServiceImpl implements PlCarroDetHistService{

    private final Logger log = LoggerFactory.getLogger(PlCarroDetHistServiceImpl.class);

    private final PlCarroDetHistRepository plCarroDetHistRepository;

    private final PlCarroDetHistMapper plCarroDetHistMapper;
    
    private final PlCarroHistRepository plCarroHistRepository;

    private final PlCarroHistMapper plCarroHistMapper;
    
    @Autowired
    private PlIntentoPagoRepository plIntengoPagoRepository;

    public PlCarroDetHistServiceImpl(PlCarroDetHistRepository plCarroDetHistRepository, PlCarroDetHistMapper plCarroDetHistMapper,PlCarroHistRepository plCarroHistRepository, PlCarroHistMapper plCarroHistMapper) {
        this.plCarroDetHistRepository = plCarroDetHistRepository;
        this.plCarroDetHistMapper = plCarroDetHistMapper;
        this.plCarroHistRepository = plCarroHistRepository;
        this.plCarroHistMapper = plCarroHistMapper;
    }

    /**
     * Save a plCarroDetHist.
     *
     * @param plCarroDetHistDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PlCarroDetHistDTO save(PlCarroDetHistDTO plCarroDetHistDTO) {
        log.debug("Request to save PlCarroDetHist : {}", plCarroDetHistDTO);
        PlCarroDetHist plCarroDetHist = plCarroDetHistMapper.toEntity(plCarroDetHistDTO);
        plCarroDetHist = plCarroDetHistRepository.save(plCarroDetHist);
        return plCarroDetHistMapper.toDto(plCarroDetHist);
    }

    /**
     *  Get all the plCarroDetHists.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PlCarroDetHistDTO> findAll() {
        log.debug("Request to get all PlCarroDetHists");
        return plCarroDetHistRepository.findAll().stream()
            .map(plCarroDetHistMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one plCarroDetHist by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PlCarroDetHistDTO findOne(Long id) {
        log.debug("Request to get PlCarroDetHist : {}", id);
        PlCarroDetHist plCarroDetHist = plCarroDetHistRepository.findOne(id);
        return plCarroDetHistMapper.toDto(plCarroDetHist);
    }

    /**
     *  Delete the  plCarroDetHist by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PlCarroDetHist : {}", id);
        plCarroDetHistRepository.delete(id);
    }
    /**
     * Se guardan los conceptos del carroDet en el carroDetHist
     * @param listaConceptos 
     * @param carroHist 
     * @return carroHist debido a que puede cambiar en el proceso
     * @throws ExceptionAPI
     * @throws Exception 
     */
    @Override
    public PlCarroHistDTO guardarConceptos(List<PlCarroDetDTO> listaConceptos, PlCarroHistDTO carroHist) throws ExceptionAPI, Exception {  	
        //se revisa si la lista esta vacia 
        if (listaConceptos.isEmpty()) {   
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST,"La lista de conceptos esta vacía");//se arroja una excepcion personalizada con mensaje personalizado
        }       
        Boolean verifica =true; //si todos los conceptos del carroDet y carroDetHist son iguales no se guardan
        // se recorre la lista de conceptos para guardarla en carroDetHist
        for (PlCarroDetDTO carroDet : listaConceptos) {      	
            //Se busca si el id de liquidación ya esta dentro de un concepto en el carroHist
            log.debug("id liquidacion " +carroDet.getLiquidacionId());
            PlCarroDetHist PlCarroDetHist = this.plCarroDetHistRepository.buscarDetallerCarroHist(carroHist.getId(),carroDet.getLiquidacionId());         
            // Si no se encontro el idLiquidacion se agrega al carroHist
            if(PlCarroDetHist ==null){
            	verifica=false;
                break;
            }
        }
        if (!verifica) {//si se encontro alguna diferencia en los conceptos del carroDet y carroHist
            // Se crea un nuevo historial de carro (carroHist) con la fecha actual y la referencia vacia
            PlCarroHistDTO carroHist2 = new PlCarroHistDTO(ZonedDateTime.now(), "", carroHist.getCarroId());
            PlCarroHist plCarroHist = plCarroHistMapper.toEntity(carroHist2);
            plCarroHist = plCarroHistRepository.save(plCarroHist);
            carroHist =plCarroHistMapper.toDto(plCarroHist); //cambia el carroHist que se recibio por el que se creo
            
            for (PlCarroDetDTO carroDet : listaConceptos) { 
                log.debug("id liquidacion: "+carroDet.getLiquidacionId());
                //Se guarda el carroDet en un nuevo carroDetHist
                PlCarroDetHistDTO carroDetHist = new PlCarroDetHistDTO(carroDet.getConcepto(),      //se envia el concepto del CarroDet
                                                                        carroDet.getLiquidacionId(),//se envia el id de liquidacion
                                                                        carroDet.getFechaVigencia(),//se envia la fecha de vigencia
                                                                        carroDet.getImporte(),      //se envia el importe a pagar
                                                                        carroDet.getLlave(),        //se envia la llave (clave catastral o numero de liquidacion)
                                                                        carroDet.isGeneroUs(),      //se envia si lo genero el usuario o no
                                                                        carroHist.getId(),          //se envia el id del carroHist
                                                                        carroDet.getLiquidacionId()); 

                PlCarroDetHist plCarroDetHist = this.plCarroDetHistMapper.toEntity(carroDetHist);
                // se guarda el carroHist en la base de datos
                this.plCarroDetHistRepository.save(plCarroDetHist); 
            }            
        }        
        return carroHist;        
    }
    
    /**
     * Metodo para obtener los detalles del historial de un carro
     * @param plCarroHist
     * @return 
     */
    @Override
    public List<PlCarroDetHistDTO> buscarPorIdCarroHist(PlCarroHistDTO plCarroHist) {
        return plCarroDetHistRepository.findByCarro(plCarroHist).stream()
            .map(plCarroDetHistMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }
    
    /**
     * Metodo para obtener los detalles de un historial de pagos realizados, a travez del id del intento pago
     * @param idIntentoPago
     * @return
     * @throws ExceptionAPI 
     */
    @Override
    public List<PlCarroDetHistDTO> obtenerDetallesHist(Long idIntentoPago) throws ExceptionAPI {
        //Buscamos el intento del pago
        PlIntentoPago intentoPago= plIntengoPagoRepository.findOne(idIntentoPago);
        if (intentoPago==null) {
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST,"No se encuentra el intento del pago");
        }
        //buscamos los detalles del historial del carro
        List<PlCarroDetHist> carroHistDet = plCarroDetHistRepository.getAllByIdCarroHist(intentoPago.getHistorialcarro().getId());
        if (carroHistDet.isEmpty()) {
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST,"No se encuentra el historial del pago");   
        }
        return carroHistDet.stream()
            .map(plCarroDetHistMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }
}
