package com.worknest.pagobancomer.service.dto;

import java.io.Serializable;
import java.util.List;

public class PagoLineaDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private List<Long> idLiqs;
	private String Auth;

	public List<Long> getIdLiqs() {
		return idLiqs;
	}

	public void setIdLiqs(List<Long> idLiqs) {
		this.idLiqs = idLiqs;
	}

	public String getAuth() {
		return Auth;
	}

	public void setAuth(String auth) {
		Auth = auth;
	}
}
