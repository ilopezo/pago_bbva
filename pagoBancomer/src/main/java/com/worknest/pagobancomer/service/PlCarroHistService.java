package com.worknest.pagobancomer.service;

import java.util.List;

import com.worknest.pagobancomer.service.dto.PlCarroHistDTO;
import com.worknest.pagobancomer.domain.PlCarro;
import com.worknest.pagobancomer.web.rest.errors.ExceptionAPI;

/**
 * Service Interface for managing PlCarroHist.
 */
public interface PlCarroHistService {

    /**
     * Save a plCarroHist.
     *
     * @param plCarroHistDTO the entity to save
     * @return the persisted entity
     */
    PlCarroHistDTO save(PlCarroHistDTO plCarroHistDTO);

    /**
     *  Get all the plCarroHists.
     *
     *  @return the list of entities
     */
    List<PlCarroHistDTO> findAll();

    /**
     *  Get the "id" plCarroHist.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PlCarroHistDTO findOne(Long id);

    /**
     *  Delete the "id" plCarroHist.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
    
    //crea un nuevo CarroHist en base al carrito del usuario
    PlCarroHistDTO crearCarroHist(Long idCarro) throws ExceptionAPI, Exception;
}
