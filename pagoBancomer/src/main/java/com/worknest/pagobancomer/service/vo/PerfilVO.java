/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worknest.pagobancomer.service.vo;

import java.util.List;
import javax.validation.constraints.NotNull;

/**
 *
 * @author WorkNest9
 */
public class PerfilVO {
    
    @NotNull
    private PersonaVO persona;
    
    @NotNull
    private List<EmailVO> email;
    
    private List<TelefonoVO> telefonos;
    
    @NotNull
    private List<DireccionVO> direcciones;
    
    private Object usuario;
    
    private List<DatosFacturacionVO> datosFacturacion;
    
    //Set and get

    public PersonaVO getPersona() {
        return persona;
    }

    public void setPersona(PersonaVO persona) {
        this.persona = persona;
    }

    public List<EmailVO> getEmail() {
        return email;
    }

    public void setEmail(List<EmailVO> email) {
        this.email = email;
    }

    public List<TelefonoVO> getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(List<TelefonoVO> telefonos) {
        this.telefonos = telefonos;
    }

    public List<DireccionVO> getDirecciones() {
        return direcciones;
    }

    public void setDirecciones(List<DireccionVO> direcciones) {
        this.direcciones = direcciones;
    }

    public Object getUsuario() {
        return usuario;
    }

    public void setUsuario(Object usuario) {
        this.usuario = usuario;
    }

    public List<DatosFacturacionVO> getDatosFacturacion() {
        return datosFacturacion;
    }

    public void setDatosFacturacion(List<DatosFacturacionVO>  datosFacturacion) {
        this.datosFacturacion = datosFacturacion;
    }

    public PerfilVO() {
    }

    public PerfilVO(PersonaVO persona, List<EmailVO> email, List<TelefonoVO> telefonos, List<DireccionVO> direcciones, Object usuario, List<DatosFacturacionVO> datosFacturacion) {
        this.persona = persona;
        this.email = email;
        this.telefonos = telefonos;
        this.direcciones = direcciones;
        this.usuario = usuario;
        this.datosFacturacion = datosFacturacion;
    }
    
    @Override
    public String toString() {
        return "info{" +
            "persona=" + getPersona() +
            "}";
    }
}
