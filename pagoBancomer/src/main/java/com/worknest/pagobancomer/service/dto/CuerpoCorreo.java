package com.worknest.pagobancomer.service.dto;

import java.math.BigDecimal;
import java.util.Date;

public class CuerpoCorreo {
    private Date fechaPago;
    private String nombreUsuario;
    private String noTransaccion;
    private Integer noPago;
    private String conceptoCobro;
    private BigDecimal total;
    private BigDecimal folio;
    private Integer token;
    private String urlDetallePago;
    private String urlBase;
    private String avisoPrivacidad;
    private String politicasUso;
    private String textoLink;

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getNoTransaccion() {
        return noTransaccion;
    }

    public void setNoTransaccion(String noTransaccion) {
        this.noTransaccion = noTransaccion;
    }

    public Integer getNoPago() {
        return noPago;
    }

    public void setNoPago(Integer noPago) {
        this.noPago = noPago;
    }

    public String getConceptoCobro() {
        return conceptoCobro;
    }

    public void setConceptoCobro(String conceptoCobro) {
        this.conceptoCobro = conceptoCobro;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getFolio() {
        return folio;
    }

    public void setFolio(BigDecimal folio) {
        this.folio = folio;
    }

    public Integer getToken() {
        return token;
    }

    public void setToken(Integer token) {
        this.token = token;
    }

    public String getUrlDetallePago() {
        return urlDetallePago;
    }

    public void setUrlDetallePago(String urlDetallePago) {
        this.urlDetallePago = urlDetallePago;
    }

    public String getUrlBase() {
        return urlBase;
    }

    public void setUrlBase(String urlBase) {
        this.urlBase = urlBase;
    }

    public String getAvisoPrivacidad() {
        return avisoPrivacidad;
    }

    public void setAvisoPrivacidad(String avisoPrivacidad) {
        this.avisoPrivacidad = avisoPrivacidad;
    }

    public String getPoliticasUso() {
        return politicasUso;
    }

    public void setPoliticasUso(String politicasUso) {
        this.politicasUso = politicasUso;
    }

    public String getTextoLink() {
        return textoLink;
    }

    public void setTextoLink(String textoLink) {
        this.textoLink = textoLink;
    }
}
