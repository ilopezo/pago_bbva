package com.worknest.pagobancomer.service;

import java.util.List;

import com.worknest.pagobancomer.service.dto.PlParamBancoDTO;

/**
 * Service Interface for managing PlParamBanco.
 */
public interface PlParamBancoService {

    /**
     * Save a plParamBanco.
     *
     * @param plParamBancoDTO the entity to save
     * @return the persisted entity
     */
    PlParamBancoDTO save(PlParamBancoDTO plParamBancoDTO);

    /**
     *  Get all the plParamBancos.
     *
     *  @return the list of entities
     */
    List<PlParamBancoDTO> findAll();

    /**
     *  Get the "id" plParamBanco.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PlParamBancoDTO findOne(Long id);

    /**
     *  Delete the "id" plParamBanco.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
