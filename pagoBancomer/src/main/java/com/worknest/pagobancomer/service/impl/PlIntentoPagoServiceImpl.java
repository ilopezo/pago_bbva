package com.worknest.pagobancomer.service.impl;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.worknest.pagobancomer.config.ApplicationProperties;
import com.worknest.pagobancomer.domain.PlCarro;
import com.worknest.pagobancomer.domain.PlCarroDetHist;
import com.worknest.pagobancomer.domain.PlCarroHist;
import com.worknest.pagobancomer.domain.PlIntentoPago;
import com.worknest.pagobancomer.domain.PlParamBanco;
import com.worknest.pagobancomer.domain.PlParamEnvio;
import com.worknest.pagobancomer.domain.User;
import com.worknest.pagobancomer.repository.PlCarroDetHistRepository;
import com.worknest.pagobancomer.repository.PlCarroHistRepository;
import com.worknest.pagobancomer.repository.PlCarroRepository;
import com.worknest.pagobancomer.repository.PlIntentoPagoRepository;
import com.worknest.pagobancomer.repository.PlParamBancoRepository;
import com.worknest.pagobancomer.repository.PlParamEnvioRepository;
import com.worknest.pagobancomer.service.PlIntentoPagoService;
import com.worknest.pagobancomer.service.dto.AuthenticateDTO;
import com.worknest.pagobancomer.service.dto.PlCarroHistDTO;
import com.worknest.pagobancomer.service.dto.PlIntentoPagoDTO;
import com.worknest.pagobancomer.service.dto.TokenDTO;
import com.worknest.pagobancomer.service.mapper.PlCarroHistMapper;
import com.worknest.pagobancomer.service.mapper.PlIntentoPagoMapper;
import com.worknest.pagobancomer.web.rest.errors.ExceptionAPI;
import com.worknest.pagobancomer.web.rest.util.UtilCadenas;



/**
 * Service Implementation for managing PlIntentoPago.
 */
@Service
@Transactional
public class PlIntentoPagoServiceImpl implements PlIntentoPagoService{

    private final Logger log = LoggerFactory.getLogger(PlIntentoPagoServiceImpl.class);

    private final PlIntentoPagoRepository plIntentoPagoRepository;

    private final PlIntentoPagoMapper plIntentoPagoMapper;
    
    private final PlParamEnvioRepository plParamEnvioRepository;
    
    private final PlParamBancoRepository plParamBancoRepository;
    
    private final PlCarroRepository plCarroRepository;
    
    private final ApplicationProperties applicationProperties;
    
    private final PlCarroDetHistRepository plCarroDetHistRepository;
    
    private String _token;
    
    @Value("${application.autenticate}") private String _autenticate;
    @Value("${application.usuario}") private String _usuario;
    @Value("${application.pass}") private String _pass;
    @Value("${application.urlusuario}") private String _urlusuario;
    @Value("${application.account}") private String _account;
    
    @Autowired
    private PlCarroHistMapper plCarroHistMapper;
    
    @Autowired
    private PlCarroHistRepository plCarroHistRepository;

	public PlIntentoPagoServiceImpl(PlIntentoPagoRepository plIntentoPagoRepository,
			PlIntentoPagoMapper plIntentoPagoMapper, PlParamBancoRepository plParamBancoRepository,
			ApplicationProperties applicationProperties, PlParamEnvioRepository plParamEnvioRepository,
			PlCarroRepository plCarroRepository, PlCarroDetHistRepository plCarroDetHistRepository) {
		this.plIntentoPagoRepository = plIntentoPagoRepository;
		this.plIntentoPagoMapper = plIntentoPagoMapper;
		this.plParamBancoRepository = plParamBancoRepository;
		this.applicationProperties = applicationProperties;
		this.plParamEnvioRepository = plParamEnvioRepository;
		this.plCarroRepository = plCarroRepository;
		this.plCarroDetHistRepository = plCarroDetHistRepository;

	}

    /**
     * Save a plIntentoPago.
     *
     * @param plIntentoPagoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PlIntentoPagoDTO save(PlIntentoPagoDTO plIntentoPagoDTO) {
        log.debug("Request to save PlIntentoPago : {}", plIntentoPagoDTO);
        PlIntentoPago plIntentoPago = plIntentoPagoMapper.toEntity(plIntentoPagoDTO);
        plIntentoPago = plIntentoPagoRepository.save(plIntentoPago);
        return plIntentoPagoMapper.toDto(plIntentoPago);
    }

    /**
     *  Get all the plIntentoPagos.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PlIntentoPagoDTO> findAll() {
        log.debug("Request to get all PlIntentoPagos");
        return plIntentoPagoRepository.findAll().stream()
            .map(plIntentoPagoMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one plIntentoPago by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PlIntentoPagoDTO findOne(Long id) {
        log.debug("Request to get PlIntentoPago : {}", id);
        PlIntentoPago plIntentoPago = plIntentoPagoRepository.findOne(id);
        return plIntentoPagoMapper.toDto(plIntentoPago);
    }

    /**
     *  Delete the  plIntentoPago by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PlIntentoPago : {}", id);
        plIntentoPagoRepository.delete(id);
    }
    
    /**
     * Calcula los parametros de envio para el banco BBVA
     * @param intentoPago
     * @param carroHist
     * @param referencia
     * @return Map con los parametros que se envian al banco
     * @throws InvalidKeyException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException 
     */
    private Map<String, String> calculaParametrosMultopagosBBVA( PlIntentoPago intentoPago, PlCarroHist carroHist, String referencia) 
                                         throws InvalidKeyException, UnsupportedEncodingException, NoSuchAlgorithmException{
    	
    	Map<String, String> paramEnvio = new HashMap<>();
    	
    	DecimalFormat df = new DecimalFormat("#.00");
    	List<PlParamBanco> parametrosBanco = plParamBancoRepository.getParamsActivosByClaveBanco("00001");    	
            BigDecimal total =  carroHist.getCarro().getDetalles().stream()
                                                 .map(d -> d.getImporte())
                                                 .reduce(BigDecimal.ZERO, BigDecimal::add);
        PlCarro carro = plCarroRepository.findOne(carroHist.getCarro().getId());
        User usuario = obtenerInfoUsuario(carro.getUser().toString());
    	for(PlParamBanco param : parametrosBanco){	
            switch (param.getNombre()) {
                case "mp_order":
                    paramEnvio.put("mp_order", String.valueOf( carroHist.getCarro().getId()) );
                    break;
                case "mp_reference":
                    paramEnvio.put("mp_reference", referencia );
                    break;			
                case "mp_concept":
                    paramEnvio.put("mp_concept", "1" );
                    break;
                case "mp_amount":
                    paramEnvio.put("mp_amount", df.format( total) );
                    break; 
                case "mp_customername":       
                    String  nombre = usuario.getFirstName()+" "+usuario.getLastName();

                    paramEnvio.put("mp_customername", nombre );
                    break; 	
                case "mp_signature":
                    String cadenaInformacion = String.valueOf( carroHist.getCarro().getId()) + referencia + df.format( total);
                    String token = obtenerCadenaHASH256(cadenaInformacion, applicationProperties.getClave());

                    paramEnvio.put("mp_signature", token );
                    break;
                case "mp_promo":
                    if (param.getValorDefault()==null) {
                        String[] paramD=param.getValorDefault().split("&");
                        String  value="";

                        for(String str : paramD){
                            String[] entry= str.split(";");

                            if(total.compareTo(new BigDecimal(entry[0])) >=0){
                                value = entry[1];
                            }
                        }
                        if(!"".equals(value))
                            paramEnvio.put("mp_promo", value );
                    }
                    break;
                case "mp_promo_msi":
                    if (param.getValorDefault()==null) {
                        String[] paramD2=param.getValorDefault().split("&");
                        String  value2="";

                        for(String str : paramD2){
                             String[] entry= str.split(";");

                            if(total.compareTo(new BigDecimal(entry[0])) >=0){
                                 value2 = entry[1];
                            }
                        }
                        if(!"".equals(value2))
                            paramEnvio.put("mp_promo_msi", value2 ); 
                    }                    
                    break;
                case "mp_email":
                    String email=usuario.getEmail();
                    paramEnvio.put("mp_email", email );
                    break;

                default:
                    paramEnvio.put(param.getNombre(), param.getValorDefault());
                    break;
                }
    	}  	
    	// guardar los parametros de envio en la base de datos
    	for(PlParamBanco param : parametrosBanco){ 		
            String value = paramEnvio.get(param.getNombre());
            if(!StringUtils.isEmpty(value) || !StringUtils.isBlank(value)) {
                PlParamEnvio parametrosEnvio = new PlParamEnvio();
                parametrosEnvio.setIntentoPago(new PlIntentoPago());
                parametrosEnvio.getIntentoPago().setId(intentoPago.getId());
                parametrosEnvio.setParamBanco(param);
                parametrosEnvio.setValor(value);
                this.plParamEnvioRepository.save(parametrosEnvio);
            }
    	}   	
    	return paramEnvio;
    }

    /**
     * Obiene la referencia bancaria en base al carro historial
     * @param carroHist
     * @return String de la referencia
     * @throws Exception 
     */
    private String obtenerReferenciaBanco(PlCarroHist carroHist) throws Exception {
        try {
            List<String> llaves = new ArrayList<>();
            llaves.addAll( 
            		carroHist.getDetalles().stream().map( a -> a.getLlave()).collect(Collectors.toList())
            		);
            
            llaves.addAll( 
            		carroHist.getDetalles().stream().map( a -> a.getConcepto()).collect(Collectors.toList())
            		);                      
            return obtenerCRC32(llaves.stream().sorted().collect( Collectors.joining( "" )) );    
        } catch (Exception e) {
            throw new  Exception("Error al generar la referencia de pago: "+carroHist.getId() + "  -  " + e.getMessage());
        }
    }
    
    
    public static String obtenerCRC32(String cadena) {
        byte bytes[] = cadena.getBytes();
        java.util.zip.CRC32 x = new java.util.zip.CRC32();
        x.update(bytes);
        String ans = Long.toHexString(x.getValue());
        return ans.toUpperCase();
    }
    /**
     * Obtiene la cadena HASH de un mensaje
     * @param mensaje
     * @param llavePrivada
     * @return
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException 
     */
    public static String obtenerCadenaHASH256(String mensaje, String llavePrivada) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {      
        String sEncodedString;
        SecretKeySpec key = new SecretKeySpec((llavePrivada).getBytes("UTF-8"), "HmacSHA256");
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(key);
        byte[] bytes = mac.doFinal(mensaje.getBytes("ASCII"));
        StringBuffer hash = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                hash.append('0');
            }
            hash.append(hex);
        }
        sEncodedString = hash.toString();         
        return sEncodedString;
    }

    /**
     * Crea un nuevo intento de pago ademas de generar la referencia y los parametros de envio
     * @param carroHist
     * @return Map con parametros de envio para el banco
     * @throws ExceptionAPI
     * @throws Exception 
     */
    @Override
    public Map<String, String> crearIntentoPago(PlCarroHistDTO carroHist) throws ExceptionAPI, Exception {	
        //se lanza una excepcion si el carroHist viene vacio
        if (carroHist==null) {
        	//se arroja una excepcion personalizada con mensaje personalizado
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST,"No se encuentran conceptos en el historial del carro");
        }
        
        // se crea un nuevo intento de pago
        PlIntentoPagoDTO intentoPago = new PlIntentoPagoDTO(ZonedDateTime.now(),          //se agrega la fecha en que se genero el intento
                                                            false,                        //se agrega si ya fue enviado al banco          
                                                            null,  						  //se agrega el status
                                                            // StatusIntentoPago.DISPERSA,//se agrega el status
                                                            "",                           //se agrega la autorización
                                                            carroHist.getId());           //se envia el carroHist
        
        PlIntentoPago plIntentoPago = plIntentoPagoMapper.toEntity(intentoPago);       
        // se guarda el intento pago en la base de datos
        plIntentoPagoRepository.save(plIntentoPago);    
        log.debug("se creo el intento pago : " + plIntentoPago.getId());

        PlCarroHist plCarroHist = plCarroHistMapper.toEntity(carroHist);   
        List<PlCarroDetHist> plCarroDetHist = this.plCarroDetHistRepository.getAllByIdCarroHist(plCarroHist.getId());
        
        plCarroHist.setDetalles(new HashSet<PlCarroDetHist>(plCarroDetHist));
        // se obtiene la referencia del banco
        String referencia = this.obtenerReferenciaBanco(plCarroHist);  
        log.debug("se creo la referencia : " + referencia);
        // se guarda la referencia dentro del carroHist
        plCarroHist.setReferencia(referencia);        
        // se guarda los cambios que se realizaron en el carroHist
        plCarroHistRepository.save(plCarroHist);
        log.debug("se guardo la referencia: "  );
        
        PlCarro carro = this.plCarroRepository.getOne(carroHist.getCarroId());
        
        plCarroHist.setCarro(carro);
        
        //Se otienen los parametros de envio para el banco
        Map<String, String> paramEnvio = this.calculaParametrosMultopagosBBVA(plIntentoPago, plCarroHist, referencia);      
        return paramEnvio;
    }

    @Override
    public PlIntentoPago findById(Long id) {
        PlIntentoPago plIntentoPago  = plIntentoPagoRepository.findOne(id);
        return plIntentoPago;
    }

    /**
     * Metodo para validar los datos que contiene la respuesta del banco
     * @param param
     * @param saltarValidacionFirma
     * @return
     * @throws com.worknest.pagobancomer.web.rest.errors.ExceptionAPI
     * @throws InvalidKeyException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException 
     */
    @Override
    public boolean validarDatosReciboBancomer(Map<String, String> param, boolean saltarValidacionFirma) throws ExceptionAPI, InvalidKeyException, UnsupportedEncodingException, NoSuchAlgorithmException {
        boolean firmaCorrecta = true;
        // validaciones        
        if (param.get("mp_order").equals("") || param.get("mp_order") == null) {
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST,"El campo mp_order no debe estar vacío.");
        }
        if (param.get("mp_concept").equals("") || param.get("mp_concept") == null) {
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST,"El campo mp_concept no debe estar vacío.");
        }
        if (param.get("mp_amount").equals("") || param.get("mp_amount") == null) {
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST,"El campo mp_amount no debe estar vacío.");
        }
        if (param.get("mp_email").equals("") || param.get("mp_email") == null) {
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST,"El campo mp_email no debe estar vacío.");
        }
        if (param.get("mp_phone").equals("") || param.get("mp_phone") == null) {
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST,"El campo mp_phone no debe estar vacío.");
        }
        if (param.get("mp_saleid").equals("") || param.get("mp_saleid") == null) {
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST,"El número de campo mp_saleid no debe estar vacío.");
        }
        if(!UtilCadenas.convertirCadenaAInteger(param.get("mp_order"))) {
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST,"El campo mp_order no tiene un formato númerico.");
        }
        if(!UtilCadenas.convertirCadenaAInteger(param.get("mp_concept"))) {
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST,"El campo mp_concept no tiene un formato númerico.");
        }
        if(!UtilCadenas.convertirCadenaAFlotante(param.get("mp_amount"))) {
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST,"El campo mp_amount no tiene un formato decimal.");
        }
        if (!saltarValidacionFirma) {

            String mpOrder = (String) param.get("mp_order");
            String mpReference = (String) param.get("mp_reference");
            String mpAmount = (String) param.get("mp_amount");
            // firma recibida
            String mpSignature = (String) param.get("mp_signature");
            // firma generada
            String hash = obtenerCadenaHASH256(mpOrder + mpReference + mpAmount, this.applicationProperties.getClave());
            if (!hash.equals(mpSignature)){
                    firmaCorrecta = false;
            }
        }
        return firmaCorrecta;

    }

    /**
     * Verifica si la autorizacion existe en los intentos de pago
     * @param auth
     * @return 
     */
    @Override
    public boolean verificaAutorizacion(String auth) {
        boolean resultado = false;
        String existe = plIntentoPagoRepository.verificarAutorizacion(auth);
        if (existe == null) {
            resultado = true;
        }
        return resultado;
    }

    public User obtenerInfoUsuario(String userId){
        
        this._token = this.generaToken(this._usuario, this._pass).getId_token();
        
        // Se consulta el WS que genera el token
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + this._token);

        //Genera la url para consumir el WS que busque la informacion del usuario
        String uri = new String(this._urlusuario + this._account);
        log.debug("----------------------------------"+userId);

        HttpEntity<?> entity = new HttpEntity<String>(userId, headers);
        // El WS genera las liquidaciones en base a los datos de envio
        
        ResponseEntity<User> usuario = restTemplate.exchange(uri, HttpMethod.POST, entity, User.class);
        
        User user = usuario.getBody();
        log.debug("----------------------------------"+user.toString());
        return user;
    }
    
    /**
     * Genera token para comunicacion con WS acapulco-caja
     * @param usuario usuario para generar token 
     * @param pass clave de acceso para generar token
     * @return token obtenido
     */
    public TokenDTO generaToken(String usuario, String pass) {
        // se definie las credenciales FIJAS
        AuthenticateDTO auth = new AuthenticateDTO();
        
        auth.setUsername(usuario); // user
        auth.setRememberMe(true);
        auth.setPassword(pass);    // user
        // se definie los headers
        Map<String, String> vars = new HashMap<String, String>();
        vars.put("Content-Type", "application/json");  
        // se consulta el ws que genera el token
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        String uri = new String(this._urlusuario + this._autenticate);
        TokenDTO token = new TokenDTO();
        token = restTemplate.postForObject(uri, auth, TokenDTO.class, vars);
        // regresa el token
        return token;
    }
    
}
