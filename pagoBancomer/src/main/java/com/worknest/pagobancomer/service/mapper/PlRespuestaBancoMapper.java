package com.worknest.pagobancomer.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.worknest.pagobancomer.domain.PlRespuestaBanco;
import com.worknest.pagobancomer.service.dto.PlRespuestaBancoDTO;

/**
 * Mapper for the entity PlRespuestaBanco and its DTO PlRespuestaBancoDTO.
 */
@Mapper(componentModel = "spring", uses = {PlIntentoPagoMapper.class})
public interface PlRespuestaBancoMapper extends EntityMapper<PlRespuestaBancoDTO, PlRespuestaBanco> {

    @Mapping(source = "intentoPago.id", target = "intentoPagoId")
    PlRespuestaBancoDTO toDto(PlRespuestaBanco plRespuestaBanco); 

    @Mapping(source = "intentoPagoId", target = "intentoPago")
    PlRespuestaBanco toEntity(PlRespuestaBancoDTO plRespuestaBancoDTO);

    default PlRespuestaBanco fromId(Long id) {
        if (id == null) {
            return null;
        }
        PlRespuestaBanco plRespuestaBanco = new PlRespuestaBanco();
        plRespuestaBanco.setId(id);
        return plRespuestaBanco;
    }
}
