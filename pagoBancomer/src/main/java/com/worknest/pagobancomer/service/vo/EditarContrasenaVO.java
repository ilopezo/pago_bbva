/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worknest.pagobancomer.service.vo;

/**
 *
 * @author WorkNest9
 */
public class EditarContrasenaVO {
    private String contrasenaNueva;
    
    private String contrasenaActual;
    
    private PersonaVO persona;
    
    public EditarContrasenaVO() {
    }

    public EditarContrasenaVO(String contrasenaNueva, String contrasenaActual, PersonaVO persona) {
        this.contrasenaNueva = contrasenaNueva;
        this.contrasenaActual = contrasenaActual;
        this.persona = persona;
    }

    public String getContrasenaNueva() {
        return contrasenaNueva;
    }

    public void setContrasenaNueva(String contrasenaNueva) {
        this.contrasenaNueva = contrasenaNueva;
    }

    public String getContrasenaActual() {
        return contrasenaActual;
    }

    public void setContrasenaActual(String contrasenaActual) {
        this.contrasenaActual = contrasenaActual;
    }

    public PersonaVO getPersona() {
        return persona;
    }

    public void setPersona(PersonaVO persona) {
        this.persona = persona;
    }
    
}
