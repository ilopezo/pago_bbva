/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worknest.pagobancomer.service.vo;

import com.worknest.pagobancomer.domain.enumeration.TipoPais;

/**
 *
 * @author WorkNest9
 */
public class CatalogoDirVO {
    private Long id;
    
    private String cp;
    
    private String asentamiento;
    
    private String tipoasentamiento;
    
    private String municipio;
    
    private String estado;
    
    private String ciudad;
    
    private TipoPais pais;

    public CatalogoDirVO() {
    }

    public CatalogoDirVO(Long id, String cp, String asentamiento, String tipoasentamiento, String municipio, String estado, String ciudad, TipoPais pais) {
        this.id = id;
        this.cp = cp;
        this.asentamiento = asentamiento;
        this.tipoasentamiento = tipoasentamiento;
        this.municipio = municipio;
        this.estado = estado;
        this.ciudad = ciudad;
        this.pais = pais;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getAsentamiento() {
        return asentamiento;
    }

    public void setAsentamiento(String asentamiento) {
        this.asentamiento = asentamiento;
    }

    public String getTipoasentamiento() {
        return tipoasentamiento;
    }

    public void setTipoasentamiento(String tipoasentamiento) {
        this.tipoasentamiento = tipoasentamiento;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public TipoPais getPais() {
        return pais;
    }

    public void setPais(TipoPais pais) {
        this.pais = pais;
    }
    
    
}
