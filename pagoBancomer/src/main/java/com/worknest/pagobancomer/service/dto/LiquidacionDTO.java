package com.worknest.pagobancomer.service.dto;


import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.worknest.pagobancomer.domain.enumeration.EstatusLiquidacion;
import com.worknest.pagobancomer.domain.enumeration.TipoLiquidacion;

/**
 * A DTO for the Liquidacion entity.
 */
public class LiquidacionDTO implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Long id;

    @NotNull
    private Long numeroLiquidacion;

    @NotNull
    private LocalDate fechaCreacion;

    @NotNull
    private LocalDate vigencia;

    @Size(max = 31)
    private String referencia;

    @NotNull
    private TipoLiquidacion tipoLiquidacion;

    @NotNull
    @Size(max = 511)
    private String concepto;

    @NotNull
    @DecimalMin(value = "0")
    private BigDecimal total;

    @NotNull
    @Size(max = 120)
    private String totalLetra;

    private Float redondeo;

    @NotNull
    private EstatusLiquidacion estatus;

    private Long catEmisorId;

    private String catEmisorDescripcion;
	
    private List<LiquidacionDesConceptoDTO> liquidacionDesConcepto;
 
    private List<LiquidacionDesConceptoDetDTO> desgloceConceptoDets;

    private Set<LiquidacionDesDescripcionDTO> desgloceDescripciones = new HashSet<>();

    private LiquidacionPredialDTO liquidacionPredial;

    private Long grupoId;

    private String grupoDescripcion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumeroLiquidacion() {
        return numeroLiquidacion;
    }

    public void setNumeroLiquidacion(Long numeroLiquidacion) {
        this.numeroLiquidacion = numeroLiquidacion;
    }

    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public LocalDate getVigencia() {
        return vigencia;
    }

    public void setVigencia(LocalDate vigencia) {
        this.vigencia = vigencia;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public TipoLiquidacion getTipoLiquidacion() {
        return tipoLiquidacion;
    }

    public void setTipoLiquidacion(TipoLiquidacion tipoLiquidacion) {
        this.tipoLiquidacion = tipoLiquidacion;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getTotalLetra() {
        return totalLetra;
    }

    public void setTotalLetra(String totalLetra) {
        this.totalLetra = totalLetra;
    }

    public Float getRedondeo() {
        return redondeo;
    }

    public void setRedondeo(Float redondeo) {
        this.redondeo = redondeo;
    }

    public EstatusLiquidacion getEstatus() {
        return estatus;
    }

    public void setEstatus(EstatusLiquidacion estatus) {
        this.estatus = estatus;
    }

    public Long getCatEmisorId() {
        return catEmisorId;
    }

    public void setCatEmisorId(Long liquidacionEmisorId) {
        this.catEmisorId = liquidacionEmisorId;
    }

    public String getCatEmisorDescripcion() {
        return catEmisorDescripcion;
    }

    public void setCatEmisorDescripcion(String liquidacionEmisorDescripcion) {
        this.catEmisorDescripcion = liquidacionEmisorDescripcion;
    }

    public Long getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(Long catGrupoConceptoId) {
        this.grupoId = catGrupoConceptoId;
    }

    public String getGrupoDescripcion() {
        return grupoDescripcion;
    }

    public void setGrupoDescripcion(String catGrupoConceptoDescripcion) {
        this.grupoDescripcion = catGrupoConceptoDescripcion;
    }

	
    public List<LiquidacionDesConceptoDTO> getLiquidacionDesConcepto() {
		return liquidacionDesConcepto;
	}

	public void setLiquidacionDesConcepto(List<LiquidacionDesConceptoDTO> liquidacionDesConcepto) {
		this.liquidacionDesConcepto = liquidacionDesConcepto;
	}

	public List<LiquidacionDesConceptoDetDTO> getDesgloceConceptoDets() {
		return desgloceConceptoDets;
	}

	public void setDesgloceConceptoDets(List<LiquidacionDesConceptoDetDTO> desgloceConceptoDets) {
		this.desgloceConceptoDets = desgloceConceptoDets;
	}

	public Set<LiquidacionDesDescripcionDTO> getDesgloceDescripciones() {
		return desgloceDescripciones;
	}

	public void setDesgloceDescripciones(Set<LiquidacionDesDescripcionDTO> desgloceDescripciones) {
		this.desgloceDescripciones = desgloceDescripciones;
	}

	public LiquidacionPredialDTO getLiquidacionPredial() {
		return liquidacionPredial;
	}

	public void setLiquidacionPredial(LiquidacionPredialDTO liquidacionPredial) {
		this.liquidacionPredial = liquidacionPredial;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LiquidacionDTO liquidacionDTO = (LiquidacionDTO) o;
        if(liquidacionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), liquidacionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LiquidacionDTO{" +
            "id=" + getId() +
            ", numeroLiquidacion='" + getNumeroLiquidacion() + "'" +
            ", fechaCreacion='" + getFechaCreacion() + "'" +
            ", vigencia='" + getVigencia() + "'" +
            ", referencia='" + getReferencia() + "'" +
            ", tipoLiquidacion='" + getTipoLiquidacion() + "'" +
            ", concepto='" + getConcepto() + "'" +
            ", total='" + getTotal() + "'" +
            ", totalLetra='" + getTotalLetra() + "'" +
            ", redondeo='" + getRedondeo() + "'" +
            ", estatus='" + getEstatus() + "'" +
            "}";
    }
}
