package com.worknest.pagobancomer.service.dto;


import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the PlCarro entity.
 */
public class PlCarroDTO implements Serializable {

    /**
    * 
    */
    private static final long serialVersionUID = 1L;

    private Long id;

    @NotNull
    private LocalDate fecha;

    private Long userId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public PlCarroDTO() {
    }

    public PlCarroDTO(LocalDate fecha, Long userId) {
        this.fecha = fecha;
        this.userId = userId;
    }
    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PlCarroDTO plCarroDTO = (PlCarroDTO) o;
        if(plCarroDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plCarroDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlCarroDTO{" +
            "id=" + getId() +
            ", fecha='" + getFecha() + "'" +
            "}";
    }

}
