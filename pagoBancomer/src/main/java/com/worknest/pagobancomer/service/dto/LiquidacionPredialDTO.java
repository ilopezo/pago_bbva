package com.worknest.pagobancomer.service.dto;

import java.math.BigDecimal;
import java.util.Objects;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class LiquidacionPredialDTO {

    private Long id;

    @NotNull
    @Size(max = 31)
    private String clavecatastral;

    @Size(max = 31)
    private String clavecatastralant;

    private BigDecimal valorcatastral;

    @NotNull
    @Max(value = 99999)
    private Integer binInicial;

    @NotNull
    @Max(value = 99999)
    private Integer bimFinal;

    private BigDecimal supTerreno;

    private BigDecimal supConstruccion;

    @NotNull
    @Size(max = 511)
    private String domicilio;

    @NotNull
    @Size(max = 511)
    private String colonia;

    @Size(max = 20)
    private String tipoPredio;

    @NotNull
    @Size(max = 511)
    private String concepto;

    @NotNull
    @Size(max = 511)
    private String propietarios;

    @Size(max = 13)
    private String rfc;

    private Long liquidacionId;

    private String liquidacionNumeroLiquidacion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClavecatastral() {
        return clavecatastral;
    }

    public void setClavecatastral(String clavecatastral) {
        this.clavecatastral = clavecatastral;
    }

    public String getClavecatastralant() {
        return clavecatastralant;
    }

    public void setClavecatastralant(String clavecatastralant) {
        this.clavecatastralant = clavecatastralant;
    }

    public BigDecimal getValorcatastral() {
        return valorcatastral;
    }

    public void setValorcatastral(BigDecimal valorcatastral) {
        this.valorcatastral = valorcatastral;
    }

    public Integer getBinInicial() {
        return binInicial;
    }

    public void setBinInicial(Integer binInicial) {
        this.binInicial = binInicial;
    }

    public Integer getBimFinal() {
        return bimFinal;
    }

    public void setBimFinal(Integer bimFinal) {
        this.bimFinal = bimFinal;
    }

    public BigDecimal getSupTerreno() {
        return supTerreno;
    }

    public void setSupTerreno(BigDecimal supTerreno) {
        this.supTerreno = supTerreno;
    }

    public BigDecimal getSupConstruccion() {
        return supConstruccion;
    }

    public void setSupConstruccion(BigDecimal supConstruccion) {
        this.supConstruccion = supConstruccion;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getTipoPredio() {
        return tipoPredio;
    }

    public void setTipoPredio(String tipoPredio) {
        this.tipoPredio = tipoPredio;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getPropietarios() {
        return propietarios;
    }

    public void setPropietarios(String propietarios) {
        this.propietarios = propietarios;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public Long getLiquidacionId() {
        return liquidacionId;
    }

    public void setLiquidacionId(Long liquidacionId) {
        this.liquidacionId = liquidacionId;
    }

    public String getLiquidacionNumeroLiquidacion() {
        return liquidacionNumeroLiquidacion;
    }

    public void setLiquidacionNumeroLiquidacion(String liquidacionNumeroLiquidacion) {
        this.liquidacionNumeroLiquidacion = liquidacionNumeroLiquidacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LiquidacionPredialDTO liquidacionPredialDTO = (LiquidacionPredialDTO) o;
        if(liquidacionPredialDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), liquidacionPredialDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LiquidacionPredialDTO{" +
            "id=" + getId() +
            ", clavecatastral='" + getClavecatastral() + "'" +
            ", clavecatastralant='" + getClavecatastralant() + "'" +
            ", valorcatastral='" + getValorcatastral() + "'" +
            ", binInicial='" + getBinInicial() + "'" +
            ", bimFinal='" + getBimFinal() + "'" +
            ", supTerreno='" + getSupTerreno() + "'" +
            ", supConstruccion='" + getSupConstruccion() + "'" +
            ", domicilio='" + getDomicilio() + "'" +
            ", colonia='" + getColonia() + "'" +
            ", tipoPredio='" + getTipoPredio() + "'" +
            ", concepto='" + getConcepto() + "'" +
            ", propietarios='" + getPropietarios() + "'" +
            ", rfc='" + getRfc() + "'" +
            "}";
    }
}
