package com.worknest.pagobancomer.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.worknest.pagobancomer.domain.PlCarro;
import com.worknest.pagobancomer.service.dto.PlCarroDTO;

/**
 * Mapper for the entity PlCarro and its DTO PlCarroDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface PlCarroMapper extends EntityMapper<PlCarroDTO, PlCarro> {

    @Mapping(source = "user", target = "userId")
    PlCarroDTO toDto(PlCarro plCarro); 

    @Mapping(source = "userId", target = "user")
    @Mapping(target = "detalles", ignore = true)
    PlCarro toEntity(PlCarroDTO plCarroDTO);

    default PlCarro fromId(Long id) {
        if (id == null) {
            return null;
        }
        PlCarro plCarro = new PlCarro();
        plCarro.setId(id);
        return plCarro;
    }
}
