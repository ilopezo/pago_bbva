package com.worknest.pagobancomer.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the PlCarroDetHist entity.
 */
public class PlCarroDetHistDTO implements Serializable {

	private Long id;

	@Size(max = 300)
	private String concepto;

	@NotNull
	private Long idLiquidacion;

	@NotNull
	private LocalDate fechaVigencia;

	@NotNull
	private BigDecimal importe;

	@NotNull
	@Size(max = 31)
	private String llave;

	@NotNull
	private Boolean generoUs;

	private Long carroId;

	private Long liquidacionId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public Long getIdLiquidacion() {
		return idLiquidacion;
	}

	public void setIdLiquidacion(Long idLiquidacion) {
		this.idLiquidacion = idLiquidacion;
	}

	public LocalDate getFechaVigencia() {
		return fechaVigencia;
	}

	public void setFechaVigencia(LocalDate fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	public String getLlave() {
		return llave;
	}

	public void setLlave(String llave) {
		this.llave = llave;
	}

	public Boolean isGeneroUs() {
		return generoUs;
	}

	public void setGeneroUs(Boolean generoUs) {
		this.generoUs = generoUs;
	}

	public Long getCarroId() {
		return carroId;
	}

	public void setCarroId(Long plCarroHistId) {
		this.carroId = plCarroHistId;
	}

	public Long getLiquidacionId() {
		return liquidacionId;
	}

	public void setLiquidacionId(Long liquidacionId) {
		this.liquidacionId = liquidacionId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		PlCarroDetHistDTO plCarroDetHistDTO = (PlCarroDetHistDTO) o;
		if (plCarroDetHistDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), plCarroDetHistDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "PlCarroDetHistDTO{" + "id=" + getId() + ", concepto='" + getConcepto() + "'" + ", idLiquidacion='"
				+ getIdLiquidacion() + "'" + ", fechaVigencia='" + getFechaVigencia() + "'" + ", importe='"
				+ getImporte() + "'" + ", llave='" + getLlave() + "'" + ", generoUs='" + isGeneroUs() + "'" + "}";
	}

	public PlCarroDetHistDTO(String concepto, Long idLiquidacion, LocalDate fechaVigencia, BigDecimal importe,
			String llave, Boolean generoUs, Long carroId, Long liquidacion) {
		this.concepto = concepto;
                this.idLiquidacion=idLiquidacion;
		this.fechaVigencia = fechaVigencia;
		this.importe = importe;
		this.llave = llave;
		this.generoUs = generoUs;
		this.carroId = carroId;
                this.liquidacionId=liquidacion;
        }

	public PlCarroDetHistDTO() {

	}
}
