/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worknest.pagobancomer.service.dto;

import javax.validation.constraints.NotNull;

/**
 *
 * @author WorkNest9
 */
public class DatosConciliacionDTO {
    @NotNull
    private String fecha; //mp_date

    private String node; //mp_node
    @NotNull
    private String concepto; //mp_concept
    @NotNull
    private String tipoPago; //mp_paymetmethod
    
    private String referencia; //mp_reference
    @NotNull
    private String numOrden; //mp_order
    @NotNull
    private String autorizacion; //mp_authorization
    @NotNull
    private String idVenta; //mp_saleid
    @NotNull
    private String importe; //mp_amount
    
    private String nombreBanco; //mp_bankname
    @NotNull
    private String nombrePagador; //mp_cardholdername
    @NotNull
    private String email; //mp_email
    @NotNull
    private String telefono; //mp_phone

    public DatosConciliacionDTO() {
    }

    public DatosConciliacionDTO(String fecha, String concepto, String tipoPago, String referencia, String numOrden, String autorizacion, String idVenta, String importe, String nombreBanco, String nombrePagador, String email, String telefono) {
        this.fecha = fecha;
        this.concepto = concepto;
        this.tipoPago = tipoPago;
        this.referencia = referencia;
        this.numOrden = numOrden;
        this.autorizacion = autorizacion;
        this.idVenta = idVenta;
        this.importe = importe;
        this.nombreBanco = nombreBanco;
        this.nombrePagador = nombrePagador;
        this.email = email;
        this.telefono = telefono;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getNumOrden() {
        return numOrden;
    }

    public void setNumOrden(String numOrden) {
        this.numOrden = numOrden;
    }

    public String getAutorizacion() {
        return autorizacion;
    }

    public void setAutorizacion(String autorizacion) {
        this.autorizacion = autorizacion;
    }

    public String getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(String idVenta) {
        this.idVenta = idVenta;
    }

    public String getImporte() {
        return importe;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }

    public String getNombrePagador() {
        return nombrePagador;
    }

    public void setNombrePagador(String nombrePagador) {
        this.nombrePagador = nombrePagador;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

}
