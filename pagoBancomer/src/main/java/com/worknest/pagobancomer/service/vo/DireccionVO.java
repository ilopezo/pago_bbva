/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worknest.pagobancomer.service.vo;

import com.worknest.pagobancomer.domain.enumeration.TipoDireccion;

/**
 *
 * @author WorkNest9
 */
public class DireccionVO {
    private Long id;
    
    private String calle;
    
    private String numero;
    
    private String numeroInterior;
    
    private TipoDireccion tipo;
    
    private CatalogoDirVO catalogoDir;

    public DireccionVO(Long id, String calle, String numero, String numeroInterior, TipoDireccion tipo, CatalogoDirVO catalogoDir) {
        this.id = id;
        this.calle = calle;
        this.numero = numero;
        this.numeroInterior = numeroInterior;
        this.tipo = tipo;
        this.catalogoDir = catalogoDir;
    }

    public DireccionVO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNumeroInterior() {
        return numeroInterior;
    }

    public void setNumeroInterior(String numeroInterior) {
        this.numeroInterior = numeroInterior;
    }

    public TipoDireccion getTipo() {
        return tipo;
    }

    public void setTipo(TipoDireccion tipo) {
        this.tipo = tipo;
    }

    public CatalogoDirVO getCatalogoDir() {
        return catalogoDir;
    }

    public void setCatalogoDir(CatalogoDirVO catalogoDir) {
        this.catalogoDir = catalogoDir;
    }
    
    
    
}
