package com.worknest.pagobancomer.service.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.worknest.pagobancomer.domain.Liquidacion;
import com.worknest.pagobancomer.domain.PlCarro;
import com.worknest.pagobancomer.domain.PlCarroDet;
import com.worknest.pagobancomer.domain.PlCarroDetHist;
import com.worknest.pagobancomer.repository.PlCarroDetHistRepository;
import com.worknest.pagobancomer.repository.PlCarroDetRepository;
import com.worknest.pagobancomer.repository.PlCarroRepository;
import com.worknest.pagobancomer.service.LiquidacionService;
import com.worknest.pagobancomer.service.PlCarroService;
import com.worknest.pagobancomer.service.PlCarroDetService;
import com.worknest.pagobancomer.service.dto.AgregarConceptoDTO;
import com.worknest.pagobancomer.service.dto.AuthenticateDTO;
import com.worknest.pagobancomer.service.dto.LiquidacionDTO;
import com.worknest.pagobancomer.service.dto.PlCarroDTO;
import com.worknest.pagobancomer.service.dto.PlCarroDetDTO;
import com.worknest.pagobancomer.service.dto.TokenDTO;
import com.worknest.pagobancomer.service.mapper.PlCarroDetMapper;
import com.worknest.pagobancomer.service.vo.LiquidacionAdeudoPredialVO;
import com.worknest.pagobancomer.web.rest.errors.ExceptionAPI;
import com.worknest.pagobancomer.web.rest.util.Respuesta;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Service Implementation for managing PlCarroDet.
 */
@Service
@Transactional
public class PlCarroDetServiceImpl implements PlCarroDetService{

    private final Logger log = LoggerFactory.getLogger(PlCarroDetServiceImpl.class);

    private final PlCarroDetRepository plCarroDetRepository;
    private final PlCarroDetHistRepository plCarroDetHistRepository;
    private final PlCarroRepository plCarroRepository;
    private final LiquidacionService liquidacionService;

    @Autowired
    private PlCarroService plCarroService;
    private final PlCarroDetMapper plCarroDetMapper;
    private String _token;
    
    @Value("${application.host}") private String _host;
    @Value("${application.port}") private String _port;
    @Value("${application.autenticate}") private String _autenticate;
    @Value("${application.lpgenera}") private String _lpgenera;
    @Value("${application.deleteLiq}") private String _deleteLiq;
    @Value("${application.usuario}") private String _usuario;
    @Value("${application.pass}") private String _pass;
    @Value("${application.pagarLiq}") private String _pagarLiq;
    
    public PlCarroDetServiceImpl(PlCarroDetRepository plCarroDetRepository, PlCarroDetMapper plCarroDetMapper, 
    		PlCarroRepository plCarroRepository, LiquidacionService liquidacionService, PlCarroDetHistRepository plCarroDetHistRepository) {
        this.plCarroDetRepository = plCarroDetRepository;
        this.plCarroDetMapper = plCarroDetMapper;
        this.plCarroRepository = plCarroRepository;
        this.liquidacionService = liquidacionService;
        this.plCarroDetHistRepository = plCarroDetHistRepository;
    }

    /**
     * Save a plCarroDet.
     *
     * @param plCarroDetDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PlCarroDetDTO save(PlCarroDetDTO plCarroDetDTO) {
        log.debug("Request to save PlCarroDet : {}", plCarroDetDTO);
        PlCarroDet plCarroDet = plCarroDetMapper.toEntity(plCarroDetDTO);
        plCarroDet = plCarroDetRepository.save(plCarroDet);
        return plCarroDetMapper.toDto(plCarroDet);
    }

    /**
     *  Get all the plCarroDets.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PlCarroDetDTO> findAll() {
        log.debug("Request to get all PlCarroDets");
        return plCarroDetRepository.findAll().stream()
            .map(plCarroDetMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one plCarroDet by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PlCarroDetDTO findOne(Long id) {
        log.debug("Request to get PlCarroDet : {}", id);
        PlCarroDet plCarroDet = plCarroDetRepository.findOne(id);
        return plCarroDetMapper.toDto(plCarroDet);
    }

    /**
     *  Delete the  plCarroDet by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PlCarroDet : {}", id);
        plCarroDetRepository.delete(id);
    }

    /**
     * Se agrega un nuevo concepto al carro 
     * @param nuevoConcepto contiene la llave y los bimestres inicio y fin
     * si los bimestres vienen nulos se consume un WS que genera una liquidacion
     * @param idCarro contiene el id del carro delusuario logeado
     * @throws ExceptionAPI Si el algun paso del proceso sale mal se devuelve n mensaje de error
     * @throws Exception 
     */
    @Override
    public void guardarCarroDet(AgregarConceptoDTO nuevoConcepto, Long idCarro) throws ExceptionAPI, Exception {
    	
        // agregar al carro por numero de  liquidacion
        if (nuevoConcepto.getBimini() == null || nuevoConcepto.getBimfin() == null) {

            // buscar si existe la liquidacion en el detalle del carro
            List<PlCarroDet> listaCarroDet = this.plCarroDetRepository.findByLlaveCarro(nuevoConcepto.getLlave(), idCarro);
            
            //si se encontraron conceptos con el numero de liquidacion no se agrega al carroDet
            if(!listaCarroDet.isEmpty()) {
                throw new ExceptionAPI(HttpStatus.BAD_REQUEST, "Ya se tiene agregada la liquidacion con el numero " + nuevoConcepto.getLlave());
            }
            
            //se busca si la liquidacion existe
            LiquidacionDTO liq = this.liquidacionService.findOneByNumLiq(Long.parseLong(nuevoConcepto.getLlave(), 10));
            if (liq == null) {
                // se arroja una excepcion personalizada con mensaje personalizado
                throw new ExceptionAPI(HttpStatus.BAD_REQUEST, "No se encontro la liquidacion con el numero: " + nuevoConcepto.getLlave());					
            }

            // se prepara la entidad a guardar
            PlCarroDet plCarroDet = new PlCarroDet();
            plCarroDet.setConcepto(liq.getConcepto());
            plCarroDet.setFechaVigencia(liq.getVigencia());
            plCarroDet.setGeneroUs(false);
            plCarroDet.setImporte(liq.getTotal());
            plCarroDet.setLlave(nuevoConcepto.getLlave());

            plCarroDet.setCarro(new PlCarro());
            plCarroDet.getCarro().setId(idCarro);

            plCarroDet.setLiquidacion(new Liquidacion());
            plCarroDet.getLiquidacion().setId(liq.getId());
            
            // se guarda el nuevo concepto en la tabla
            this.plCarroDetRepository.save(plCarroDet);

        } else { // agregar al carro por clave catastral
            try { // generar liquidacion con los datos del nuevo concepto
                
            	// se verifica si el token ya existe
                if (this._token == null) {
                        this._token = this.generaToken(this._usuario, this._pass).getId_token(); // se genera el token
                }
            	
                // se manda a llamar al metodo que va a consultar los WS que generan las
                // liquidaciones
                List<LiquidacionDTO> resultado = this.generaLiquidaciones(nuevoConcepto);

                for (LiquidacionDTO liq : resultado) {
                    // se prepara la entidad a guardar
                    PlCarroDet plCarroDet = new PlCarroDet();
                    plCarroDet.setConcepto(liq.getConcepto());
                    plCarroDet.setFechaVigencia(liq.getVigencia());
                    plCarroDet.setGeneroUs(true);
                    plCarroDet.setImporte(liq.getTotal());
                    plCarroDet.setLlave(nuevoConcepto.getLlave());

                    plCarroDet.setCarro(new PlCarro());
                    plCarroDet.getCarro().setId(idCarro);

                    plCarroDet.setLiquidacion(new com.worknest.pagobancomer.domain.Liquidacion());
                    plCarroDet.getLiquidacion().setId(liq.getId());

                    // se guarda el nuevo concepto en la tabla
                    this.plCarroDetRepository.save(plCarroDet);
                }

            } catch (HttpStatusCodeException e) {
            	
            	String responseString = e.getResponseBodyAsString();
                ObjectMapper mapper = new ObjectMapper();
            	Respuesta result = mapper.readValue(responseString,
            			Respuesta.class);       	
                throw new ExceptionAPI(e.getStatusCode(), result.getMensaje());
            }
        }
    }

    /**
     * Se busca los conceptos que el carro del usuario logeado contiene
     * @return una lista de los conceptos dentro del carro
     * @throws ExceptionAPI regresa un mensaje de error cuando no se encuentra el carro 
     *                       o cuando los detalles estan vacios
     * @throws Exception 
     */
    @Override
    public List<PlCarroDetDTO> buscarPorCarro(Long idCarro) throws ExceptionAPI, Exception {
    		
        //se buscan los conceptos segun el carro del usuario1
        List<PlCarroDet> listaCarro = plCarroDetRepository.findByCarroId(idCarro);         
        // si no se encuentran conceptos en el carro se devuelve un mensaje de error
        if(listaCarro.isEmpty()){  	
            // se arroja una excepcion personalizada con mensaje personalizado
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST,"No se encontraron conceptos dentro del carro");
        }
        return listaCarro.stream()
            .map(plCarroDetMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }
    
    /**
     * Se borra un concepto del carroDet a travez de la llave
     * @param llave puede ser numero de liquidacion o clave catastral
     * @param idCarro
     * @param generoUs
     * @param envio
     * @throws ExceptionAPI
     * @throws Exception 
     */
    @Override
    public void borrarConcepto(String llave, Long idCarro, boolean generoUs, List<Long> envio) throws ExceptionAPI, Exception {  	
        // si fue generada por el usuario
        if (generoUs) {
            // Borrar todas las liquidaciones segun la llave
            try {
                // genera token
                // validacion
                if (this._token == null)
                        this._token = this.generaToken(this._usuario, this._pass).getId_token();

                // se envia la solicitud para eliminar todas las liquidaciones
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

                String uri = new String(this._host + ":" + this._port + this._deleteLiq);
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                headers.set("Authorization", "Bearer " + this._token);

                HttpEntity<?> entity = new HttpEntity<List<Long>>(envio, headers);

                // se manda ejecutar el ws que elimina las liquidaciones
                restTemplate.exchange(uri, HttpMethod.DELETE, entity, Void.class);

            } catch (HttpStatusCodeException e) {
                    throw new ExceptionAPI(e.getStatusCode(), "Problemas al eliminar las liquidaciones");
            }
        }
        // se borran los conceptos del a base de datos (pl_carro_det)
        // this.plCarroDetRepository.delete(concepto);
    }
    
    /**
     * Genera token para comunicacion con WS acapulco-caja
     * @param usuario usuario para generar token 
     * @param pass clave de acceso para generar token
     * @return token obtenido
     */
    public TokenDTO generaToken(String usuario, String pass) {

        // se definie las credenciales FIJAS
        AuthenticateDTO auth = new AuthenticateDTO();

        auth.setUsername(usuario); // user
        auth.setRememberMe(true);
        auth.setPassword(pass);    // user

        // se definie los headers
        Map<String, String> vars = new HashMap<String, String>();
        vars.put("Content-Type", "application/json");

        // se consulta el ws que genera el token
        RestTemplate rt = new RestTemplate();
        rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        rt.getMessageConverters().add(new StringHttpMessageConverter());

        String uri = new String(this._host + ":" + this._port + this._autenticate);

        TokenDTO token = new TokenDTO();

        token = rt.postForObject(uri, auth, TokenDTO.class, vars);

        // regresa el token
        return token;
    }

    /**
     * metodo para crear una nueva liquidacion a partir de un nuevo concepto
     * @param nuevaLiquidacion contiene los datos clave catastral, bimestre inicial y bimestre final
     * @return lista de liquidaciones generadas 
     */
    public List<LiquidacionDTO> generaLiquidaciones(AgregarConceptoDTO nuevaLiquidacion){
    	
        List<LiquidacionDTO> listaLiquidaciones = null;

        // Se prepara la entidad de envio con los datos necesarios
        LiquidacionAdeudoPredialVO envio = new LiquidacionAdeudoPredialVO(
                                                    nuevaLiquidacion.getBimini(), // Bimestre Inicial
                                                    nuevaLiquidacion.getBimfin(), // Bimestre Final
                                                    nuevaLiquidacion.getLlave(),  // Clave Catastral
                                                    true,  // Es agrupada
                                                    true, // Es pago en linea
                                                    true); // Guardar Liquidacion


        // Se consulta el WS que genera el token
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + this._token);

        //Genera la url para consumir el WS que genera liquidaciones
        String uri = new String(this._host + ":" + this._port + this._lpgenera);

        HttpEntity<?> entity = new HttpEntity<LiquidacionAdeudoPredialVO>(envio, headers);

        // El WS genera las liquidaciones en base a los datos de envio
        ResponseEntity<List<LiquidacionDTO>> responseEntity = restTemplate.exchange(uri, HttpMethod.POST, entity,
                        new ParameterizedTypeReference<List<LiquidacionDTO>>() {});

        listaLiquidaciones = responseEntity.getBody();

        return listaLiquidaciones;
    }

    /**
     * Borra los conceptos con la llave del carroDet del usuario
     * @param llave clave catastral o numero de liquidacion
     * @param carro
     * @return true o false dependiendo si la liquidacion la genero el usuario
     * @throws ExceptionAPI
     * @throws Exception 
     */
    @Override
    public boolean borraDetalle(String llave, PlCarroDTO carro) throws ExceptionAPI, Exception {
        // borrar todo el detalle que tenga que ver con la llave PL_CARRO_DET
        List<PlCarroDet> carroDetalles = this.plCarroDetRepository.findByLlave(llave);
        
        //obtiene de los detalles del carro si la liquidacion fue generada por el usuario
        boolean generoUs = carroDetalles.size() > 0 ? carroDetalles.get(0).isGeneroUs() : false;
        
        //si se genero por el usuario se borran todos los detalles que contengan la llave
        if(generoUs){
            this.plCarroDetRepository.delete(carroDetalles);
        }else{
            //si NO se genero por el usuario solo se borra el detalle que tenga el carro del usuario
            for (PlCarroDet carroDetalle : carroDetalles) {
                if (carro.getId()==carroDetalle.getCarro().getId()) {
                    plCarroDetRepository.delete(carroDetalle);
                }
            }
        }   
        return generoUs;
    }

    /**
     * Elimina del historial de detalles el id de liquidacion
     * @param llave clave catastral o numero de liquidacion
     * @throws ExceptionAPI
     * @throws Exception 
     */
    @Override
    public void borraDetalleHist(String llave) throws ExceptionAPI, Exception {
        // borrar (update id_liquidacion = null) todo el detalle que tenga que ver con la llave PL_CARRO_DET_HIST
        // siempre y cuando no se haya pagado la liquidacion
        List<PlCarroDetHist> plCarroDetHistL = this.plCarroDetHistRepository.getAllByLlave(llave);
        
        //cambia los id liquidacion de lista obtenida a nulos
        plCarroDetHistL.stream().forEach(a -> a.setLiquidacion(null));
        this.plCarroDetHistRepository.save(plCarroDetHistL);
    }

    /**
     * Obtiene los detalles del carro por medio de la llave
     * @param llave clave catastral o numero de liquidacion
     * @param id del carro del usuario
     * @return lista de conceptos dentro del carroDet que contienen la llave dada
     * @throws ExceptionAPI
     * @throws Exception 
     */
    @Override
    public List<PlCarroDetDTO> getDetallesByLlaveCarro(String llave, Long id) throws ExceptionAPI, Exception {
        //busca los detalles del carro por medio de la llave
        List<PlCarroDet> listaConceptos =  this.plCarroDetRepository.findByLlaveCarro(llave, id);
        
        // si no se encuentra se arroja una excepcion
        if (listaConceptos.isEmpty()) {
            // se arroja una excepcion personalizada con mensaje personalizado
            throw new ExceptionAPI(HttpStatus.BAD_REQUEST, "No se encuentra la llave del concepto");
        }
        return this.plCarroDetMapper.toDto(listaConceptos);
    }

    @Override
    public void deleteLiquidacion(Long idLiquidacion) throws ExceptionAPI {        
        List<PlCarroDet> listaConceptos = plCarroDetRepository.findByLiquidacion(idLiquidacion);
        if (!listaConceptos.isEmpty()) {
            for (PlCarroDet concepto : listaConceptos) {
                List<PlCarroDetHist> carroDet = plCarroDetHistRepository.getAllByLlave(concepto.getLlave());
                if(carroDet.isEmpty()){
                    throw new ExceptionAPI(HttpStatus.BAD_REQUEST, "No se puede borrar la liquidacion del carro");
                }
            }
            plCarroDetRepository.delete(listaConceptos);
        }
    }
}
