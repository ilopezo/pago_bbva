package com.worknest.pagobancomer.service.dto;


import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.worknest.pagobancomer.domain.enumeration.StatusIntentoPago;

/**
 * A DTO for the PlIntentoPago entity.
 */
public class PlIntentoPagoDTO implements Serializable {

    private Long id;

    @NotNull
    private ZonedDateTime fecha;

    private Boolean enviado;

    private StatusIntentoPago status;

    @Size(max = 30)
    private String auth;

    private Long historialcarroId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getFecha() {
        return fecha;
    }

    public void setFecha(ZonedDateTime fecha) {
        this.fecha = fecha;
    }

    public Boolean isEnviado() {
        return enviado;
    }

    public void setEnviado(Boolean enviado) {
        this.enviado = enviado;
    }

    public StatusIntentoPago getStatus() {
        return status;
    }

    public void setStatus(StatusIntentoPago status) {
        this.status = status;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public Long getHistorialcarroId() {
        return historialcarroId;
    }

    public void setHistorialcarroId(Long plCarroHistId) {
        this.historialcarroId = plCarroHistId;
    }

    public PlIntentoPagoDTO() {
    }

    public PlIntentoPagoDTO(ZonedDateTime fecha, Boolean enviado, StatusIntentoPago status, String auth, Long historialcarroId) {
        this.fecha = fecha;
        this.enviado = enviado;
        this.status = status;
        this.auth = auth;
        this.historialcarroId = historialcarroId;
    }
    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PlIntentoPagoDTO plIntentoPagoDTO = (PlIntentoPagoDTO) o;
        if(plIntentoPagoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plIntentoPagoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlIntentoPagoDTO{" +
            "id=" + getId() +
            ", fecha='" + getFecha() + "'" +
            ", enviado='" + isEnviado() + "'" +
            ", status='" + getStatus() + "'" +
            ", auth='" + getAuth() + "'" +
            "}";
    }
}
