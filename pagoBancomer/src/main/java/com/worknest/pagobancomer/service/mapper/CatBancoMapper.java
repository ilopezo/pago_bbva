package com.worknest.pagobancomer.service.mapper;

import org.mapstruct.Mapper;

import com.worknest.pagobancomer.domain.CatBanco;
import com.worknest.pagobancomer.service.dto.CatBancoDTO;

/**
 * Mapper for the entity CatBanco and its DTO CatBancoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CatBancoMapper extends EntityMapper<CatBancoDTO, CatBanco> {

    

    

    default CatBanco fromId(Long id) {
        if (id == null) {
            return null;
        }
        CatBanco catBanco = new CatBanco();
        catBanco.setId(id);
        return catBanco;
    }
}
