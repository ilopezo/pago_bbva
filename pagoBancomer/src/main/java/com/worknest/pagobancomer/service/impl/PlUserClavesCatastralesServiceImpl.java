package com.worknest.pagobancomer.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.worknest.pagobancomer.domain.PlUserClavesCatastrales;
import com.worknest.pagobancomer.repository.PlUserClavesCatastralesRepository;
import com.worknest.pagobancomer.service.PlUserClavesCatastralesService;
import com.worknest.pagobancomer.service.dto.PlUserClavesCatastralesDTO;
import com.worknest.pagobancomer.service.mapper.PlUserClavesCatastralesMapper;

/**
 * Service Implementation for managing PlUserClavesCatastrales.
 */
@Service
@Transactional
public class PlUserClavesCatastralesServiceImpl implements PlUserClavesCatastralesService{

    private final Logger log = LoggerFactory.getLogger(PlUserClavesCatastralesServiceImpl.class);

    private final PlUserClavesCatastralesRepository plUserClavesCatastralesRepository;

    private final PlUserClavesCatastralesMapper plUserClavesCatastralesMapper;

    public PlUserClavesCatastralesServiceImpl(PlUserClavesCatastralesRepository plUserClavesCatastralesRepository, PlUserClavesCatastralesMapper plUserClavesCatastralesMapper) {
        this.plUserClavesCatastralesRepository = plUserClavesCatastralesRepository;
        this.plUserClavesCatastralesMapper = plUserClavesCatastralesMapper;
    }

    /**
     * Save a plUserClavesCatastrales.
     *
     * @param plUserClavesCatastralesDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PlUserClavesCatastralesDTO save(PlUserClavesCatastralesDTO plUserClavesCatastralesDTO) {
        log.debug("Request to save PlUserClavesCatastrales : {}", plUserClavesCatastralesDTO);
        PlUserClavesCatastrales plUserClavesCatastrales = plUserClavesCatastralesMapper.toEntity(plUserClavesCatastralesDTO);
        plUserClavesCatastrales = plUserClavesCatastralesRepository.save(plUserClavesCatastrales);
        return plUserClavesCatastralesMapper.toDto(plUserClavesCatastrales);
    }

    /**
     *  Get all the plUserClavesCatastrales.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PlUserClavesCatastralesDTO> findAll() {
        log.debug("Request to get all PlUserClavesCatastrales");
        return plUserClavesCatastralesRepository.findAll().stream()
            .map(plUserClavesCatastralesMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one plUserClavesCatastrales by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PlUserClavesCatastralesDTO findOne(Long id) {
        log.debug("Request to get PlUserClavesCatastrales : {}", id);
        PlUserClavesCatastrales plUserClavesCatastrales = plUserClavesCatastralesRepository.findOne(id);
        return plUserClavesCatastralesMapper.toDto(plUserClavesCatastrales);
    }

    /**
     *  Delete the  plUserClavesCatastrales by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PlUserClavesCatastrales : {}", id);
        plUserClavesCatastralesRepository.delete(id);
    }
}
