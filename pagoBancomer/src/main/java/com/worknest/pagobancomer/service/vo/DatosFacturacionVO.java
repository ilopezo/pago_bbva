/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worknest.pagobancomer.service.vo;

/**
 *
 * @author WorkNest9
 */
public class DatosFacturacionVO {
    private Long id;
    
    private String rfc;
    
    private String nomrazonSocial;
    
    private PersonaVO persona;
    
    private DireccionVO direccion;
    
    private EmailVO email;

    public DatosFacturacionVO() {
    }

    public DatosFacturacionVO(Long id, String rfc, String nomrazonSocial, PersonaVO persona, DireccionVO direccion, EmailVO email) {
        this.id = id;
        this.rfc = rfc;
        this.nomrazonSocial = nomrazonSocial;
        this.persona = persona;
        this.direccion = direccion;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNomrazonSocial() {
        return nomrazonSocial;
    }

    public void setNomrazonSocial(String nomrazonSocial) {
        this.nomrazonSocial = nomrazonSocial;
    }

    public PersonaVO getPersona() {
        return persona;
    }

    public void setPersona(PersonaVO persona) {
        this.persona = persona;
    }

    public DireccionVO getDireccion() {
        return direccion;
    }

    public void setDireccion(DireccionVO direccion) {
        this.direccion = direccion;
    }

    public EmailVO getEmail() {
        return email;
    }

    public void setEmail(EmailVO email) {
        this.email = email;
    }
   
}
