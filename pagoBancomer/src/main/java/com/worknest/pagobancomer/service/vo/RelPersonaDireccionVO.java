/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worknest.pagobancomer.service.vo;

/**
 *
 * @author WorkNest9
 */
public class RelPersonaDireccionVO {
    private Long id;
    
    private DireccionVO direccion;
    
    private PersonaVO persona;

    public RelPersonaDireccionVO() {
    }

    public RelPersonaDireccionVO(Long id, DireccionVO direccion, PersonaVO persona) {
        this.id = id;
        this.direccion = direccion;
        this.persona = persona;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DireccionVO getDireccion() {
        return direccion;
    }

    public void setDireccion(DireccionVO direccion) {
        this.direccion = direccion;
    }

    public PersonaVO getPersona() {
        return persona;
    }

    public void setPersona(PersonaVO persona) {
        this.persona = persona;
    }
    
}
