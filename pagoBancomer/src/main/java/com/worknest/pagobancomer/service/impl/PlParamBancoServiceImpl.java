package com.worknest.pagobancomer.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.worknest.pagobancomer.domain.PlParamBanco;
import com.worknest.pagobancomer.repository.PlParamBancoRepository;
import com.worknest.pagobancomer.service.PlParamBancoService;
import com.worknest.pagobancomer.service.dto.PlParamBancoDTO;
import com.worknest.pagobancomer.service.mapper.PlParamBancoMapper;

/**
 * Service Implementation for managing PlParamBanco.
 */
@Service
@Transactional
public class PlParamBancoServiceImpl implements PlParamBancoService{

    private final Logger log = LoggerFactory.getLogger(PlParamBancoServiceImpl.class);

    private final PlParamBancoRepository plParamBancoRepository;

    private final PlParamBancoMapper plParamBancoMapper;

    public PlParamBancoServiceImpl(PlParamBancoRepository plParamBancoRepository, PlParamBancoMapper plParamBancoMapper) {
        this.plParamBancoRepository = plParamBancoRepository;
        this.plParamBancoMapper = plParamBancoMapper;
    }

    /**
     * Save a plParamBanco.
     *
     * @param plParamBancoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PlParamBancoDTO save(PlParamBancoDTO plParamBancoDTO) {
        log.debug("Request to save PlParamBanco : {}", plParamBancoDTO);
        PlParamBanco plParamBanco = plParamBancoMapper.toEntity(plParamBancoDTO);
        plParamBanco = plParamBancoRepository.save(plParamBanco);
        return plParamBancoMapper.toDto(plParamBanco);
    }

    /**
     *  Get all the plParamBancos.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PlParamBancoDTO> findAll() {
        log.debug("Request to get all PlParamBancos");
        return plParamBancoRepository.findAll().stream()
            .map(plParamBancoMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one plParamBanco by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PlParamBancoDTO findOne(Long id) {
        log.debug("Request to get PlParamBanco : {}", id);
        PlParamBanco plParamBanco = plParamBancoRepository.findOne(id);
        return plParamBancoMapper.toDto(plParamBanco);
    }

    /**
     *  Delete the  plParamBanco by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PlParamBanco : {}", id);
        plParamBancoRepository.delete(id);
    }
}
