/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worknest.pagobancomer.service.vo;

import com.worknest.pagobancomer.domain.enumeration.Tel;

/**
 *
 * @author WorkNest9
 */
public class TelefonoVO {
    private Long id;
    
    private String lada;
    
    private String telefono;

    private Tel tipo;

    public TelefonoVO() {
    }

    public TelefonoVO(Long id, String lada, String telefono, Tel tipo) {
        this.id = id;
        this.lada = lada;
        this.telefono = telefono;
        this.tipo = tipo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLada() {
        return lada;
    }

    public void setLada(String lada) {
        this.lada = lada;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Tel getTipo() {
        return tipo;
    }

    public void setTipo(Tel tipo) {
        this.tipo = tipo;
    }
    
}
