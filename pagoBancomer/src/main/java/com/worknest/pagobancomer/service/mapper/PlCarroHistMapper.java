package com.worknest.pagobancomer.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.worknest.pagobancomer.domain.PlCarroHist;
import com.worknest.pagobancomer.service.dto.PlCarroHistDTO;

/**
 * Mapper for the entity PlCarroHist and its DTO PlCarroHistDTO.
 */
@Mapper(componentModel = "spring", uses = {PlCarroMapper.class})
public interface PlCarroHistMapper extends EntityMapper<PlCarroHistDTO, PlCarroHist> {

    @Mapping(source = "carro.id", target = "carroId")
    PlCarroHistDTO toDto(PlCarroHist plCarroHist); 

    @Mapping(source = "carroId", target = "carro")
    @Mapping(target = "detalles", ignore = true)
    PlCarroHist toEntity(PlCarroHistDTO plCarroHistDTO);

    default PlCarroHist fromId(Long id) {
        if (id == null) {
            return null;
        }
        PlCarroHist plCarroHist = new PlCarroHist();
        plCarroHist.setId(id);
        return plCarroHist;
    }
}
