package com.worknest.pagobancomer.service.dto;

import java.math.BigDecimal;
import java.util.Objects;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.worknest.pagobancomer.domain.enumeration.TipoAdeudo;

public class LiquidacionDesConceptoDetDTO {
	private Long id;

	@NotNull
	private Integer orden;

	@NotNull
	private BigDecimal importe;

	@Size(max = 250)
	private String descripcion;

	@Max(value = 99999)
	private Integer bimInicial;

	@Max(value = 99999)
	private Integer bimFinal;

	private TipoAdeudo tipo;

	private Long liquidacionId;

	private String liquidacionNumeroLiquidacion;

	private Long conceptoId;

	private String conceptoDescripcion;

	private String claveConcepto;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getBimInicial() {
		return bimInicial;
	}

	public void setBimInicial(Integer bimInicial) {
		this.bimInicial = bimInicial;
	}

	public Integer getBimFinal() {
		return bimFinal;
	}

	public void setBimFinal(Integer bimFinal) {
		this.bimFinal = bimFinal;
	}

	public TipoAdeudo getTipo() {
		return tipo;
	}

	public void setTipo(TipoAdeudo tipo) {
		this.tipo = tipo;
	}

	public Long getLiquidacionId() {
		return liquidacionId;
	}

	public void setLiquidacionId(Long liquidacionId) {
		this.liquidacionId = liquidacionId;
	}

	public String getLiquidacionNumeroLiquidacion() {
		return liquidacionNumeroLiquidacion;
	}

	public void setLiquidacionNumeroLiquidacion(String liquidacionNumeroLiquidacion) {
		this.liquidacionNumeroLiquidacion = liquidacionNumeroLiquidacion;
	}

	public Long getConceptoId() {
		return conceptoId;
	}

	public void setConceptoId(Long catConceptoId) {
		this.conceptoId = catConceptoId;
	}

	public String getConceptoDescripcion() {
		return conceptoDescripcion;
	}

	public void setConceptoDescripcion(String conceptoDescripcion) {
		this.conceptoDescripcion = conceptoDescripcion;
	}

	public String getClaveConcepto() {
		return claveConcepto;
	}

	public void setClaveConcepto(String claveConcepto) {
		this.claveConcepto = claveConcepto;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		LiquidacionDesConceptoDetDTO liquidacionDesConceptoDetDTO = (LiquidacionDesConceptoDetDTO) o;
		if (liquidacionDesConceptoDetDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), liquidacionDesConceptoDetDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "LiquidacionDesConceptoDetDTO{" + "id=" + getId() + ", orden='" + getOrden() + "'" + ", importe='"
				+ getImporte() + "'" + ", descripcion='" + getDescripcion() + "'" + ", bimInicial='" + getBimInicial()
				+ "'" + ", bimFinal='" + getBimFinal() + "'" + ", tipo='" + getTipo() + "'" + "}";
	}

}
