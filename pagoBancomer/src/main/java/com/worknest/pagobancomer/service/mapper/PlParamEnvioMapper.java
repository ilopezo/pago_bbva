package com.worknest.pagobancomer.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.worknest.pagobancomer.domain.PlParamEnvio;
import com.worknest.pagobancomer.service.dto.PlParamEnvioDTO;

/**
 * Mapper for the entity PlParamEnvio and its DTO PlParamEnvioDTO.
 */
@Mapper(componentModel = "spring", uses = {PlParamBancoMapper.class, PlIntentoPagoMapper.class})
public interface PlParamEnvioMapper extends EntityMapper<PlParamEnvioDTO, PlParamEnvio> {

    @Mapping(source = "paramBanco.id", target = "paramBancoId")
    @Mapping(source = "intentoPago.id", target = "intentoPagoId")
    PlParamEnvioDTO toDto(PlParamEnvio plParamEnvio); 

    @Mapping(source = "paramBancoId", target = "paramBanco")
    @Mapping(source = "intentoPagoId", target = "intentoPago")
    PlParamEnvio toEntity(PlParamEnvioDTO plParamEnvioDTO);

    default PlParamEnvio fromId(Long id) {
        if (id == null) {
            return null;
        }
        PlParamEnvio plParamEnvio = new PlParamEnvio();
        plParamEnvio.setId(id);
        return plParamEnvio;
    }
}
