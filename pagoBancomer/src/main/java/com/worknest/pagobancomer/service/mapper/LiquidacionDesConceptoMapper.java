package com.worknest.pagobancomer.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.worknest.pagobancomer.domain.LiquidacionDesConcepto;
import com.worknest.pagobancomer.service.dto.LiquidacionDesConceptoDTO;

/**
 * Mapper for the entity LiquidacionDesConcepto and its DTO LiquidacionDesConceptoDTO.
 */
@Mapper(componentModel = "spring", uses = {LiquidacionMapper.class })
public interface LiquidacionDesConceptoMapper extends EntityMapper <LiquidacionDesConceptoDTO, LiquidacionDesConcepto> {


    @Mapping(source = "liquidacion.id", target = "liquidacionId")
    @Mapping(source = "liquidacion.numeroLiquidacion", target = "liquidacionNumeroLiquidacion")
    @Mapping(source = "concepto.id", target = "conceptoId")
    @Mapping(source = "concepto.descripcion", target = "conceptoDescripcion")
    @Mapping(source = "concepto.clave", target = "claveConcepto")
    LiquidacionDesConceptoDTO toDto(LiquidacionDesConcepto liquidacionDesConcepto); 

    @Mapping(source = "liquidacionId", target = "liquidacion")

    LiquidacionDesConcepto toEntity(LiquidacionDesConceptoDTO liquidacionDesConceptoDTO); 
    default LiquidacionDesConcepto fromId(Long id) {
        if (id == null) {
            return null;
        }
        LiquidacionDesConcepto liquidacionDesConcepto = new LiquidacionDesConcepto();
        liquidacionDesConcepto.setId(id);
        return liquidacionDesConcepto;
    }
}
