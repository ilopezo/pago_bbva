package com.worknest.pagobancomer.service.dto;


import java.io.Serializable;

import java.time.ZonedDateTime;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * A DTO for the PlCarroHist entity.
 */
public class PlCarroHistDTO implements Serializable {

    private Long id;

    @NotNull
    private ZonedDateTime fechaEnvio;

    @NotNull
    @Size(max = 250)
    private String referencia;

    private Long carroId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(ZonedDateTime fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public Long getCarroId() {
        return carroId;
    }

    public void setCarroId(Long plCarroId) {
        this.carroId = plCarroId;
    }

    public PlCarroHistDTO() {
    }

    public PlCarroHistDTO(ZonedDateTime fechaEnvio, String referencia, Long carroId) {
        this.fechaEnvio = fechaEnvio;
        this.referencia = referencia;
        this.carroId = carroId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PlCarroHistDTO plCarroHistDTO = (PlCarroHistDTO) o;
        if(plCarroHistDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plCarroHistDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlCarroHistDTO{" +
            "id=" + getId() +
            ", fechaEnvio='" + getFechaEnvio() + "'" +
            ", referencia='" + getReferencia() + "'" +
            "}";
    }
}
