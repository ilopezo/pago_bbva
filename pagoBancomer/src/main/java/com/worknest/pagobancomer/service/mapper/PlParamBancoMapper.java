package com.worknest.pagobancomer.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.worknest.pagobancomer.domain.PlParamBanco;
import com.worknest.pagobancomer.service.dto.PlParamBancoDTO;

/**
 * Mapper for the entity PlParamBanco and its DTO PlParamBancoDTO.
 */
@Mapper(componentModel = "spring", uses = {CatBancoMapper.class})
public interface PlParamBancoMapper extends EntityMapper<PlParamBancoDTO, PlParamBanco> {

    @Mapping(source = "institucionBancaria.id", target = "institucionBancariaId")
    @Mapping(source = "institucionBancaria.descripcion", target = "institucionBancariaDescripcion")
    PlParamBancoDTO toDto(PlParamBanco plParamBanco); 

    @Mapping(source = "institucionBancariaId", target = "institucionBancaria")
    PlParamBanco toEntity(PlParamBancoDTO plParamBancoDTO);

    default PlParamBanco fromId(Long id) {
        if (id == null) {
            return null;
        }
        PlParamBanco plParamBanco = new PlParamBanco();
        plParamBanco.setId(id);
        return plParamBanco;
    }
}
