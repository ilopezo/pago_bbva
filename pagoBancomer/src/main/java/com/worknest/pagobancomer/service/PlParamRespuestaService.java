package com.worknest.pagobancomer.service;

import java.util.List;
import com.worknest.pagobancomer.domain.PlParamRespuesta;
import com.worknest.pagobancomer.service.dto.PlParamRespuestaDTO;

/**
 * Service Interface for managing PlParamRespuesta.
 */
public interface PlParamRespuestaService {

    /**
     * Save a plParamRespuesta.
     *
     * @param plParamRespuestaDTO the entity to save
     * @return the persisted entity
     */
    PlParamRespuestaDTO save(PlParamRespuestaDTO plParamRespuestaDTO);

    /**
     *  Get all the plParamRespuestas.
     *
     *  @return the list of entities
     */
    List<PlParamRespuestaDTO> findAll();

    /**
     *  Get the "id" plParamRespuesta.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PlParamRespuestaDTO findOne(Long id);

    /**
     *  Delete the "id" plParamRespuesta.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
    
    PlParamRespuesta guardarPlParamRespuesta(PlParamRespuesta plParamRespuesta);
    
}
