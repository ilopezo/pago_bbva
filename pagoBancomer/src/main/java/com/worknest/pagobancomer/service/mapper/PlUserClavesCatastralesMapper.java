package com.worknest.pagobancomer.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.worknest.pagobancomer.domain.PlUserClavesCatastrales;
import com.worknest.pagobancomer.service.dto.PlUserClavesCatastralesDTO;

/**
 * Mapper for the entity PlUserClavesCatastrales and its DTO PlUserClavesCatastralesDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface PlUserClavesCatastralesMapper extends EntityMapper<PlUserClavesCatastralesDTO, PlUserClavesCatastrales> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    PlUserClavesCatastralesDTO toDto(PlUserClavesCatastrales plUserClavesCatastrales); 

    @Mapping(source = "userId", target = "user")
    PlUserClavesCatastrales toEntity(PlUserClavesCatastralesDTO plUserClavesCatastralesDTO);

    default PlUserClavesCatastrales fromId(Long id) {
        if (id == null) {
            return null;
        }
        PlUserClavesCatastrales plUserClavesCatastrales = new PlUserClavesCatastrales();
        plUserClavesCatastrales.setId(id);
        return plUserClavesCatastrales;
    }
}
