package com.worknest.pagobancomer.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import javax.validation.constraints.NotNull;

public class LiquidacionDesConceptoDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

    @NotNull
    private Integer orden;

    @NotNull
    private BigDecimal importe;

    private Long liquidacionId;

    private String liquidacionNumeroLiquidacion;

    private Long conceptoId;

    private String conceptoDescripcion;
    
    private String claveConcepto;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public Long getLiquidacionId() {
        return liquidacionId;
    }

    public void setLiquidacionId(Long liquidacionId) {
        this.liquidacionId = liquidacionId;
    }

    public String getLiquidacionNumeroLiquidacion() {
        return liquidacionNumeroLiquidacion;
    }

    public void setLiquidacionNumeroLiquidacion(String liquidacionNumeroLiquidacion) {
        this.liquidacionNumeroLiquidacion = liquidacionNumeroLiquidacion;
    }

    public Long getConceptoId() {
        return conceptoId;
    }

    public void setConceptoId(Long catConceptoId) {
        this.conceptoId = catConceptoId;
    }

    public String getConceptoDescripcion() {
        return conceptoDescripcion;
    }

    public void setConceptoDescripcion(String catConceptoDescripcion) {
        this.conceptoDescripcion = catConceptoDescripcion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LiquidacionDesConceptoDTO liquidacionDesConceptoDTO = (LiquidacionDesConceptoDTO) o;
        if(liquidacionDesConceptoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), liquidacionDesConceptoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LiquidacionDesConceptoDTO{" +
            "id=" + getId() +
            ", orden='" + getOrden() + "'" +
            ", importe='" + getImporte() + "'" +
            "}";
    }

	public String getClaveConcepto() {
		return claveConcepto;
	}

	public void setClaveConcepto(String claveConcepto) {
		this.claveConcepto = claveConcepto;
	}
}
