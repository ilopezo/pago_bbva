package com.worknest.pagobancomer.service;

import java.math.BigDecimal;

import com.worknest.pagobancomer.domain.PlIntentoPago;

public interface CorreosService {

    public Boolean enviarCorreo(PlIntentoPago intentoPago, BigDecimal importe, String nombre, String authCode,
        String textoConceptos, String link, String ruta, String correo, String urlBase);
}
