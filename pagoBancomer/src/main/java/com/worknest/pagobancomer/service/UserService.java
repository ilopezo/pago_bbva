package com.worknest.pagobancomer.service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.worknest.pagobancomer.config.Constants;
import com.worknest.pagobancomer.domain.Authority;
import com.worknest.pagobancomer.domain.User;
import com.worknest.pagobancomer.repository.AuthorityRepository;
import com.worknest.pagobancomer.repository.PersistentTokenRepository;
import com.worknest.pagobancomer.repository.UserRepository;
import com.worknest.pagobancomer.security.AuthoritiesConstants;
import com.worknest.pagobancomer.security.SecurityUtils;
import com.worknest.pagobancomer.service.dto.AuthenticateDTO;
import com.worknest.pagobancomer.service.dto.PlCarroDTO;
import com.worknest.pagobancomer.service.dto.TokenDTO;
import com.worknest.pagobancomer.service.dto.UserDTO;
import com.worknest.pagobancomer.service.mapper.UserMapper;
import com.worknest.pagobancomer.service.util.RandomUtil;
import com.worknest.pagobancomer.service.vo.CatalogoDirVO;
import com.worknest.pagobancomer.service.vo.DatosFacturacionVO;
import com.worknest.pagobancomer.service.vo.DireccionVO;
import com.worknest.pagobancomer.service.vo.EditarContrasenaVO;
import com.worknest.pagobancomer.service.vo.EmailVO;
import com.worknest.pagobancomer.service.vo.PersonaVO;
import com.worknest.pagobancomer.service.vo.RelPersonaDireccionVO;
import com.worknest.pagobancomer.service.vo.RelPersonaEmailVO;
import com.worknest.pagobancomer.service.vo.RelPersonaTelVO;
import com.worknest.pagobancomer.service.vo.PerfilVO;
import com.worknest.pagobancomer.web.rest.errors.ExceptionAPI;
import com.worknest.pagobancomer.web.rest.vm.ManagedUserVM;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private static final String USERS_CACHE = "users";

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final PersistentTokenRepository persistentTokenRepository;

    private final AuthorityRepository authorityRepository;

    private final CacheManager cacheManager;
    
    private final PlCarroService plCarroService;
    
    private String _token;
    
    @Value("${application.autenticate}") private String _autenticate;
    @Value("${application.usuario}") private String _usuario;
    @Value("${application.pass}") private String _pass;
    @Value("${application.urlusuario}") private String _urlusuario;
    @Value("${application.account}") private String _account;
    @Value("${application.infoperfil}") private String _infoperfil;
    @Value("${application.modiperfil}") private String _modiperfil;
    
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, PersistentTokenRepository persistentTokenRepository, 
            AuthorityRepository authorityRepository, CacheManager cacheManager, PlCarroService plCarroRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.persistentTokenRepository = persistentTokenRepository;
        this.authorityRepository = authorityRepository;
        this.cacheManager = cacheManager;
        this.plCarroService = plCarroRepository;
    }

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        return userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                user.setActivationKey(null);
                cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
                log.debug("Activated user: {}", user);
                return user;
            });
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
       log.debug("Reset user password for reset key {}", key);

       return userRepository.findOneByResetKey(key)
           .filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400)))
           .map(user -> {
                user.setPassword(passwordEncoder.encode(newPassword));
                user.setResetKey(null);
                user.setResetDate(null);
                cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
                return user;
           });
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository.findOneByEmailIgnoreCase(mail)
            .filter(User::getActivated)
            .map(user -> {
                user.setResetKey(RandomUtil.generateResetKey());
                user.setResetDate(Instant.now());
                cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
                return user;
            });
    }

    public User registerUser(ManagedUserVM userDTO) {

        User newUser = new User();
        Authority authority = authorityRepository.findOne(AuthoritiesConstants.USER);
        Set<Authority> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(userDTO.getPassword());
        newUser.setLogin(userDTO.getLogin());
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(userDTO.getFirstName());
        newUser.setLastName(userDTO.getLastName());
        newUser.setEmail(userDTO.getEmail());
        newUser.setImageUrl(userDTO.getImageUrl());
        newUser.setLangKey(userDTO.getLangKey());
        // new user is not active
        newUser.setActivated(true);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        authorities.add(authority);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        
        // crea un carro al usuario creado
        log.debug("Id usuario creado"+ newUser.getId());
        PlCarroDTO plCarroDTO = new PlCarroDTO(LocalDate.now(), newUser.getId());
        plCarroService.save(plCarroDTO);
        
        return newUser;
    }

    public User createUser(UserDTO userDTO) {
        User user = new User();
        user.setLogin(userDTO.getLogin());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
        user.setImageUrl(userDTO.getImageUrl());
        if (userDTO.getLangKey() == null) {
            user.setLangKey(Constants.DEFAULT_LANGUAGE); // default language
        } else {
            user.setLangKey(userDTO.getLangKey());
        }
        if (userDTO.getAuthorities() != null) {
            Set<Authority> authorities = userDTO.getAuthorities().stream()
                .map(authorityRepository::findOne)
                .collect(Collectors.toSet());
            user.setAuthorities(authorities);
        }
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(Instant.now());
        user.setActivated(true);
        userRepository.save(user);
        log.debug("Created Information for User: {}", user);
        
        // crea un carro al usuario creado
        log.debug("Id usuario creado"+ user.getId());
        PlCarroDTO plCarroDTO = new PlCarroDTO(LocalDate.now(), user.getId());
        plCarroService.save(plCarroDTO);
        
        return user;
    }

    /**
     * Update basic information (first name, last name, email, language) for the current user.
     *
     * @param firstName first name of user
     * @param lastName last name of user
     * @param email email id of user
     * @param langKey language key
     * @param imageUrl image URL of user
     */
    public void updateUser(String firstName, String lastName, String email, String langKey, String imageUrl) {
        userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(user -> {
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmail(email);
            user.setLangKey(langKey);
            user.setImageUrl(imageUrl);
            cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
            log.debug("Changed Information for User: {}", user);
        });
    }

    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param userDTO user to update
     * @return updated user
     */
    public Optional<UserDTO> updateUser(UserDTO userDTO) {
        return Optional.of(userRepository
            .findOne(userDTO.getId()))
            .map(user -> {
                user.setLogin(userDTO.getLogin());
                user.setFirstName(userDTO.getFirstName());
                user.setLastName(userDTO.getLastName());
                user.setEmail(userDTO.getEmail());
                user.setImageUrl(userDTO.getImageUrl());
                user.setActivated(userDTO.isActivated());
                user.setLangKey(userDTO.getLangKey());
                Set<Authority> managedAuthorities = user.getAuthorities();
                managedAuthorities.clear();
                userDTO.getAuthorities().stream()
                    .map(authorityRepository::findOne)
                    .forEach(managedAuthorities::add);
                cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
                log.debug("Changed Information for User: {}", user);
                return user;
            })
            .map(UserDTO::new);
    }

    public void deleteUser(String login) {
        userRepository.findOneByLogin(login).ifPresent(user -> {
            userRepository.delete(user);
            cacheManager.getCache(USERS_CACHE).evict(login);
            log.debug("Deleted User: {}", user);
        });
    }

    public void changePassword(String password) {
        userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(user -> {
            String encryptedPassword = passwordEncoder.encode(password);
            user.setPassword(encryptedPassword);
            cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
            log.debug("Changed password for User: {}", user);
        });
    }

    @Transactional(readOnly = true)
    public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
        return userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(UserDTO::new);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneWithAuthoritiesByLogin(login);
    }

    @Transactional(readOnly = true)
    public User getUserWithAuthorities(Long id) {
        return userRepository.findOneWithAuthoritiesById(id);
    }

    @Transactional(readOnly = true)
    public User getUserWithAuthorities() {
       
        this._token = this.generaToken(this._usuario, this._pass).getId_token();
        // Se consulta el WS que genera el token
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + this._token);

        //Genera la url para consumir el WS que busque la informacion del usuario
        String uri = new String(this._urlusuario + this._account);
        log.debug("----------------------------------"+SecurityUtils.getCurrentUserLogin());

        HttpEntity<?> entity = new HttpEntity<String>(SecurityUtils.getCurrentUserLogin(), headers);
        // El WS genera las liquidaciones en base a los datos de envio
        
        ResponseEntity<User> usuario = restTemplate.exchange(uri, HttpMethod.POST, entity, User.class);
        
        User user = usuario.getBody();
        log.debug("----------------------------------"+user.toString());
        return user;
        //return userRepository.findOneWithAuthoritiesByLogin(SecurityUtils.getCurrentUserLogin()).orElse(null);
    }

    /**
     * Persistent Token are used for providing automatic authentication, they should be automatically deleted after
     * 30 days.
     * <p>
     * This is scheduled to get fired everyday, at midnight.
     */
    @Scheduled(cron = "0 0 0 * * ?")
    public void removeOldPersistentTokens() {
        LocalDate now = LocalDate.now();
        persistentTokenRepository.findByTokenDateBefore(now.minusMonths(1)).forEach(token -> {
            log.debug("Deleting token {}", token.getSeries());
            User user = token.getUser();
            user.getPersistentTokens().remove(token);
            persistentTokenRepository.delete(token);
        });
    }

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        List<User> users = userRepository.findAllByActivatedIsFalseAndCreatedDateBefore(Instant.now().minus(3, ChronoUnit.DAYS));
        for (User user : users) {
            log.debug("Deleting not activated user {}", user.getLogin());
            userRepository.delete(user);
            cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
        }
    }

    /**
     * @return a list of all the authorities
     */
    public List<String> getAuthorities() {
        return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
    }

	public User getUserbyLogin(String login) {
		return (User) userRepository.getUserByLogin(login);
	}

    /**
     * Genera token para comunicacion con WS acapulco-caja
     * @param usuario usuario para generar token 
     * @param pass clave de acceso para generar token
     * @return token obtenido
     */
    public TokenDTO generaToken(String usuario, String pass) {

        // se definie las credenciales FIJAS
        AuthenticateDTO auth = new AuthenticateDTO();

        auth.setUsername(usuario); // user
        auth.setRememberMe(true);
        auth.setPassword(pass);    // user

        // se definie los headers
        Map<String, String> vars = new HashMap<String, String>();
        vars.put("Content-Type", "application/json");

        // se consulta el ws que genera el token
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        String uri = new String(this._urlusuario + this._autenticate);

        TokenDTO token = new TokenDTO();

        token = restTemplate.postForObject(uri, auth, TokenDTO.class, vars);

        // regresa el token
        return token;
    }
    
    /**
     * Metodo para obtener la informacion del perfil del usuario del sistema de usuarios
     * @return
     * @throws ExceptionAPI
     * @throws Exception 
     */
    public Map<String, Object> informacionPerfil()throws ExceptionAPI, Exception{
                
        this._token = this.generaToken(this._usuario, this._pass).getId_token();
        
        // Se consulta el WS que genera el token
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + this._token);

        //Genera la url para consumir el WS que busque la informacion del usuario
        String uri = new String(this._urlusuario + this._infoperfil +"/"+SecurityUtils.getCurrentUserLogin());

        HttpEntity<?> entity = new HttpEntity<String>(SecurityUtils.getCurrentUserLogin(), headers);
        // El WS genera las liquidaciones en base a los datos de envio
        
        ResponseEntity<Map<String,Object>> infoUsuario = restTemplate.exchange(uri, HttpMethod.GET, entity,
                                    new ParameterizedTypeReference<Map<String,Object>>() {});
        
        Map<String, Object> user = infoUsuario.getBody();
        return user;
    }
    /**
     * Metodo para actualizar la contraseña del usuario
     * @param contrasena
     * @return 
     * @throws com.worknest.pagobancomer.web.rest.errors.ExceptionAPI
     * @throws java.lang.Exception
     */
    public Map<String, Object> actualizarContrasena(EditarContrasenaVO contrasena) throws ExceptionAPI, Exception {
        this._token = this.generaToken(this._usuario, this._pass).getId_token();
        
        // Se consulta el WS que genera el token
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + this._token);

        //Genera la url para consumir el WS que modifique la contraseña del usuario
        String uri = new String(this._urlusuario + "/api/usuarios/actualizar-contrasena");

        HttpEntity<EditarContrasenaVO> entity = new HttpEntity<EditarContrasenaVO>(contrasena, headers);
        // El WS genera las liquidaciones en base a los datos de envio
        
        ResponseEntity<Map<String,Object>> infoUsuario = restTemplate.exchange(uri, HttpMethod.PUT, entity,
                                    new ParameterizedTypeReference<Map<String,Object>>() {});
        
        return infoUsuario.getBody();
    }
    
    public Map<String, Object> modificarPerfil(PerfilVO perfil) throws ExceptionAPI, Exception {
        this._token = this.generaToken(this._usuario, this._pass).getId_token();
        
        // Se consulta el WS que genera el token
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + this._token);

        //Genera la url para consumir el WS que modifique la contraseña del usuario
        String uri = new String(this._urlusuario + _modiperfil);

        HttpEntity<PerfilVO> entity = new HttpEntity<PerfilVO>(perfil, headers);
        // El WS genera las liquidaciones en base a los datos de envio
        
        ResponseEntity<Map<String,Object>> infoUsuario = restTemplate.exchange(uri, HttpMethod.PUT, entity,
                                    new ParameterizedTypeReference<Map<String,Object>>() {});
        
        return infoUsuario.getBody();
    }
}
