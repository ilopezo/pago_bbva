package com.worknest.pagobancomer.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.worknest.pagobancomer.domain.PlCarroDet;
import com.worknest.pagobancomer.service.dto.PlCarroDetDTO;

/**
 * Mapper for the entity PlCarroDet and its DTO PlCarroDetDTO.
 */
@Mapper(componentModel = "spring", uses = {PlCarroMapper.class, LiquidacionMapper.class})
public interface PlCarroDetMapper extends EntityMapper<PlCarroDetDTO, PlCarroDet> {

    @Mapping(source = "carro.id", target = "carroId")
    @Mapping(source = "liquidacion.id", target = "liquidacionId")
    PlCarroDetDTO toDto(PlCarroDet plCarroDet); 

    @Mapping(source = "carroId", target = "carro")
    @Mapping(source = "liquidacionId", target = "liquidacion")
    PlCarroDet toEntity(PlCarroDetDTO plCarroDetDTO);

    default PlCarroDet fromId(Long id) {
        if (id == null) {
            return null;
        }
        PlCarroDet plCarroDet = new PlCarroDet();
        plCarroDet.setId(id);
        return plCarroDet;
    }
}
