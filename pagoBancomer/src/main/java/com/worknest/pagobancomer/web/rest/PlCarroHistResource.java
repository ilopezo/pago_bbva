package com.worknest.pagobancomer.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.worknest.pagobancomer.service.PlCarroHistService;
import com.worknest.pagobancomer.service.dto.PlCarroHistDTO;
import com.worknest.pagobancomer.web.rest.errors.BadRequestAlertException;
import com.worknest.pagobancomer.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing PlCarroHist.
 */
@RestController
@RequestMapping("/api")
public class PlCarroHistResource {

    private final Logger log = LoggerFactory.getLogger(PlCarroHistResource.class);

    private static final String ENTITY_NAME = "plCarroHist";

    private final PlCarroHistService plCarroHistService;

    public PlCarroHistResource(PlCarroHistService plCarroHistService) {
        this.plCarroHistService = plCarroHistService;
    }

    /**
     * POST  /pl-carro-hists : Create a new plCarroHist.
     *
     * @param plCarroHistDTO the plCarroHistDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new plCarroHistDTO, or with status 400 (Bad Request) if the plCarroHist has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pl-carro-hists")
    @Timed
    public ResponseEntity<PlCarroHistDTO> createPlCarroHist(@Valid @RequestBody PlCarroHistDTO plCarroHistDTO) throws URISyntaxException {
        log.debug("REST request to save PlCarroHist : {}", plCarroHistDTO);
        if (plCarroHistDTO.getId() != null) {
            throw new BadRequestAlertException("A new plCarroHist cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlCarroHistDTO result = plCarroHistService.save(plCarroHistDTO);
        return ResponseEntity.created(new URI("/api/pl-carro-hists/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pl-carro-hists : Updates an existing plCarroHist.
     *
     * @param plCarroHistDTO the plCarroHistDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated plCarroHistDTO,
     * or with status 400 (Bad Request) if the plCarroHistDTO is not valid,
     * or with status 500 (Internal Server Error) if the plCarroHistDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pl-carro-hists")
    @Timed
    public ResponseEntity<PlCarroHistDTO> updatePlCarroHist(@Valid @RequestBody PlCarroHistDTO plCarroHistDTO) throws URISyntaxException {
        log.debug("REST request to update PlCarroHist : {}", plCarroHistDTO);
        if (plCarroHistDTO.getId() == null) {
            return createPlCarroHist(plCarroHistDTO);
        }
        PlCarroHistDTO result = plCarroHistService.save(plCarroHistDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, plCarroHistDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pl-carro-hists : get all the plCarroHists.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of plCarroHists in body
     */
    @GetMapping("/pl-carro-hists")
    @Timed
    public List<PlCarroHistDTO> getAllPlCarroHists() {
        log.debug("REST request to get all PlCarroHists");
        return plCarroHistService.findAll();
        }

    /**
     * GET  /pl-carro-hists/:id : get the "id" plCarroHist.
     *
     * @param id the id of the plCarroHistDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the plCarroHistDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pl-carro-hists/{id}")
    @Timed
    public ResponseEntity<PlCarroHistDTO> getPlCarroHist(@PathVariable Long id) {
        log.debug("REST request to get PlCarroHist : {}", id);
        PlCarroHistDTO plCarroHistDTO = plCarroHistService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(plCarroHistDTO));
    }

    /**
     * DELETE  /pl-carro-hists/:id : delete the "id" plCarroHist.
     *
     * @param id the id of the plCarroHistDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pl-carro-hists/{id}")
    @Timed
    public ResponseEntity<Void> deletePlCarroHist(@PathVariable Long id) {
        log.debug("REST request to delete PlCarroHist : {}", id);
        plCarroHistService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
