package com.worknest.pagobancomer.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.worknest.pagobancomer.service.PlParamEnvioService;
import com.worknest.pagobancomer.service.dto.PlParamEnvioDTO;
import com.worknest.pagobancomer.web.rest.errors.BadRequestAlertException;
import com.worknest.pagobancomer.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing PlParamEnvio.
 */
@RestController
@RequestMapping("/api")
public class PlParamEnvioResource {

    private final Logger log = LoggerFactory.getLogger(PlParamEnvioResource.class);

    private static final String ENTITY_NAME = "plParamEnvio";

    private final PlParamEnvioService plParamEnvioService;

    public PlParamEnvioResource(PlParamEnvioService plParamEnvioService) {
        this.plParamEnvioService = plParamEnvioService;
    }

    /**
     * POST  /pl-param-envios : Create a new plParamEnvio.
     *
     * @param plParamEnvioDTO the plParamEnvioDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new plParamEnvioDTO, or with status 400 (Bad Request) if the plParamEnvio has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pl-param-envios")
    @Timed
    public ResponseEntity<PlParamEnvioDTO> createPlParamEnvio(@Valid @RequestBody PlParamEnvioDTO plParamEnvioDTO) throws URISyntaxException {
        log.debug("REST request to save PlParamEnvio : {}", plParamEnvioDTO);
        if (plParamEnvioDTO.getId() != null) {
            throw new BadRequestAlertException("A new plParamEnvio cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlParamEnvioDTO result = plParamEnvioService.save(plParamEnvioDTO);
        return ResponseEntity.created(new URI("/api/pl-param-envios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pl-param-envios : Updates an existing plParamEnvio.
     *
     * @param plParamEnvioDTO the plParamEnvioDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated plParamEnvioDTO,
     * or with status 400 (Bad Request) if the plParamEnvioDTO is not valid,
     * or with status 500 (Internal Server Error) if the plParamEnvioDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pl-param-envios")
    @Timed
    public ResponseEntity<PlParamEnvioDTO> updatePlParamEnvio(@Valid @RequestBody PlParamEnvioDTO plParamEnvioDTO) throws URISyntaxException {
        log.debug("REST request to update PlParamEnvio : {}", plParamEnvioDTO);
        if (plParamEnvioDTO.getId() == null) {
            return createPlParamEnvio(plParamEnvioDTO);
        }
        PlParamEnvioDTO result = plParamEnvioService.save(plParamEnvioDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, plParamEnvioDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pl-param-envios : get all the plParamEnvios.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of plParamEnvios in body
     */
    @GetMapping("/pl-param-envios")
    @Timed
    public List<PlParamEnvioDTO> getAllPlParamEnvios() {
        log.debug("REST request to get all PlParamEnvios");
        return plParamEnvioService.findAll();
        }

    /**
     * GET  /pl-param-envios/:id : get the "id" plParamEnvio.
     *
     * @param id the id of the plParamEnvioDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the plParamEnvioDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pl-param-envios/{id}")
    @Timed
    public ResponseEntity<PlParamEnvioDTO> getPlParamEnvio(@PathVariable Long id) {
        log.debug("REST request to get PlParamEnvio : {}", id);
        PlParamEnvioDTO plParamEnvioDTO = plParamEnvioService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(plParamEnvioDTO));
    }

    /**
     * DELETE  /pl-param-envios/:id : delete the "id" plParamEnvio.
     *
     * @param id the id of the plParamEnvioDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pl-param-envios/{id}")
    @Timed
    public ResponseEntity<Void> deletePlParamEnvio(@PathVariable Long id) {
        log.debug("REST request to delete PlParamEnvio : {}", id);
        plParamEnvioService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
