package com.worknest.pagobancomer.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.worknest.pagobancomer.service.PlUserClavesCatastralesService;
import com.worknest.pagobancomer.service.dto.PlUserClavesCatastralesDTO;
import com.worknest.pagobancomer.web.rest.errors.BadRequestAlertException;
import com.worknest.pagobancomer.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing PlUserClavesCatastrales.
 */
@RestController
@RequestMapping("/api")
public class PlUserClavesCatastralesResource {

    private final Logger log = LoggerFactory.getLogger(PlUserClavesCatastralesResource.class);

    private static final String ENTITY_NAME = "plUserClavesCatastrales";

    private final PlUserClavesCatastralesService plUserClavesCatastralesService;

    public PlUserClavesCatastralesResource(PlUserClavesCatastralesService plUserClavesCatastralesService) {
        this.plUserClavesCatastralesService = plUserClavesCatastralesService;
    }

    /**
     * POST  /pl-user-claves-catastrales : Create a new plUserClavesCatastrales.
     *
     * @param plUserClavesCatastralesDTO the plUserClavesCatastralesDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new plUserClavesCatastralesDTO, or with status 400 (Bad Request) if the plUserClavesCatastrales has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pl-user-claves-catastrales")
    @Timed
    public ResponseEntity<PlUserClavesCatastralesDTO> createPlUserClavesCatastrales(@Valid @RequestBody PlUserClavesCatastralesDTO plUserClavesCatastralesDTO) throws URISyntaxException {
        log.debug("REST request to save PlUserClavesCatastrales : {}", plUserClavesCatastralesDTO);
        if (plUserClavesCatastralesDTO.getId() != null) {
            throw new BadRequestAlertException("A new plUserClavesCatastrales cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlUserClavesCatastralesDTO result = plUserClavesCatastralesService.save(plUserClavesCatastralesDTO);
        return ResponseEntity.created(new URI("/api/pl-user-claves-catastrales/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pl-user-claves-catastrales : Updates an existing plUserClavesCatastrales.
     *
     * @param plUserClavesCatastralesDTO the plUserClavesCatastralesDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated plUserClavesCatastralesDTO,
     * or with status 400 (Bad Request) if the plUserClavesCatastralesDTO is not valid,
     * or with status 500 (Internal Server Error) if the plUserClavesCatastralesDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pl-user-claves-catastrales")
    @Timed
    public ResponseEntity<PlUserClavesCatastralesDTO> updatePlUserClavesCatastrales(@Valid @RequestBody PlUserClavesCatastralesDTO plUserClavesCatastralesDTO) throws URISyntaxException {
        log.debug("REST request to update PlUserClavesCatastrales : {}", plUserClavesCatastralesDTO);
        if (plUserClavesCatastralesDTO.getId() == null) {
            return createPlUserClavesCatastrales(plUserClavesCatastralesDTO);
        }
        PlUserClavesCatastralesDTO result = plUserClavesCatastralesService.save(plUserClavesCatastralesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, plUserClavesCatastralesDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pl-user-claves-catastrales : get all the plUserClavesCatastrales.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of plUserClavesCatastrales in body
     */
    @GetMapping("/pl-user-claves-catastrales")
    @Timed
    public List<PlUserClavesCatastralesDTO> getAllPlUserClavesCatastrales() {
        log.debug("REST request to get all PlUserClavesCatastrales");
        return plUserClavesCatastralesService.findAll();
        }

    /**
     * GET  /pl-user-claves-catastrales/:id : get the "id" plUserClavesCatastrales.
     *
     * @param id the id of the plUserClavesCatastralesDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the plUserClavesCatastralesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pl-user-claves-catastrales/{id}")
    @Timed
    public ResponseEntity<PlUserClavesCatastralesDTO> getPlUserClavesCatastrales(@PathVariable Long id) {
        log.debug("REST request to get PlUserClavesCatastrales : {}", id);
        PlUserClavesCatastralesDTO plUserClavesCatastralesDTO = plUserClavesCatastralesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(plUserClavesCatastralesDTO));
    }

    /**
     * DELETE  /pl-user-claves-catastrales/:id : delete the "id" plUserClavesCatastrales.
     *
     * @param id the id of the plUserClavesCatastralesDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pl-user-claves-catastrales/{id}")
    @Timed
    public ResponseEntity<Void> deletePlUserClavesCatastrales(@PathVariable Long id) {
        log.debug("REST request to delete PlUserClavesCatastrales : {}", id);
        plUserClavesCatastralesService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
