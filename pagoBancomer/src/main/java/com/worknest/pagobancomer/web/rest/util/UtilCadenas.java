package com.worknest.pagobancomer.web.rest.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class UtilCadenas {

    /**
     * Metodo para verificar el formato numerico en un string
     * @param numeroCadena
     * @return 
     */
    public static boolean convertirCadenaAInteger(String numeroCadena) {
        try {
            Integer.parseInt(numeroCadena);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
    /**
     * Metodo para verificar el formato decimal en un string
     * @param numeroCadena
     * @return 
     */
    public static boolean convertirCadenaAFlotante(String numeroCadena) {
        try {
            Float.parseFloat(numeroCadena);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
    /**
     * Metodo para verificar el formato contable ne un string
     * @param importe
     * @param simbolo
     * @return 
     */
    public static Object formatoContable(BigDecimal importe, boolean simbolo) {
        if(importe == null )
            importe= BigDecimal.ZERO;
        return simbolo ? new DecimalFormat("#,##0.00").format(importe.doubleValue()) : DecimalFormat.getCurrencyInstance().format(importe.doubleValue());
    }
}
