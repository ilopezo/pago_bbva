package com.worknest.pagobancomer.web.rest;

import com.worknest.pagobancomer.service.PlCarroDetHistService;
import com.worknest.pagobancomer.service.PlCarroHistService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;

import com.codahale.metrics.annotation.Timed;
import com.worknest.pagobancomer.service.PlIntentoPagoService;
import com.worknest.pagobancomer.service.dto.PlIntentoPagoDTO;
import com.worknest.pagobancomer.web.rest.errors.BadRequestAlertException;
import com.worknest.pagobancomer.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing PlIntentoPago.
 */
@RestController
@RequestMapping("/api")
public class PlIntentoPagoResource {

    private final Logger log = LoggerFactory.getLogger(PlIntentoPagoResource.class);

    private static final String ENTITY_NAME = "plIntentoPago";

    private final PlIntentoPagoService plIntentoPagoService;
    
    @Autowired
    private PlCarroHistService plCarroHistService;
    
    @Autowired
    private PlCarroDetHistService plCarroDetHistService;

    public PlIntentoPagoResource(PlIntentoPagoService plIntentoPagoService) {
        this.plIntentoPagoService = plIntentoPagoService;
    }

    /**
     * POST  /pl-intento-pagos : Create a new plIntentoPago.
     *
     * @param plIntentoPagoDTO the plIntentoPagoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new plIntentoPagoDTO, or with status 400 (Bad Request) if the plIntentoPago has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pl-intento-pagos")
    @Timed
    public ResponseEntity<PlIntentoPagoDTO> createPlIntentoPago(@Valid @RequestBody PlIntentoPagoDTO plIntentoPagoDTO) throws URISyntaxException {
        log.debug("REST request to save PlIntentoPago : {}", plIntentoPagoDTO);
        if (plIntentoPagoDTO.getId() != null) {
            throw new BadRequestAlertException("A new plIntentoPago cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlIntentoPagoDTO result = plIntentoPagoService.save(plIntentoPagoDTO);
        return ResponseEntity.created(new URI("/api/pl-intento-pagos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pl-intento-pagos : Updates an existing plIntentoPago.
     *
     * @param plIntentoPagoDTO the plIntentoPagoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated plIntentoPagoDTO,
     * or with status 400 (Bad Request) if the plIntentoPagoDTO is not valid,
     * or with status 500 (Internal Server Error) if the plIntentoPagoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pl-intento-pagos")
    @Timed
    public ResponseEntity<PlIntentoPagoDTO> updatePlIntentoPago(@Valid @RequestBody PlIntentoPagoDTO plIntentoPagoDTO) throws URISyntaxException {
        log.debug("REST request to update PlIntentoPago : {}", plIntentoPagoDTO);
        if (plIntentoPagoDTO.getId() == null) {
            return createPlIntentoPago(plIntentoPagoDTO);
        }
        PlIntentoPagoDTO result = plIntentoPagoService.save(plIntentoPagoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, plIntentoPagoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pl-intento-pagos : get all the plIntentoPagos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of plIntentoPagos in body
     */
    @GetMapping("/pl-intento-pagos")
    @Timed
    public List<PlIntentoPagoDTO> getAllPlIntentoPagos() {
        log.debug("REST request to get all PlIntentoPagos");
        return plIntentoPagoService.findAll();
        }

    /**
     * GET  /pl-intento-pagos/:id : get the "id" plIntentoPago.
     *
     * @param id the id of the plIntentoPagoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the plIntentoPagoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pl-intento-pagos/{id}")
    @Timed
    public ResponseEntity<PlIntentoPagoDTO> getPlIntentoPago(@PathVariable Long id) {
        log.debug("REST request to get PlIntentoPago : {}", id);
        PlIntentoPagoDTO plIntentoPagoDTO = plIntentoPagoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(plIntentoPagoDTO));
    }

    /**
     * DELETE  /pl-intento-pagos/:id : delete the "id" plIntentoPago.
     *
     * @param id the id of the plIntentoPagoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pl-intento-pagos/{id}")
    @Timed
    public ResponseEntity<Void> deletePlIntentoPago(@PathVariable Long id) {
        log.debug("REST request to delete PlIntentoPago : {}", id);
        plIntentoPagoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
