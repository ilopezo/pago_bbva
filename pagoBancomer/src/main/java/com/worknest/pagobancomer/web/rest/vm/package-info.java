/**
 * View Models used by Spring MVC REST controllers.
 */
package com.worknest.pagobancomer.web.rest.vm;
