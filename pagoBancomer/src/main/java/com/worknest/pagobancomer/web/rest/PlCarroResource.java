package com.worknest.pagobancomer.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.codahale.metrics.annotation.Timed;
import com.worknest.pagobancomer.service.PlCarroDetHistService;
import com.worknest.pagobancomer.service.PlCarroDetService;
import com.worknest.pagobancomer.service.PlCarroHistService;
import com.worknest.pagobancomer.service.PlCarroService;
import com.worknest.pagobancomer.service.PlIntentoPagoService;
import com.worknest.pagobancomer.service.dto.PlCarroDTO;
import com.worknest.pagobancomer.service.dto.PlCarroDetDTO;
import com.worknest.pagobancomer.service.dto.PlCarroHistDTO;
import com.worknest.pagobancomer.web.rest.errors.BadRequestAlertException;
import com.worknest.pagobancomer.web.rest.errors.ExceptionAPI;
import com.worknest.pagobancomer.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;
import java.util.HashMap;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * REST controller for managing PlCarro.
 */
@RestController
@CrossOrigin(origins="*", maxAge=100000)
@RequestMapping("/api")
public class PlCarroResource {

    private final Logger log = LoggerFactory.getLogger(PlCarroResource.class);

    private static final String ENTITY_NAME = "plCarro";

    private final PlCarroService plCarroService;

    @Autowired
    private PlCarroDetService servicioCarroDet;

    @Autowired
    private PlCarroHistService servicioCarroHist;

    @Autowired
    private PlCarroDetHistService servicioCarroDetHist;

    @Autowired
    private PlIntentoPagoService servicioIntentoPago;

    public PlCarroResource(PlCarroService plCarroService) {
            this.plCarroService = plCarroService;
    }

    /**
     * POST /pl-carros : Create a new plCarro.
     *
     * @param plCarroDTO
     *            the plCarroDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new
     *         plCarroDTO, or with status 400 (Bad Request) if the plCarro has
     *         already an ID
     * @throws URISyntaxException
     *             if the Location URI syntax is incorrect
     */
    @PostMapping("/pl-carros")
    @Timed
    public ResponseEntity<PlCarroDTO> createPlCarro(@Valid @RequestBody PlCarroDTO plCarroDTO)
                    throws URISyntaxException {
            log.debug("REST request to save PlCarro : {}", plCarroDTO);
            if (plCarroDTO.getId() != null) {
                    throw new BadRequestAlertException("A new plCarro cannot already have an ID", ENTITY_NAME, "idexists");
            }
            PlCarroDTO result = plCarroService.save(plCarroDTO);
            return ResponseEntity.created(new URI("/api/pl-carros/" + result.getId()))
                            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT /pl-carros : Updates an existing plCarro.
     *
     * @param plCarroDTO
     *            the plCarroDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated
     *         plCarroDTO, or with status 400 (Bad Request) if the plCarroDTO is not
     *         valid, or with status 500 (Internal Server Error) if the plCarroDTO
     *         couldn't be updated
     * @throws URISyntaxException
     *             if the Location URI syntax is incorrect
     */
    @PutMapping("/pl-carros")
    @Timed
    public ResponseEntity<PlCarroDTO> updatePlCarro(@Valid @RequestBody PlCarroDTO plCarroDTO)
                    throws URISyntaxException {
            log.debug("REST request to update PlCarro : {}", plCarroDTO);
            if (plCarroDTO.getId() == null) {
                    return createPlCarro(plCarroDTO);
            }
            PlCarroDTO result = plCarroService.save(plCarroDTO);
            return ResponseEntity.ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, plCarroDTO.getId().toString())).body(result);
    }

    /**
     * GET /pl-carros : get all the plCarros.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of plCarros in
     *         body
     */
    @GetMapping("/pl-carros")
    @Timed
    public List<PlCarroDTO> getAllPlCarros() {
            log.debug("REST request to get all PlCarros");
            return plCarroService.findAll();
    }

    /**
     * GET /pl-carros/:id : get the "id" plCarro.
     *
     * @param id
     *            the id of the plCarroDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the plCarroDTO,
     *         or with status 404 (Not Found)
     */
    @GetMapping("/pl-carros/{id}")
    @Timed
    public ResponseEntity<PlCarroDTO> getPlCarro(@PathVariable Long id) {
            log.debug("REST request to get PlCarro : {}", id);
            PlCarroDTO plCarroDTO = plCarroService.findOne(id);
            return ResponseUtil.wrapOrNotFound(Optional.ofNullable(plCarroDTO));
    }

    /**
     * DELETE /pl-carros/:id : delete the "id" plCarro.
     *
     * @param id
     *            the id of the plCarroDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pl-carros/{id}")
    @Timed
    public ResponseEntity<Void> deletePlCarro(@PathVariable Long id) {
        log.debug("REST request to delete PlCarro : {}", id);
        plCarroService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * POST /realizar-pago : Metodo para que pasa los conceptos del carroDet al carroDetHist, crea un
     * carroHist y un intento de pago
     * @return ResponseEntiti con status 200 (OK)
     * @throws Exception  responseEntity con status 400 y mensaje de error
     */
    @PostMapping("/realizar-pago")
    @Timed
    public ModelAndView realizarPago() throws Exception {	

        ModelAndView modelAndView = new ModelAndView("form");

        try {
            // obtener carro segun usuario
            PlCarroDTO carro = this.plCarroService.getCarro();
            // se genera una lista de los conceptos que contiene el carro del usuario
            List<PlCarroDetDTO> listaCarro = this.servicioCarroDet.buscarPorCarro(carro.getId());

            // se crea un nuevo carroHist en base al carro del usuario
            PlCarroHistDTO carroHist = this.servicioCarroHist.crearCarroHist(carro.getId());

            // se guardan los conceptos del carro en el CarroDetHist
            carroHist=this.servicioCarroDetHist.guardarConceptos(listaCarro, carroHist);
            log.debug("----------------------------------------id carro" + carroHist.getCarroId());

            // se genera un nuevo intento de pago y devuelve los parametros de envio para le banco
            Map<String, String> paramEnvio = this.servicioIntentoPago.crearIntentoPago(carroHist);

            // prepara la vista 
            modelAndView.addObject("map", paramEnvio);

        } catch (ExceptionAPI e) { 
            // si se obtiene alguna exception en el proceso, se genera una respuesta con el mensaje de error correspondiente
            // modelAndView = new ModelAndView(e.getMensaje());
            modelAndView = new ModelAndView("error");
            modelAndView.addObject("error", e.getMessage());
            log.debug("ERROR: " + e.getMessage());
        }catch (Exception ex) {
            // modelAndView = new ModelAndView( ex.getMessage());
            modelAndView = new ModelAndView("error");
            modelAndView.addObject("error", ex.getMessage());
            log.debug("ERROR: " + ex.getMessage());
        }

        return modelAndView;
    }
    
    /**
     * Servicio para obtener los adeudos de una clave catastral
     * @param clvCatastral
     * @return
     * @throws Exception 
     */
    @GetMapping("/obtener-adeudos/{clvCatastral}")
    @Timed
    public ResponseEntity<?> obtenerAdeudos(@PathVariable String clvCatastral) throws Exception {
        // Respuesta a la petición del cliente
        ResponseEntity<?> respuesta = null;
        // Map para generar el JSON con nombre
        Map<String, Object> resultado = new HashMap<>();
        try {
            //busca los adeudos relacionados con la clave catastral
            resultado.put("respuesta", this.plCarroService.obtenerAdeudos(clvCatastral));
            respuesta = ResponseEntity.status(HttpStatus.OK).body(resultado);
        } catch (ExceptionAPI e) {
            resultado.put("respuesta", e.getMessage());
            return ResponseEntity.status(e.getEstadoHttp()).body(resultado);
        }catch (Exception ex) {
                resultado.put("respuesta", ex.getMessage());
                respuesta = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultado);
        }
        return respuesta;
    }
    
    /**
     * Servicio para obtener los adeudos de una clave catastral
     * @param numLiquidacion
     * @return
     * @throws Exception 
     */
    @GetMapping("/obtener-liquidacion/{numLiquidacion}")
    @Timed
    public ResponseEntity<?> obtenerLiquidaciones(@PathVariable String numLiquidacion) throws Exception {
        // Respuesta a la petición del cliente
        ResponseEntity<?> respuesta = null;
        // Map para generar el JSON con nombre
        Map<String, Object> resultado = new HashMap<>();
        try {
            //busca los adeudos relacionados con la clave catastral
            resultado.put("respuesta", this.plCarroService.obtenerLiquidacion(numLiquidacion));
            respuesta = ResponseEntity.status(HttpStatus.OK).body(resultado);
        } catch (ExceptionAPI e) {
            resultado.put("respuesta", e.getMessage());
            return ResponseEntity.status(e.getEstadoHttp()).body(resultado);
        }catch (Exception ex) {
                resultado.put("respuesta", ex.getMessage());
                respuesta = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultado);
        }
        return respuesta;
    }
    
    /**
     * Servicio que obtiene el historial de pagos realizados por un usuario
     * @param pageable
     * @return
     * @throws Exception 
     */
    @GetMapping("/historial-pagos")
    @Timed
    public ResponseEntity<?> historial(Pageable pageable) throws Exception {
        // Respuesta a la petición del cliente
        ResponseEntity<?> respuesta = null;
        // Map para generar el JSON con nombre
        Map<String, Object> resultado = new HashMap<>();
        try {
            //busca el historial de pagos del usuario
            resultado.put("respuesta", this.plCarroService.obtenerHistorial(pageable));
            respuesta = ResponseEntity.status(HttpStatus.OK).body(resultado);
        } catch (ExceptionAPI e) {
            resultado.put("respuesta", e.getMessage());
            respuesta = ResponseEntity.status(e.getEstadoHttp()).body(resultado);
        }catch (Exception ex) {
            resultado.put("respuesta", ex.getMessage());
            respuesta = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultado);
        }
        return respuesta;
    }
    
    /**
     * Servicio para obtener la cantidad de paginas.
     * @param cantidadElementos
     * @return 
     */
    @GetMapping("/cantidadPaginas/{cantidadElementos}")
    @Timed
     public ResponseEntity obtenerLaCantidadDePaginas(@PathVariable int cantidadElementos){   
        ResponseEntity respuesta = null;//Creamos el objeto de respuesat al cliente
        Map resultado = new HashMap();//Creamos un map para generar el JSON con nombre
        try{
            resultado.put("respuesta",plCarroService.getNumPaginas(cantidadElementos));
            respuesta = new ResponseEntity(resultado, HttpStatus.OK);
        }catch(ExceptionAPI e){            
            resultado.put("respuesta", e.getMessage()); //Agregamos la respuesta al map indicando el error
            respuesta =  new ResponseEntity(resultado, e.getEstadoHttp()); //Regresamos la excepcion al cliente
        }     
        return respuesta;//Regresamos una respuesta la cliente
    }
}
