package com.worknest.pagobancomer.web.rest;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.codahale.metrics.annotation.Timed;
import com.worknest.pagobancomer.service.PlIntentoPagoService;
import com.worknest.pagobancomer.service.PlRespuestaBancoService;
import com.worknest.pagobancomer.service.dto.DatosConciliacionDTO;
import com.worknest.pagobancomer.service.dto.PlRespuestaBancoDTO;
import com.worknest.pagobancomer.web.rest.errors.BadRequestAlertException;
import com.worknest.pagobancomer.web.rest.errors.ExceptionAPI;
import com.worknest.pagobancomer.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;
import org.springframework.http.HttpStatus;

/**
 * REST controller for managing PlRespuestaBanco.
 */
@RestController
@RequestMapping("/api")
public class PlRespuestaBancoResource {

    private final Logger log = LoggerFactory.getLogger(PlRespuestaBancoResource.class);

    private static final String ENTITY_NAME = "plRespuestaBanco";

    private final PlRespuestaBancoService plRespuestaBancoService;
        
    private final PlIntentoPagoService plIntentoPagoService;
    
    public PlRespuestaBancoResource(PlRespuestaBancoService plRespuestaBancoService, PlIntentoPagoService plIntentoPagoService) {
        this.plRespuestaBancoService = plRespuestaBancoService;
        this.plIntentoPagoService = plIntentoPagoService;
    }

    /**
     * POST  /pl-respuesta-bancos : Create a new plRespuestaBanco.
     *
     * @param plRespuestaBancoDTO the plRespuestaBancoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new plRespuestaBancoDTO, or with status 400 (Bad Request) if the plRespuestaBanco has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pl-respuesta-bancos")
    @Timed
    public ResponseEntity<PlRespuestaBancoDTO> createPlRespuestaBanco(@Valid @RequestBody PlRespuestaBancoDTO plRespuestaBancoDTO) throws URISyntaxException {
        log.debug("REST request to save PlRespuestaBanco : {}", plRespuestaBancoDTO);
        if (plRespuestaBancoDTO.getId() != null) {
            throw new BadRequestAlertException("A new plRespuestaBanco cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlRespuestaBancoDTO result = plRespuestaBancoService.save(plRespuestaBancoDTO);
        return ResponseEntity.created(new URI("/api/pl-respuesta-bancos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pl-respuesta-bancos : Updates an existing plRespuestaBanco.
     *
     * @param plRespuestaBancoDTO the plRespuestaBancoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated plRespuestaBancoDTO,
     * or with status 400 (Bad Request) if the plRespuestaBancoDTO is not valid,
     * or with status 500 (Internal Server Error) if the plRespuestaBancoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pl-respuesta-bancos")
    @Timed
    public ResponseEntity<PlRespuestaBancoDTO> updatePlRespuestaBanco(@Valid @RequestBody PlRespuestaBancoDTO plRespuestaBancoDTO) throws URISyntaxException {
        log.debug("REST request to update PlRespuestaBanco : {}", plRespuestaBancoDTO);
        if (plRespuestaBancoDTO.getId() == null) {
            return createPlRespuestaBanco(plRespuestaBancoDTO);
        }
        PlRespuestaBancoDTO result = plRespuestaBancoService.save(plRespuestaBancoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, plRespuestaBancoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pl-respuesta-bancos : get all the plRespuestaBancos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of plRespuestaBancos in body
     */
    @GetMapping("/pl-respuesta-bancos")
    @Timed
    public List<PlRespuestaBancoDTO> getAllPlRespuestaBancos() {
        log.debug("REST request to get all PlRespuestaBancos");
        return plRespuestaBancoService.findAll();
        }

    /**
     * GET  /pl-respuesta-bancos/:id : get the "id" plRespuestaBanco.
     *
     * @param id the id of the plRespuestaBancoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the plRespuestaBancoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pl-respuesta-bancos/{id}")
    @Timed
    public ResponseEntity<PlRespuestaBancoDTO> getPlRespuestaBanco(@PathVariable Long id) {
        log.debug("REST request to get PlRespuestaBanco : {}", id);
        PlRespuestaBancoDTO plRespuestaBancoDTO = plRespuestaBancoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(plRespuestaBancoDTO));
    }

    /**
     * DELETE  /pl-respuesta-bancos/:id : delete the "id" plRespuestaBanco.
     *
     * @param id the id of the plRespuestaBancoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pl-respuesta-bancos/{id}")
    @Timed
    public ResponseEntity<Void> deletePlRespuestaBanco(@PathVariable Long id) {
        log.debug("REST request to delete PlRespuestaBanco : {}", id);
        plRespuestaBancoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    
    /**
     * Inserta los parametros que regresa el banco cuando se realiza un pago aceptado
     * @param respuestaBanco
     * @return
     * @throws Exception 
     */
    @PostMapping("/respuesta-banco/aceptado")
    @Timed
    public ModelAndView respuestaBancoAceptada(@Valid @RequestBody Map<String,String> respuestaBanco) throws ExceptionAPI, Exception  {   	
    	ModelAndView respuesta;
        respuesta= new ModelAndView("respuestaBancomer");	
        try {
            // validar datos de la respuesta
            if(!plIntentoPagoService.validarDatosReciboBancomer(respuestaBanco, false)) {
                respuesta= new ModelAndView("error").addObject("error", "Los datos recibidos no son correctos");
            }
            if(!plIntentoPagoService.verificaAutorizacion(respuestaBanco.get("mp_authorization"))) {
                respuesta= new ModelAndView("error").addObject("error", "El pago ya se aplicó con anterioridad: " + respuestaBanco.get("mp_reference") + ".");
            }   	
            //se guarda la respuesta del banco, se envia true ya que el pago fue aceptado
            String mensaje =plRespuestaBancoService.guardarRespuesta(respuestaBanco, true);         
            respuesta.addObject("esExito", true);
            respuesta.addObject("referencia", respuestaBanco.get("mp_reference"));
            respuesta.addObject("mensajeBanco", respuestaBanco.get("mp_responsemsg") + ". Información adicional: " + mensaje);
            respuesta.addObject("error", "");                       
        } catch (ExceptionAPI e) {
            log.error(e.toString());
            respuesta= new ModelAndView("error").addObject("error", e.getMessage() == null ? e.toString() : e.getMessage());
        } catch (Exception e) {
            log.error("respuestaBancoAceptada():" + e.toString());
            respuesta= new ModelAndView("error").addObject("error", e.getMessage() == null ? e.toString() : e.getMessage());
        }
        return respuesta;
    }
    
    /**
     * Inserta los parametros que regresa el banco cuando el pago fue rechazado
     * @param respuestaBanco
     * @return
     * @throws Exception 
     */
    @PostMapping("/respuesta-banco/rechazado")
    @Timed
    public ModelAndView respuestaBancoRechazada(@Valid @RequestBody Map<String, String> respuestaBanco) throws Exception {
        ModelAndView respuesta;
        respuesta= new ModelAndView("respuestaBancomer");
        try {
            // validar datos de la respuesta
            if (!this.plIntentoPagoService.validarDatosReciboBancomer(respuestaBanco, false)) {
                respuesta= new ModelAndView("error").addObject("error", "Los firma no es correcta");
            }
            // se guarda la respuesta del banco, se envia false ya que el pago fue rechazado
            this.plRespuestaBancoService.guardarRespuesta(respuestaBanco, false);
            respuesta.addObject("esExito", false);
            respuesta.addObject("referencia", respuestaBanco.get("mp_reference"));
            respuesta.addObject("mensajeBanco", respuestaBanco.get("mp_responsemsg"));
            respuesta.addObject("error", "");
        } catch (ExceptionAPI e) {
            respuesta= new ModelAndView("error").addObject("error", e.getMessage());
        } catch (Exception e) {
            respuesta= new ModelAndView("error").addObject("error", e.getMessage());
        }
        return respuesta;
    }
    
    /**
     * Servicio para aplicar pago fuera de linea con archivo
     * @param multipart
     * @return 
     */
    @PostMapping("/aplicar-pago-archivo")
    @Timed
    public ResponseEntity getAplicarPagoOfflineArchivo(@RequestParam("nombreArchivo") MultipartFile multipart ) throws Exception {
        ResponseEntity respuesta;//Generamos la respuesta para el cliente
        Map resultado = new HashMap();//Generamos un mapa para construir el JSON con nombre
        
        BufferedReader reader = null;
        Map<String, String> parametros = new HashMap<String, String>();
        int cont =1;
        String mensajeRespuesta;
        List<String> listaRespuestas = new ArrayList<>();

        try {
            reader = new BufferedReader(new InputStreamReader(multipart.getInputStream(), "UTF-8"));
            String cadena;
            // TODO no se cual sea el EMAIL, se debe de optener de 'mp_email'
            String urlBase = "";

            while ((cadena = reader.readLine()) != null) {

                parametros.put("mp_date", cadena.substring(0, 26).trim());
                parametros.put("mp_node", cadena.substring(76, 86).trim());
                parametros.put("mp_concept", cadena.substring(86, 96).trim());
                parametros.put("mp_paymentmethod", cadena.substring(96, 106).trim());
                parametros.put("mp_reference", cadena.substring(106, 146).trim());
                parametros.put("mp_order", cadena.substring(146, 186).trim());
                parametros.put("mp_authorization", cadena.substring(186, 196).trim());
                parametros.put("mp_saleid", cadena.substring(196, 216).trim());
                parametros.put("mp_amount", cadena.substring(236, 258).trim());
                parametros.put("mp_email",cadena.substring(515, 566).trim());

                log.info("dato: "+cont);
                log.info("1 26 :" + cadena.substring(0, 26).trim());      //fecha
                log.info("2 50 :" + cadena.substring(26, 76).trim());     //municipio
                log.info("3 10 :" + cadena.substring(76, 86).trim());     //node
                log.info("4 10 :" + cadena.substring(86, 96).trim());     //concepto
                log.info("5 10 :" + cadena.substring(96, 106).trim());    //paymentmethod
                log.info("6 40 :" + cadena.substring(106, 146).trim());   //reference
                log.info("7 40 :" + cadena.substring(146, 186).trim());   //order
                log.info("8 10 :" + cadena.substring(186, 196).trim());   //authorization
                log.info("9 20 :" + cadena.substring(196, 216).trim());   //id venta
                log.info("10 20   :" + cadena.substring(216, 236).trim());//numtarjeta
                log.info("11 22   :" + cadena.substring(236, 258).trim());//importe
                log.info("12 22   :" + cadena.substring(258, 280).trim());//
                log.info("13 22   :" + cadena.substring(280, 302).trim());//
                log.info("14 10   :" + cadena.substring(302, 312).trim());//fecha vencimiento
                log.info("15 2    :" + cadena.substring(312, 314).trim());//
                log.info("16 1    :" + cadena.substring(314, 315).trim());//
                log.info("17 100  :" + cadena.substring(315, 415).trim());//nombre banco
                log.info("18 100  :" + cadena.substring(415, 515).trim());//nombre pagador
                log.info("19 50   :" + cadena.substring(515, 566).trim());//correo
                log.info("20 50   :" + cadena.substring(566, 615).trim());//

                urlBase = cadena.substring(236, 258).trim();
                // parametros.put("mp_email", urlBase);

                // respuestas de cada pago
                try{
                    mensajeRespuesta=plRespuestaBancoService.guardarRespuesta(parametros, true);
                    listaRespuestas.add(mensajeRespuesta);
                }
                catch (ExceptionAPI e) {
                    listaRespuestas.add("Error: " +e.getMensaje());
                } catch (Exception e) {
                    listaRespuestas.add("Error: " + e.toString());
                }
                // vaciar map
                parametros = new HashMap<String, String>();
                cont++;
            }
            resultado.put("respuesta", listaRespuestas);
            respuesta = new ResponseEntity(resultado, HttpStatus.OK);  
        } catch (FileNotFoundException e) {
            resultado.put("respuesta", e.getMessage());
            respuesta = ResponseEntity.status(HttpStatus.CONFLICT).body(resultado);
        } catch (IOException e) {
            resultado.put("respuesta", "No existe el historial de este pago");
            respuesta = ResponseEntity.status(HttpStatus.CONFLICT).body(resultado);
        }     
        return respuesta;//Regresamos la respuesta al cliente
    }
    /**
     * Servicio para aplicar pago fuera de linea individualmente
     * @param datos
     * @return
     * @throws Exception 
     */
    @PostMapping("/aplicar-pago")
    @Timed
    public ResponseEntity getAplicarPagoOfline(@Valid @RequestBody DatosConciliacionDTO datos) throws Exception {
        ResponseEntity respuesta;//Generamos la respuesta para el cliente
        Map resultado = new HashMap();//Generamos un mapa para construir el JSON con nombre

        Map<String, String> parametros = new HashMap<String, String>();

        try {
            parametros =plRespuestaBancoService.obtenerDatos(datos);
            plIntentoPagoService.validarDatosReciboBancomer(parametros, true); 
            if(!plIntentoPagoService.verificaAutorizacion(parametros.get("mp_authorization"))) {
                resultado.put("respuesta", "El pago fue aplicado con anterioridad");
                respuesta = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultado);
            } else{               
                resultado.put("respuesta", plRespuestaBancoService.guardarRespuesta(parametros, true));
                respuesta = new ResponseEntity(resultado, HttpStatus.OK);  
            }
        } catch (ExceptionAPI e) {
            resultado.put("respuesta", e.getMessage());
            respuesta = ResponseEntity.status(e.getEstadoHttp()).body(resultado);
        } catch (Exception e) {
            resultado.put("respuesta", "No existe el historial de este pago");
            respuesta = ResponseEntity.status(HttpStatus.CONFLICT).body(resultado);
        }   
        return respuesta;//Regresamos la respuesta al cliente
    }
}
