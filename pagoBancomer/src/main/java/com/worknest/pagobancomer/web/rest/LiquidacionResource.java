package com.worknest.pagobancomer.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.worknest.pagobancomer.service.LiquidacionService;
import com.worknest.pagobancomer.service.dto.LiquidacionDTO;
import com.worknest.pagobancomer.web.rest.errors.BadRequestAlertException;
import com.worknest.pagobancomer.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Liquidacion.
 */
@RestController
@RequestMapping("/api")
public class LiquidacionResource {

    private final Logger log = LoggerFactory.getLogger(LiquidacionResource.class);

    private static final String ENTITY_NAME = "liquidacion";

    private final LiquidacionService liquidacionService;

    public LiquidacionResource(LiquidacionService liquidacionService) {
        this.liquidacionService = liquidacionService;
    }

    /**
     * POST  /liquidacions : Create a new liquidacion.
     *
     * @param liquidacionDTO the liquidacionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new liquidacionDTO, or with status 400 (Bad Request) if the liquidacion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/liquidacions")
    @Timed
    public ResponseEntity<LiquidacionDTO> createLiquidacion(@Valid @RequestBody LiquidacionDTO liquidacionDTO) throws URISyntaxException {
        log.debug("REST request to save Liquidacion : {}", liquidacionDTO);
        if (liquidacionDTO.getId() != null) {
            throw new BadRequestAlertException("A new liquidacion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LiquidacionDTO result = liquidacionService.save(liquidacionDTO);
        return ResponseEntity.created(new URI("/api/liquidacions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /liquidacions : Updates an existing liquidacion.
     *
     * @param liquidacionDTO the liquidacionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated liquidacionDTO,
     * or with status 400 (Bad Request) if the liquidacionDTO is not valid,
     * or with status 500 (Internal Server Error) if the liquidacionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/liquidacions")
    @Timed
    public ResponseEntity<LiquidacionDTO> updateLiquidacion(@Valid @RequestBody LiquidacionDTO liquidacionDTO) throws URISyntaxException {
        log.debug("REST request to update Liquidacion : {}", liquidacionDTO);
        if (liquidacionDTO.getId() == null) {
            return createLiquidacion(liquidacionDTO);
        }
        LiquidacionDTO result = liquidacionService.save(liquidacionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, liquidacionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /liquidacions : get all the liquidacions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of liquidacions in body
     */
    @GetMapping("/liquidacions")
    @Timed
    public List<LiquidacionDTO> getAllLiquidacions() {
        log.debug("REST request to get all Liquidacions");
        return liquidacionService.findAll();
        }

    /**
     * GET  /liquidacions/:id : get the "id" liquidacion.
     *
     * @param id the id of the liquidacionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the liquidacionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/liquidacions/{id}")
    @Timed
    public ResponseEntity<LiquidacionDTO> getLiquidacion(@PathVariable Long id) {
        log.debug("REST request to get Liquidacion : {}", id);
        LiquidacionDTO liquidacionDTO = liquidacionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(liquidacionDTO));
    }

    /**
     * DELETE  /liquidacions/:id : delete the "id" liquidacion.
     *
     * @param id the id of the liquidacionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/liquidacions/{id}")
    @Timed
    public ResponseEntity<Void> deleteLiquidacion(@PathVariable Long id) {
        log.debug("REST request to delete Liquidacion : {}", id);
        liquidacionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
