package com.worknest.pagobancomer.web.rest.util;

import java.util.Objects;

public class Respuesta {
	private boolean ok;
    private String mensaje;
    private Object objeto;

    public Respuesta() {
    	this.ok=true;
    }

    public Respuesta(boolean ok, String mensaje) {
        this.ok = ok;
        this.mensaje = mensaje;
    }

    public Respuesta(boolean ok, String mensaje, Object objeto) {
        this.ok = ok;
        this.mensaje = mensaje;
        this.objeto = objeto;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Object getObjeto() {
        return objeto;
    }

    public void setObjeto(Object objeto) {
        this.objeto = objeto;
    }

    @Override
    public String toString() {
        return "Respuesta{" + "ok=" + isOk() + ", mensaje=" + mensaje + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) {
            return false;
        }
        if(getClass() != obj.getClass()) {
            return false;
        }
        final Respuesta other = (Respuesta) obj;
        if(this.isOk() != other.isOk()) {
            return false;
        }
        if(!Objects.equals(this.mensaje, other.mensaje)) {
            return false;
        }
        return Objects.equals(this.objeto, other.objeto);
    }

}
