package com.worknest.pagobancomer.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.worknest.pagobancomer.service.CatBancoService;
import com.worknest.pagobancomer.service.dto.CatBancoDTO;
import com.worknest.pagobancomer.web.rest.errors.BadRequestAlertException;
import com.worknest.pagobancomer.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing CatBanco.
 */
@RestController
@RequestMapping("/api")
public class CatBancoResource {

    private final Logger log = LoggerFactory.getLogger(CatBancoResource.class);

    private static final String ENTITY_NAME = "catBanco";

    private final CatBancoService catBancoService;

    public CatBancoResource(CatBancoService catBancoService) {
        this.catBancoService = catBancoService;
    }

    /**
     * POST  /cat-bancos : Create a new catBanco.
     *
     * @param catBancoDTO the catBancoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new catBancoDTO, or with status 400 (Bad Request) if the catBanco has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/cat-bancos")
    @Timed
    public ResponseEntity<CatBancoDTO> createCatBanco(@Valid @RequestBody CatBancoDTO catBancoDTO) throws URISyntaxException {
        log.debug("REST request to save CatBanco : {}", catBancoDTO);
        if (catBancoDTO.getId() != null) {
            throw new BadRequestAlertException("A new catBanco cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CatBancoDTO result = catBancoService.save(catBancoDTO);
        return ResponseEntity.created(new URI("/api/cat-bancos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /cat-bancos : Updates an existing catBanco.
     *
     * @param catBancoDTO the catBancoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated catBancoDTO,
     * or with status 400 (Bad Request) if the catBancoDTO is not valid,
     * or with status 500 (Internal Server Error) if the catBancoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/cat-bancos")
    @Timed
    public ResponseEntity<CatBancoDTO> updateCatBanco(@Valid @RequestBody CatBancoDTO catBancoDTO) throws URISyntaxException {
        log.debug("REST request to update CatBanco : {}", catBancoDTO);
        if (catBancoDTO.getId() == null) {
            return createCatBanco(catBancoDTO);
        }
        CatBancoDTO result = catBancoService.save(catBancoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, catBancoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /cat-bancos : get all the catBancos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of catBancos in body
     */
    @GetMapping("/cat-bancos")
    @Timed
    public List<CatBancoDTO> getAllCatBancos() {
        log.debug("REST request to get all CatBancos");
        return catBancoService.findAll();
        }

    /**
     * GET  /cat-bancos/:id : get the "id" catBanco.
     *
     * @param id the id of the catBancoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the catBancoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/cat-bancos/{id}")
    @Timed
    public ResponseEntity<CatBancoDTO> getCatBanco(@PathVariable Long id) {
        log.debug("REST request to get CatBanco : {}", id);
        CatBancoDTO catBancoDTO = catBancoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(catBancoDTO));
    }

    /**
     * DELETE  /cat-bancos/:id : delete the "id" catBanco.
     *
     * @param id the id of the catBancoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/cat-bancos/{id}")
    @Timed
    public ResponseEntity<Void> deleteCatBanco(@PathVariable Long id) {
        log.debug("REST request to delete CatBanco : {}", id);
        catBancoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
