package com.worknest.pagobancomer.web.rest;

import com.worknest.pagobancomer.domain.PersistentAuditEvent;
import com.worknest.pagobancomer.security.SecurityUtils;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.worknest.pagobancomer.service.AuditEventService;
import com.worknest.pagobancomer.service.dto.AuthenticateDTO;
import com.worknest.pagobancomer.service.dto.TokenDTO;
import com.worknest.pagobancomer.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

/**
 * REST controller for getting the audit events.
 */
@RestController
@RequestMapping("/management/audits")
public class AuditResource {
     private String _token;
    
    @Value("${application.autenticate}") private String _autenticate;
    @Value("${application.usuario}") private String _usuario;
    @Value("${application.pass}") private String _pass;
    @Value("${application.urlusuario}") private String _urlusuario;

    private final AuditEventService auditEventService;

    public AuditResource(AuditEventService auditEventService) {
        this.auditEventService = auditEventService;
    }

    /**
     * GET  /audits : get a page of AuditEvents.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of AuditEvents in body
     */
    @GetMapping
    public ResponseEntity<List<AuditEvent>> getAll(@ApiParam Pageable pageable) {
        Page<AuditEvent> page = auditEventService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/management/audits");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /audits : get a page of AuditEvents between the fromDate and toDate.
     *
     * @param fromDate the start of the time period of AuditEvents to get
     * @param toDate the end of the time period of AuditEvents to get
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of AuditEvents in body
     */
    @GetMapping(params = {"fromDate", "toDate"})
    public ResponseEntity<List<AuditEvent>> getByDates(
        @RequestParam(value = "fromDate") LocalDate fromDate,
        @RequestParam(value = "toDate") LocalDate toDate,
        @ApiParam Pageable pageable) {

        Page<AuditEvent> page = auditEventService.findByDates(
            fromDate.atStartOfDay(ZoneId.systemDefault()).toInstant(),
            toDate.atStartOfDay(ZoneId.systemDefault()).plusDays(1).toInstant(),
            pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/management/audits");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /audits/:id : get an AuditEvent by id.
     *
     * @param id the id of the entity to get
     * @return the ResponseEntity with status 200 (OK) and the AuditEvent in body, or status 404 (Not Found)
     */
    @GetMapping("/{id:.+}")
    public ResponseEntity<AuditEvent> get(@PathVariable Long id) {
        return ResponseUtil.wrapOrNotFound(auditEventService.find(id));
    }
    
    @PostMapping("/logout")
    public void addLogout (){
        PersistentAuditEvent audit = new PersistentAuditEvent();
        audit.setPrincipal(SecurityUtils.getCurrentUserLogin());
        audit.setAuditEventDate(Instant.now());
        audit.setAuditEventType("CIERRE_SESION");

        this._token = this.generaToken(this._usuario, this._pass).getId_token();
        // Se consulta el WS que genera el token
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + this._token);

        //Genera la url para consumir el WS que busque la informacion del usuario
        String uri = new String(this._urlusuario +"/management/audits/agregar");
        
        HttpEntity<PersistentAuditEvent> entity = new HttpEntity<PersistentAuditEvent>(audit, headers);
        // El WS genera las liquidaciones en base a los datos de envio

        restTemplate.exchange(uri, HttpMethod.POST, entity, void.class);

    }
    /**
     * Genera token para comunicacion con WS acapulco-caja
     * @param usuario usuario para generar token 
     * @param pass clave de acceso para generar token
     * @return token obtenido
     */
    public TokenDTO generaToken(String usuario, String pass) {

        // se definie las credenciales FIJAS
        AuthenticateDTO auth = new AuthenticateDTO();

        auth.setUsername(usuario); // user
        auth.setRememberMe(true);
        auth.setPassword(pass);    // user

        // se definie los headers
        Map<String, String> vars = new HashMap<String, String>();
        vars.put("Content-Type", "application/json");

        // se consulta el ws que genera el token
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        String uri = new String(this._urlusuario + this._autenticate);

        TokenDTO token = new TokenDTO();

        token = restTemplate.postForObject(uri, auth, TokenDTO.class, vars);

        // regresa el token
        return token;
    }
}
