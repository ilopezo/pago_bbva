package com.worknest.pagobancomer.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.worknest.pagobancomer.service.PlParamBancoService;
import com.worknest.pagobancomer.service.dto.PlParamBancoDTO;
import com.worknest.pagobancomer.web.rest.errors.BadRequestAlertException;
import com.worknest.pagobancomer.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing PlParamBanco.
 */
@RestController
@RequestMapping("/api")
public class PlParamBancoResource {

    private final Logger log = LoggerFactory.getLogger(PlParamBancoResource.class);

    private static final String ENTITY_NAME = "plParamBanco";

    private final PlParamBancoService plParamBancoService;

    public PlParamBancoResource(PlParamBancoService plParamBancoService) {
        this.plParamBancoService = plParamBancoService;
    }

    /**
     * POST  /pl-param-bancos : Create a new plParamBanco.
     *
     * @param plParamBancoDTO the plParamBancoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new plParamBancoDTO, or with status 400 (Bad Request) if the plParamBanco has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pl-param-bancos")
    @Timed
    public ResponseEntity<PlParamBancoDTO> createPlParamBanco(@Valid @RequestBody PlParamBancoDTO plParamBancoDTO) throws URISyntaxException {
        log.debug("REST request to save PlParamBanco : {}", plParamBancoDTO);
        if (plParamBancoDTO.getId() != null) {
            throw new BadRequestAlertException("A new plParamBanco cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlParamBancoDTO result = plParamBancoService.save(plParamBancoDTO);
        return ResponseEntity.created(new URI("/api/pl-param-bancos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pl-param-bancos : Updates an existing plParamBanco.
     *
     * @param plParamBancoDTO the plParamBancoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated plParamBancoDTO,
     * or with status 400 (Bad Request) if the plParamBancoDTO is not valid,
     * or with status 500 (Internal Server Error) if the plParamBancoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pl-param-bancos")
    @Timed
    public ResponseEntity<PlParamBancoDTO> updatePlParamBanco(@Valid @RequestBody PlParamBancoDTO plParamBancoDTO) throws URISyntaxException {
        log.debug("REST request to update PlParamBanco : {}", plParamBancoDTO);
        if (plParamBancoDTO.getId() == null) {
            return createPlParamBanco(plParamBancoDTO);
        }
        PlParamBancoDTO result = plParamBancoService.save(plParamBancoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, plParamBancoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pl-param-bancos : get all the plParamBancos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of plParamBancos in body
     */
    @GetMapping("/pl-param-bancos")
    @Timed
    public List<PlParamBancoDTO> getAllPlParamBancos() {
        log.debug("REST request to get all PlParamBancos");
        return plParamBancoService.findAll();
        }

    /**
     * GET  /pl-param-bancos/:id : get the "id" plParamBanco.
     *
     * @param id the id of the plParamBancoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the plParamBancoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pl-param-bancos/{id}")
    @Timed
    public ResponseEntity<PlParamBancoDTO> getPlParamBanco(@PathVariable Long id) {
        log.debug("REST request to get PlParamBanco : {}", id);
        PlParamBancoDTO plParamBancoDTO = plParamBancoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(plParamBancoDTO));
    }

    /**
     * DELETE  /pl-param-bancos/:id : delete the "id" plParamBanco.
     *
     * @param id the id of the plParamBancoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pl-param-bancos/{id}")
    @Timed
    public ResponseEntity<Void> deletePlParamBanco(@PathVariable Long id) {
        log.debug("REST request to delete PlParamBanco : {}", id);
        plParamBancoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
