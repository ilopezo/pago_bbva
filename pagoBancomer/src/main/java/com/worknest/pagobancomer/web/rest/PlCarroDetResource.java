package com.worknest.pagobancomer.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.worknest.pagobancomer.service.PlCarroDetService;
import com.worknest.pagobancomer.service.PlCarroService;
import com.worknest.pagobancomer.service.dto.AgregarConceptoDTO;
import com.worknest.pagobancomer.service.dto.PlCarroDTO;
import com.worknest.pagobancomer.service.dto.PlCarroDetDTO;
import com.worknest.pagobancomer.web.rest.errors.BadRequestAlertException;
import com.worknest.pagobancomer.web.rest.errors.ExceptionAPI;
import com.worknest.pagobancomer.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing PlCarroDet.
 */
@RestController
@RequestMapping("/api")
public class PlCarroDetResource {

    private final Logger log = LoggerFactory.getLogger(PlCarroDetResource.class);

    private static final String ENTITY_NAME = "plCarroDet";

    private final PlCarroDetService plCarroDetService;

    private final PlCarroService plCarroService;

    public PlCarroDetResource(PlCarroDetService plCarroDetService, PlCarroService plCarroService) {
        this.plCarroDetService = plCarroDetService;
        this.plCarroService = plCarroService;
    }

    /**
     * POST /pl-carro-dets : Create a new plCarroDet.
     * @param plCarroDetDTO
     *            the plCarroDetDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new
     *         plCarroDetDTO, or with status 400 (Bad Request) if the plCarroDet has
     *         already an ID
     * @throws URISyntaxException
     *             if the Location URI syntax is incorrect
     */
    @PostMapping("/pl-carro-dets")
    @Timed
    public ResponseEntity<PlCarroDetDTO> createPlCarroDet(@Valid @RequestBody PlCarroDetDTO plCarroDetDTO)
                throws URISyntaxException {
        log.debug("REST request to save PlCarroDet : {}", plCarroDetDTO);
        if (plCarroDetDTO.getId() != null) {
                throw new BadRequestAlertException("A new plCarroDet cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlCarroDetDTO result = plCarroDetService.save(plCarroDetDTO);
        return ResponseEntity.created(new URI("/api/pl-carro-dets/" + result.getId()))
                            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT /pl-carro-dets : Updates an existing plCarroDet.
     * @param plCarroDetDTO
     *            the plCarroDetDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated
     *         plCarroDetDTO, or with status 400 (Bad Request) if the plCarroDetDTO
     *         is not valid, or with status 500 (Internal Server Error) if the
     *         plCarroDetDTO couldn't be updated
     * @throws URISyntaxException
     *             if the Location URI syntax is incorrect
     */
    @PutMapping("/pl-carro-dets")
    @Timed
    public ResponseEntity<PlCarroDetDTO> updatePlCarroDet(@Valid @RequestBody PlCarroDetDTO plCarroDetDTO)
                    throws URISyntaxException {
        log.debug("REST request to update PlCarroDet : {}", plCarroDetDTO);
        if (plCarroDetDTO.getId() == null) {
                return createPlCarroDet(plCarroDetDTO);
        }
        PlCarroDetDTO result = plCarroDetService.save(plCarroDetDTO);
        return ResponseEntity.ok()
                        .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, plCarroDetDTO.getId().toString()))
                        .body(result);
    }

    /**
     * GET /pl-carro-dets : get all the plCarroDets.
     * @return the ResponseEntity with status 200 (OK) and the list of plCarroDets
     *         in body
     */
    @GetMapping("/pl-carro-dets")
    @Timed
    public List<PlCarroDetDTO> getAllPlCarroDets() {
        log.debug("REST request to get all PlCarroDets");
        return plCarroDetService.findAll();
    }

    /**
     * GET /pl-carro-dets/:id : get the "id" plCarroDet.
     * @param id
     *            the id of the plCarroDetDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the
     *         plCarroDetDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pl-carro-dets/{id}")
    @Timed
    public ResponseEntity<PlCarroDetDTO> getPlCarroDet(@PathVariable Long id) {
        log.debug("REST request to get PlCarroDet : {}", id);
        PlCarroDetDTO plCarroDetDTO = plCarroDetService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(plCarroDetDTO));
    }

    /**
     * DELETE /pl-carro-dets/:id : delete the "id" plCarroDet.
     * @param id
     *            the id of the plCarroDetDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pl-carro-dets/{id}")
    @Timed
    public ResponseEntity<Void> deletePlCarroDet(@PathVariable Long id) {
        log.debug("REST request to delete PlCarroDet : {}", id);
        plCarroDetService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * POST /pl-carro-det/agregar-concepto: Metodo que agrega un nuevo concepto a la
     * tabla PlCarroDet
     * @param nuevoConcepto datos del concepto a agregar (llave,bimestre inicia y bimestre
     *            final)
     * @return ResponseEntity con status 200(ok)
     * @throws Exception ResponseEntity con status 400 y mensaje de error
     */
    @PostMapping("/pl-carro-det/agregar-concepto")
    @Timed
    public ResponseEntity<?> agegarConcepto(@RequestBody AgregarConceptoDTO nuevoConcepto) throws Exception {
        ResponseEntity<?> respuesta = null;// Respuesta a la petición del cliente
        Map<String, Object> resultado = new HashMap<String, Object>();// Map para generar el JSON con nombre

        try {
            // obtener carro segun usuario
            PlCarroDTO carro = this.plCarroService.getCarro();

            // elimina los conceptos creados anteriormente si es por clave catastral
            if (nuevoConcepto.getBimini() != null && nuevoConcepto.getBimfin() != null) {

                // borra el detalle del carro
                this.plCarroDetService.borraDetalle(nuevoConcepto.getLlave(),carro);

                // borra el historico
                this.plCarroDetService.borraDetalleHist(nuevoConcepto.getLlave());
            }
            // guarda el nuevo concepto en el carro
            this.plCarroDetService.guardarCarroDet(nuevoConcepto, carro.getId());

            resultado.put("respuesta", "exito");
            respuesta = ResponseEntity.status(HttpStatus.OK).body(resultado);

        } catch (ExceptionAPI e) {
            resultado.put("respuesta", e.getMessage());
            respuesta = ResponseEntity.status(e.getEstadoHttp()).body(resultado);

        } catch (Exception ex) {
            resultado.put("respuesta", ex.getMessage());
            respuesta = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultado);
        }
        return respuesta;
    }

    /**
     * GET /pl-carro-det/ver-carrito : obtiene todos los conceptos. Método que lista los
     * conceptos que el usuario logeado tiene en su carro
     * @return ResponseEntity con lista de conceptos que se encuentran en el carro
     *         del usuario y status 200 Si no se tiene carro se regresa respuesta con 204
     * @throws Exception si se genera un error no controlado se genera la exception
     */
    @GetMapping("/pl-carro-det/ver-carrito")
    @Timed
    public ResponseEntity<?> obtenerConceptos() throws Exception {
        // Respuesta a la petición del cliente
        ResponseEntity<?> respuesta = null;

        // Map para generar el JSON con nombre
        Map<String, Object> resultado = new HashMap<String, Object>();

        try {
            // obtener carro segun usuario
            PlCarroDTO carro = this.plCarroService.getCarro();
            //busca los elementos que se encuentra en el carro del usuario
            resultado.put("respuesta", this.plCarroDetService.buscarPorCarro(carro.getId()));
            respuesta = ResponseEntity.status(HttpStatus.OK).body(resultado);
        } catch (ExceptionAPI e) {
            resultado.put("respuesta", e.getMessage());
            respuesta = ResponseEntity.status(e.getEstadoHttp()).body(resultado);
        }catch (Exception ex) {
            resultado.put("respuesta", ex.getMessage());
            respuesta = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultado);
        }

        return respuesta;
    }

    /**
     * DELETE /pl-carro-de/eliminar/:llave : llave a eliminar
     * Metodo que borra los conceptos segun su llave
     * @param llave puede ser clave catastra o numero de liquidación
     * @return ResponseEntity con status 200 (OK)
     * @throws Exception ResponseEntity con status 400 y mensaje de error
     */
    @DeleteMapping("/pl-carro-det/eliminar/{llave}")
    @Timed
    public ResponseEntity<?> eliminarConcepto(@PathVariable String llave) throws Exception {
        ResponseEntity<?> respuesta = null;// Respuesta a la petición del cliente
        Map<String, Object> resultado = new HashMap<String, Object>();// Map para generar el JSON con nombre

        try {
            // obtener carro segun usuario
            PlCarroDTO carro = this.plCarroService.getCarro();

            // obtiene los detalles de los conceptos con la llave
            List<PlCarroDetDTO> listaConceptos = this.plCarroDetService.getDetallesByLlaveCarro(llave, carro.getId());

            List<Long> envio = new ArrayList<Long>();
            
            // llenar la lista de ids que contienen el id de la liquidacion a eliminar
            for (PlCarroDetDTO carroDet : listaConceptos) {
                envio.add(carroDet.getLiquidacionId());
            }			
            // borra el detalle
            boolean generoUs = this.plCarroDetService.borraDetalle(llave,carro);

            // borra el historico
            this.plCarroDetService.borraDetalleHist(llave);

            // borra la liquidacion en caso de haberla generado el usuario
            this.plCarroDetService.borrarConcepto(llave, carro.getId(), generoUs, envio);

            resultado.put("respuesta", "exito");
            respuesta = ResponseEntity.status(HttpStatus.OK).body(resultado);

        } catch (ExceptionAPI e) {
            resultado.put("respuesta", e.getMessage());
            respuesta = ResponseEntity.status(e.getEstadoHttp()).body(resultado);
        } catch (Exception ex) {
            resultado.put("respuesta", ex.getMessage());
            respuesta = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultado);
        }
        return respuesta;
    }
    
    @DeleteMapping("/carro-det-eliminar/{idLiquidacion}")
    @Timed
    public ResponseEntity<Void> eliminarLiquidacion(@PathVariable Long idLiquidacion) throws ExceptionAPI {
        log.debug("REST request to delete PlCarroDet : {}"+idLiquidacion);
        plCarroDetService.deleteLiquidacion(idLiquidacion);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, idLiquidacion.toString())).build();
    }
}
