/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worknest.pagobancomer.web.rest.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author WorkNest9
 */
public class UtilFechas {
     public enum FormatosFecha {
        /**
         * Formato yyyy-MM-dd
         */
        ISO_8601("yyyy-MM-dd"),
        /**
         * Formato dd/MM/yyyy
         */
        LITTLE_ENDIAN("dd/MM/yyyy"),
        /**
         * Formato MM/dd/yyyy
         */
        MIDDLE_ENDIAN("MM/dd/yyyy"),
        /**
         * Formato yyyyMMdd utilizado en la 400
         */
        FORMATO_AS400("yyyyMMdd"),
        FORMATO_LITTLE_ENDIAN_CON_HORA("dd/MM/yyyy - HH:mm:ss a");

        FormatosFecha(String formato) {
            this.formato = formato;
        }
        private final String formato;

        /**
         * Cadena con el formato del enum
         *
         * @return Regresa una cadena con el formato específico del enum
         */
        public String getFormato() {
            return this.formato;
        }
    }
     /**
     * Convierte una fecha a una cadena con formato yyyy/MM/dd
     *
     * @param fecha Fecha a convertir
     * @return Regresa una cadena con el valor del formato yyyy/MM/dd o nulo si
     * el objeto de fecha es nulo
     */
    public static String convertirFecha(Date fecha) {
        return (fecha == null) ? null : convertirFecha(fecha, FormatosFecha.ISO_8601.getFormato());
    }

    /**
     * Convierte una fecha a una cadena con el formato del parámetro
     *
     * @param fecha Fecha que va a ser convertida
     * @param formato Formato de la fecha
     * @return Regresa una cadena con el valor de la fecha en el formato
     * especificado o nulo en caso de que el objeto de fecha sea nulo
     */
    public static String convertirFecha(Date fecha, FormatosFecha formato) {
        return convertirFecha(fecha, formato.getFormato());
    }

    /**
     * Convierte una fecha a una cadena con el formato del parámetro
     *
     * @param fecha Fecha que va a ser convertida
     * @param formato Formato de la fecha
     * @return Regresa una cadena con el valor de la fecha en el formato
     * especificado o nulo en caso de que el objeto de fecha sea nulo
     */
    public static String convertirFecha(Date fecha, String formato) {
        String retVal = null;
        if (fecha != null) {
            DateFormat formatoFecha = new SimpleDateFormat(formato);
            retVal = formatoFecha.format(fecha);
        }
        return retVal;
    }
}
