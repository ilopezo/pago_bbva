package com.worknest.pagobancomer.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.worknest.pagobancomer.service.PlParamRespuestaService;
import com.worknest.pagobancomer.service.dto.PlParamRespuestaDTO;
import com.worknest.pagobancomer.web.rest.errors.BadRequestAlertException;
import com.worknest.pagobancomer.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing PlParamRespuesta.
 */
@RestController
@RequestMapping("/api")
public class PlParamRespuestaResource {

    private final Logger log = LoggerFactory.getLogger(PlParamRespuestaResource.class);

    private static final String ENTITY_NAME = "plParamRespuesta";

    private final PlParamRespuestaService plParamRespuestaService;

    public PlParamRespuestaResource(PlParamRespuestaService plParamRespuestaService) {
        this.plParamRespuestaService = plParamRespuestaService;
    }

    /**
     * POST  /pl-param-respuestas : Create a new plParamRespuesta.
     *
     * @param plParamRespuestaDTO the plParamRespuestaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new plParamRespuestaDTO, or with status 400 (Bad Request) if the plParamRespuesta has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pl-param-respuestas")
    @Timed
    public ResponseEntity<PlParamRespuestaDTO> createPlParamRespuesta(@Valid @RequestBody PlParamRespuestaDTO plParamRespuestaDTO) throws URISyntaxException {
        log.debug("REST request to save PlParamRespuesta : {}", plParamRespuestaDTO);
        if (plParamRespuestaDTO.getId() != null) {
            throw new BadRequestAlertException("A new plParamRespuesta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlParamRespuestaDTO result = plParamRespuestaService.save(plParamRespuestaDTO);
        return ResponseEntity.created(new URI("/api/pl-param-respuestas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pl-param-respuestas : Updates an existing plParamRespuesta.
     *
     * @param plParamRespuestaDTO the plParamRespuestaDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated plParamRespuestaDTO,
     * or with status 400 (Bad Request) if the plParamRespuestaDTO is not valid,
     * or with status 500 (Internal Server Error) if the plParamRespuestaDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pl-param-respuestas")
    @Timed
    public ResponseEntity<PlParamRespuestaDTO> updatePlParamRespuesta(@Valid @RequestBody PlParamRespuestaDTO plParamRespuestaDTO) throws URISyntaxException {
        log.debug("REST request to update PlParamRespuesta : {}", plParamRespuestaDTO);
        if (plParamRespuestaDTO.getId() == null) {
            return createPlParamRespuesta(plParamRespuestaDTO);
        }
        PlParamRespuestaDTO result = plParamRespuestaService.save(plParamRespuestaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, plParamRespuestaDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pl-param-respuestas : get all the plParamRespuestas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of plParamRespuestas in body
     */
    @GetMapping("/pl-param-respuestas")
    @Timed
    public List<PlParamRespuestaDTO> getAllPlParamRespuestas() {
        log.debug("REST request to get all PlParamRespuestas");
        return plParamRespuestaService.findAll();
        }

    /**
     * GET  /pl-param-respuestas/:id : get the "id" plParamRespuesta.
     *
     * @param id the id of the plParamRespuestaDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the plParamRespuestaDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pl-param-respuestas/{id}")
    @Timed
    public ResponseEntity<PlParamRespuestaDTO> getPlParamRespuesta(@PathVariable Long id) {
        log.debug("REST request to get PlParamRespuesta : {}", id);
        PlParamRespuestaDTO plParamRespuestaDTO = plParamRespuestaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(plParamRespuestaDTO));
    }

    /**
     * DELETE  /pl-param-respuestas/:id : delete the "id" plParamRespuesta.
     *
     * @param id the id of the plParamRespuestaDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pl-param-respuestas/{id}")
    @Timed
    public ResponseEntity<Void> deletePlParamRespuesta(@PathVariable Long id) {
        log.debug("REST request to delete PlParamRespuesta : {}", id);
        plParamRespuestaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
