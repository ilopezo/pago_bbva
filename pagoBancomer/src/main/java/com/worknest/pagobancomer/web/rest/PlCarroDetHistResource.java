package com.worknest.pagobancomer.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.worknest.pagobancomer.service.PlCarroDetHistService;
import com.worknest.pagobancomer.service.dto.PlCarroDetHistDTO;
import com.worknest.pagobancomer.web.rest.errors.BadRequestAlertException;
import com.worknest.pagobancomer.web.rest.errors.ExceptionAPI;
import com.worknest.pagobancomer.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;
import java.util.HashMap;
import java.util.Map;
import org.springframework.http.HttpStatus;

/**
 * REST controller for managing PlCarroDetHist.
 */
@RestController
@RequestMapping("/api")
public class PlCarroDetHistResource {

    private final Logger log = LoggerFactory.getLogger(PlCarroDetHistResource.class);

    private static final String ENTITY_NAME = "plCarroDetHist";

    private final PlCarroDetHistService plCarroDetHistService;

    public PlCarroDetHistResource(PlCarroDetHistService plCarroDetHistService) {
        this.plCarroDetHistService = plCarroDetHistService;
    }

    /**
     * POST  /pl-carro-det-hists : Create a new plCarroDetHist.
     *
     * @param plCarroDetHistDTO the plCarroDetHistDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new plCarroDetHistDTO, or with status 400 (Bad Request) if the plCarroDetHist has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pl-carro-det-hists")
    @Timed
    public ResponseEntity<PlCarroDetHistDTO> createPlCarroDetHist(@Valid @RequestBody PlCarroDetHistDTO plCarroDetHistDTO) throws URISyntaxException {
        log.debug("REST request to save PlCarroDetHist : {}", plCarroDetHistDTO);
        if (plCarroDetHistDTO.getId() != null) {
            throw new BadRequestAlertException("A new plCarroDetHist cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlCarroDetHistDTO result = plCarroDetHistService.save(plCarroDetHistDTO);
        return ResponseEntity.created(new URI("/api/pl-carro-det-hists/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pl-carro-det-hists : Updates an existing plCarroDetHist.
     *
     * @param plCarroDetHistDTO the plCarroDetHistDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated plCarroDetHistDTO,
     * or with status 400 (Bad Request) if the plCarroDetHistDTO is not valid,
     * or with status 500 (Internal Server Error) if the plCarroDetHistDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pl-carro-det-hists")
    @Timed
    public ResponseEntity<PlCarroDetHistDTO> updatePlCarroDetHist(@Valid @RequestBody PlCarroDetHistDTO plCarroDetHistDTO) throws URISyntaxException {
        log.debug("REST request to update PlCarroDetHist : {}", plCarroDetHistDTO);
        if (plCarroDetHistDTO.getId() == null) {
            return createPlCarroDetHist(plCarroDetHistDTO);
        }
        PlCarroDetHistDTO result = plCarroDetHistService.save(plCarroDetHistDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, plCarroDetHistDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pl-carro-det-hists : get all the plCarroDetHists.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of plCarroDetHists in body
     */
    @GetMapping("/pl-carro-det-hists")
    @Timed
    public List<PlCarroDetHistDTO> getAllPlCarroDetHists() {
        log.debug("REST request to get all PlCarroDetHists");
        return plCarroDetHistService.findAll();
        }

    /**
     * GET  /pl-carro-det-hists/:id : get the "id" plCarroDetHist.
     *
     * @param id the id of the plCarroDetHistDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the plCarroDetHistDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pl-carro-det-hists/{id}")
    @Timed
    public ResponseEntity<PlCarroDetHistDTO> getPlCarroDetHist(@PathVariable Long id) {
        log.debug("REST request to get PlCarroDetHist : {}", id);
        PlCarroDetHistDTO plCarroDetHistDTO = plCarroDetHistService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(plCarroDetHistDTO));
    }

    /**
     * DELETE  /pl-carro-det-hists/:id : delete the "id" plCarroDetHist.
     *
     * @param id the id of the plCarroDetHistDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pl-carro-det-hists/{id}")
    @Timed
    public ResponseEntity<Void> deletePlCarroDetHist(@PathVariable Long id) {
        log.debug("REST request to delete PlCarroDetHist : {}", id);
        plCarroDetHistService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    
    /**
     * Servicio para ver los detalles del historial del pago
     * @param idPago
     * @return 
     */
    @GetMapping("/ver-detalles-historial/{idPago}")
    @Timed
    public ResponseEntity<?> verDetallesHist(@PathVariable Long idPago) {
        // Respuesta a la petición del cliente
        ResponseEntity<?> respuesta = null;
        // Map para generar el JSON con nombre
        Map<String, Object> resultado = new HashMap<>();
        try {
            //busca los detalles del historial relacionados con el id del pago
            resultado.put("respuesta", this.plCarroDetHistService.obtenerDetallesHist(idPago));
            respuesta = ResponseEntity.status(HttpStatus.OK).body(resultado);
        } catch (ExceptionAPI e) {
            resultado.put("respuesta", e.getMessage());
            respuesta = ResponseEntity.status(e.getEstadoHttp()).body(resultado);
        }catch (Exception ex) {
            resultado.put("respuesta", ex.getMessage());
            respuesta = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultado);
        }
        return respuesta;
    }
}
